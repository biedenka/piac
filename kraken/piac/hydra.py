import copy
import datetime
import os
import pickle
import time
from functools import partial
from multiprocessing import Process
import glob
from typing import List
import logging
import json

import numpy as np
from sklearn.ensemble import RandomForestClassifier

from smac.runhistory.runhistory import RunHistory, DataOrigin
from smac.scenario.scenario import Scenario
from smac.stats.stats import Stats
from smac.tae.execute_ta_run import TAEAbortException, FirstRunCrashedException
from smac.optimizer.objective import average_cost

from kraken.optimization.hydra_facade import HYDRA
from kraken.optimization.objective import average_portfolio_cost
from kraken.piac.kraken import setup_experiment_outdir
from kraken.util.stats import PIACStats
from kraken.util.utils import read_traj_file, load_runhistory_from_disc, get_tae, remove_inactive_params
from kraken.configspace import Configuration


class PretendSelector(object):
    def predict(X):
        return np.zeros((X.shape[0], ), dtype=int)


def setup_hydra_selector_over_time(scenario, file):
    overall_runhist = glob.glob(os.path.join(os.path.dirname(file), '**/overall_rh.json'), recursive=True)[0]
    # print(hydra_files, overall_runhist)
    with open(file, 'rb') as hy:
        portfolio = pickle.load(hy)
    pairs = []
    for i in range(len(portfolio)):
        round_port = list(map(lambda x: (x[1], 'h_%02d' % x[0]), enumerate(portfolio[:i+1])))
        rh = RunHistory(average_cost)
        rh.load_json(overall_runhist, scenario.cs)
        sort_by = scenario.valid_insts

        Y = []
        for config, _ in round_port:
            runs = np.array(rh.get_runs_for_config(config), dtype=object)
            y = []
            for inst in sort_by:
                y.append(average_cost(config, rh, runs[runs[:, 0] == inst]))
            Y.append(np.array(y))
        nums = []
        for idx, i in enumerate(scenario.train_insts):
            if i in sort_by:
                nums.append(idx)
        X = scenario.feature_array[nums]

        if len(round_port) == 1:
            clf = PretendSelector()
            classifiers = [clf]
            pairs.append((round_port, classifiers))
            continue
        classifiers = []
        for i in range(len(round_port)):
            for j in range(i + 1, len(round_port)):
                y_i = Y[i]
                y_j = Y[j]
                y = y_i < y_j
                weights = np.abs(y_i - y_j)
                clf = RandomForestClassifier()
                clf.fit(X, y, weights)
                classifiers.append(clf)
        pairs.append((round_port, classifiers))
    return pairs


def setup_hydra_selector(scenario, file):
    overall_runhist = glob.glob(os.path.join(os.path.dirname(file), '**/overall_rh.json'), recursive=True)[0]
    # print(hydra_files, overall_runhist)
    with open(file, 'rb') as hy:
        portfolio = pickle.load(hy)
    rh = RunHistory(average_cost)
    rh.load_json(overall_runhist, scenario.cs)
    sort_by = scenario.valid_insts

    Y = []
    for config in portfolio:
        runs = np.array(rh.get_runs_for_config(config), dtype=object)
        y = []
        for inst in sort_by:
            y.append(average_cost(config, rh, runs[runs[:, 0] == inst]))
        Y.append(np.array(y))
    nums = []
    for idx, i in enumerate(scenario.train_insts):
        if i in sort_by:
            nums.append(idx)
    X = scenario.feature_array[nums]

    classifiers = []
    for i in range(len(portfolio)):
        for j in range(i + 1, len(portfolio)):
            y_i = Y[i]
            y_j = Y[j]
            y = y_i < y_j
            weights = np.abs(y_i - y_j)
            clf = RandomForestClassifier()
            clf.fit(X, y, weights)
            classifiers.append(clf)
    return portfolio, classifiers


def get_hydra_conf(portfolio: List[Configuration], classifiers: List[RandomForestClassifier], instance: str,
                   scenario: Scenario):
    x = scenario.feature_dict[instance].reshape(1, -1)
    scores = np.zeros((1, len(portfolio)))
    clf_indx = 0
    if len(portfolio) > 1:
        for i in range(len(portfolio)):
            for j in range(i + 1, len(portfolio)):
                clf = classifiers[clf_indx]
                Y = clf.predict(x)
                scores[Y == 1, i] += 1
                scores[Y == 0, j] += 1
                clf_indx += 1
        logging.debug(scores)
        min_ = np.argmax(scores, axis=1)[0]  # NOT MIN!!!! We want the winner not the looser !!!!!

        logging.debug('np.argmax: %d' % min_)
        return portfolio[min_], 'h_%02d' % min_
    return portfolio[0]


def configure(scenario, rng, metric, stats, logger, seeds=None):
    logging.info('Starting Configurator with seed %d' % rng)
    optimizer = HYDRA(
        scenario=scenario,
        rng=np.random.RandomState(rng),
        aggregate_func=metric,
        stats=stats
    )
    try:
        stats.start_timing()
        incumbent = optimizer.optimize()
        runner = optimizer.get_tae_runner()
        logger.debug('Before: %d' % len(runner.runhistory.data))
        rh, costs = validate_config(runner, RunHistory(average_cost),
                                    optimizer.solver.scenario, incumbent, seeds, logger)
        logger.debug('After: %d' % len(rh.data))
        rh.update(optimizer.solver.runhistory, origin=DataOrigin.INTERNAL)
        logger.debug('After updated: %d' % len(rh.data))
        rh.save_json(
            fn=os.path.join(optimizer.solver.scenario.output_dir,
                            "runhistory.json"), save_external=True)
        optimizer.solver.intensifier.traj_logger.add_entry(train_perf=costs, incumbent_id=optimizer.stats.inc_changed,
                                                           incumbent=incumbent)
    except (TAEAbortException, FirstRunCrashedException) as err:
        logger.error(err)
    return optimizer.get_tae_runner(), optimizer.solver.incumbent


def validate_config(runner, runhist, scenario, incumbent, seeds, logger):
    runner.runhistory = runhist
    runner.stats = PIACStats(ta_run_limit=len(scenario.valid_insts))
    runner.stats.start_timing()
    logger.info('~*~'*40)
    logger.info('Validation Config on all instances')
    logger.debug(str(incumbent))
    logger.info('~*~'*40)
    count = 1
    costs = []
    for inst, seed in zip(scenario.valid_insts, seeds):
        logger.debug(''.join(['*'*20, '(...%s/%d)' % (inst[:-10], seed), ' => %d/%d' % (
            count, len(scenario.valid_insts))]))
        res = runner.start(incumbent, inst, cutoff=scenario.cutoff,
                           seed=seed)
        count += 1
        costs.append(res[1])
    logger.info('~*~'*40)
    return runner.runhistory, np.mean(costs)


def save_meta_info(all_runs_runhist, HYDRA_ROUND, stats, round_info,
                   rng, scenario, portfolio, metric, INST_SEEDS):
    if not os.path.exists('.meta_info'):
        os.mkdir('.meta_info')
    all_runs_runhist.save_json('overall_rh.json', save_external=True)
    round_dict = {}
    round_dict['HYDRA_ROUND'] = HYDRA_ROUND
    round_dict['used_time'] = stats.get_used_wallclock_time()
    round_dict['round_info'] = round_info
    with open(os.path.join('.meta_info', 'round_info.json'), 'w') as mf:
        json.dump(round_dict, mf)
    with open(os.path.join('.meta_info', 'rng.pkl'), 'wb') as pf:
        pickle.dump(rng, pf)
    with open(os.path.join('.meta_info', 'scenario.pkl'), 'wb') as sf:
        pickle.dump(scenario, sf)
    with open(os.path.join('.meta_info', 'portfolio.pkl'), 'wb') as pf:
        pickle.dump(portfolio, pf)
    with open(os.path.join('.meta_info', 'metric.pkl'), 'wb') as pf:
        pickle.dump(metric, pf)
    with open(os.path.join('.meta_info', 'inst_seeds.pkl'), 'wb') as pf:
        pickle.dump(INST_SEEDS, pf)


def hydra(args):
    ####################################################################################################################
    # Generate rudimentary objects that keep track of budget and where resources are located
    skip = False
    if not args.cont:
        logger, rng, scen_file, working_dir, return_to_dir = setup_experiment_outdir(
            scenario_file=args.scenario_file, working_dir=args.working_dir, seed=args.seed, name='piac',
            verbosity=args.verbosity,
            resource_dir=args.resource_dir)
        logger.info('Seed: %d' % args.seed)
        # logger, rng = setup_experiment_outdir(args, 'hydra')
        portfolio = []
        HYDRA_ROUND = 0
        scenario = Scenario(scenario=scen_file,
                            cmd_args={'output_dir': ""})
        out_dir = "HYDRA-output_%s" % (
            datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S'))
        scenario.output_dir = out_dir

        train_insts = copy.deepcopy(scenario.train_insts)
        if args.valid_small:
            np.random.shuffle(train_insts)
            split_id = int(np.floor(len(train_insts) * .9))
            valid_set = train_insts[split_id:]
            scenario.train_insts = train_insts
            scenario.__dict__['valid_insts'] = valid_set
        else:
            scenario.__dict__['valid_insts'] = train_insts

        INST_SEEDS = rng.random_integers(99999, size=len(scenario.valid_insts))
        round_info = []
        iteration_time = 0
        if args.debug:
            for i in range(10):
                logger.debug('%d: %s' % (i, scenario.train_insts[i]))
            rng.shuffle(scenario.train_insts)
            logger.debug(':'*120)
            scenario.train_insts = scenario.train_insts[:10]
            for idx, inst in enumerate(scenario.train_insts):
                logger.debug('%d: %s' % (idx, inst))
            scenario.wallclock_limit = 30

        all_rh = RunHistory(aggregate_func=average_portfolio_cost)
        scenario.output_dir += '_%d' % HYDRA_ROUND
        outerStats = Stats(scenario)
        outerStats.start_timing()
        stats = Stats(scenario)

        processes = []
        scens = []
        start_time = time.time()
        for pnum in range(args.cores):
            tmp_scen = copy.deepcopy(scenario)
            tmp_scen.output_dir += '_%d' % pnum
            scens.append(tmp_scen)
            p = Process(target=configure, args=(tmp_scen, rng.randint(9999999),
                                                average_portfolio_cost, Stats(tmp_scen), logger, INST_SEEDS))
            p.start()
            processes.append(p)
        for p in processes:
            p.join()

        all_res = []
        for pnum in range(args.cores):
            incumbent, cost = read_traj_file(scenario.cs, os.path.join(scens[pnum].output_dir, 'traj_aclib2.json'))
            logger.debug(incumbent)
            logger.debug(cost)
            logger.debug(':'*120)
            all_rh.update_from_json(os.path.join(scens[pnum].output_dir, 'runhistory.json'),
                                    scenario.cs, origin=DataOrigin.INTERNAL)
            all_res.append((cost, incumbent))
        all_res = sorted(all_res, key=lambda x: x[0])
        logger.debug(str(all_res))

        logger.info('Initial run done. Modifying metric')
        for k in range(args.to_keep):
            logger.debug('_-_'*40)
            logger.debug('Keeping:')
            logger.debug(str(all_res[k][1]))
            logger.debug(str(all_res[k][0]))
            portfolio.append(all_res[k][1])

        new_metric = partial(average_portfolio_cost, portfolio=portfolio, portfolio_runhistory=all_rh)
        logger.info('~*~'*40)
        round_info.append(time.time() - start_time)
        mean_time = np.mean(round_info)
        logging.info('Mean_time: %f' % mean_time)
        save_meta_info(all_rh, HYDRA_ROUND + 1, outerStats, round_info,
                       rng, scenario, portfolio, new_metric, INST_SEEDS)
        start = 0
        if (outerStats.get_used_wallclock_time() >= args.allowed_max_time) or (
                    outerStats.get_used_wallclock_time() + 1.5*mean_time) >= args.allowed_max_time:
            logging.warning('No time left to run continously! Start from last checkpoint to complete run!')
            skip = True
    else:
        logging.basicConfig(level=args.verbosity,
                            format="%(asctime)s:%(processName)s:%(levelname)s:%(name)s:%(message)s",
                            datefmt="%Y-%m-%d %H:%M:%S")
        name = 'piac'
        logger = logging.getLogger('%s_MAIN' % name.upper())
        args.working_dir = os.path.abspath(args.working_dir)
        assert os.path.exists(args.working_dir), 'The scenario you want to continue does not exist!'
        os.chdir(args.working_dir)
        assert os.path.exists('.meta_info'), '.meta_info folder missing'
        out_dir = "HYDRA-output_%s" % (
                datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S'))

        with open(os.path.join('.meta_info', 'round_info.json'), 'r') as mf:
            round_dict = json.load(mf)
        HYDRA_ROUND = round_dict['HYDRA_ROUND']
        round_info = round_dict['round_info']
        with open(os.path.join('.meta_info', 'rng.pkl'), 'rb') as pf:
            rng = pickle.load(pf)
        with open(os.path.join('.meta_info', 'scenario.pkl'), 'rb') as sf:
            scenario = pickle.load(sf)
        with open(os.path.join('.meta_info', 'portfolio.pkl'), 'rb') as pf:
            portfolio = pickle.load(pf)
        with open(os.path.join('.meta_info', 'metric.pkl'), 'rb') as pf:
            new_metric = pickle.load(pf)
        with open(os.path.join('.meta_info', 'inst_seeds.pkl'), 'rb') as pf:
            INST_SEEDS = pickle.load(pf)
        all_rh = RunHistory(aggregate_func=average_portfolio_cost)
        all_rh.load_json('overall_rh.json', scenario.cs)
        start = HYDRA_ROUND
        outerStats = Stats(scenario)
        outerStats.start_timing()

    for i in range(start, args.n_runs):
        if skip:
            break
        logger.info('Starting new optimization pass')
        scenario.output_dir = out_dir
        HYDRA_ROUND += 1
        scenario.output_dir += '_%d' % HYDRA_ROUND

        processes = []
        scens = []
        start_time = time.time()
        for pnum in range(args.cores):
            tmp_scen = copy.deepcopy(scenario)
            tmp_scen.output_dir += '_%d' % pnum
            scens.append(tmp_scen)
            p = Process(target=configure, args=(tmp_scen, rng.randint(9999999),
                                                new_metric, Stats(tmp_scen), logger, INST_SEEDS))
            p.start()
            processes.append(p)
        for p in processes:
            p.join()

        all_res = []
        for pnum in range(args.cores):
            incumbent, cost = read_traj_file(scenario.cs, os.path.join(scens[pnum].output_dir, 'traj_aclib2.json'))
            logger.debug(incumbent)
            logger.debug(cost)
            logger.debug(':'*120)
            all_rh.update_from_json(os.path.join(scens[pnum].output_dir, 'runhistory.json'),
                                    scenario.cs, origin=DataOrigin.INTERNAL)
            all_res.append((cost, incumbent))
        all_res = sorted(all_res, key=lambda x: x[0])
        logger.debug(str(all_res))

        for k in range(args.to_keep):
            logger.debug('_-_'*40)
            logger.debug('Keeping:')
            logger.debug(str(all_res[k][1]))
            logger.debug(str(all_res[k][0]))
            portfolio.append(all_res[k][1])

        logger.info('~*~'*40)
        logger.info('Run #{:>3d} done. Modifying metric.'.format(HYDRA_ROUND))
        new_metric = partial(average_portfolio_cost, portfolio=portfolio, portfolio_runhistory=all_rh)
        logger.info('~*~'*40)
        round_info.append(time.time() - start_time)
        mean_time = np.mean(round_info)
        logging.info('Mean_time: %f' % mean_time)
        save_meta_info(all_rh, HYDRA_ROUND, outerStats, round_info,
                       rng, scenario, portfolio, new_metric, INST_SEEDS)
        if (outerStats.get_used_wallclock_time() >= args.allowed_max_time) or (outerStats.get_used_wallclock_time()
                                                                               + mean_time) >= args.allowed_max_time:
            logging.warning('No time left to run continously! Start from last checkpoint to complete run!')
            break
    logger.debug('Size rh: %d' % len(all_rh.data))
    all_rh.save_json('overall_rh.json', save_external=True)
    save_meta_info(all_rh, HYDRA_ROUND, outerStats, round_info,
                   rng, scenario, portfolio, new_metric, INST_SEEDS)
    with open('hydra_port.pkl', 'wb') as out_port:
        pickle.dump(portfolio, out_port)
    for r in round_info:
        logging.info(r)


def cmd_line_call():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('scenario_file', help='Path to the scenario File')
    parser.add_argument('working_dir', help='Directory to run in')
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=['INFO', 'DEBUG'])
    parser.add_argument('-s', '--seed', default=None, help='Seed', type=int)
    parser.add_argument('-d', '--debug', action='store_true', help='Special Debugging flag')
    parser.add_argument('-n', '--n_runs', default=10, help='Number of HYDRA iterations', type=int)
    parser.add_argument('-c', '--cores', default=8, help='Number of parallel SMAC executions', type=int)
    parser.add_argument('-k', '--keep', default=1, dest='to_keep', help='How many configurations to keep each round',
                        type=int)
    parser.add_argument('-r', '--resources', default=None, dest='resource_dir', help='Dir to load resources from.' \
                        ' If none is specified, the call directory will be used!'
                        ' !!!REQUIRES ABSOLUTE PATHS!!!')
    parser.add_argument('--allowed_max_time', default=np.float('inf'),
                        type=float, help='Allowed max time before having to restart'
                        ' from last checkpoint')
    parser.add_argument('--continue', action='store_true', help='Continue from checkpoint', dest='cont')
    parser.add_argument('--use_full_train', action='store_false', help='Continue from checkpoint', dest='valid_small')

    args, unknown = parser.parse_known_args()
    if args.seed is None:
        args.seed = int.from_bytes(os.urandom(4), byteorder="big")
    hydra(args=args)


if __name__ == '__main__':
    cmd_line_call()
