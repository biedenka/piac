import datetime
import logging
import glob
import multiprocessing
import os
import pickle
import time
from copy import deepcopy
from typing import List

import numpy as np
from sklearn.metrics import euclidean_distances

from smac.optimizer.objective import average_cost
from smac.runhistory.runhistory import RunHistory
from smac.scenario.scenario import Scenario
from smac.stats.stats import Stats as SMACstats

from kraken.configspace.partition_config_space import GMeans
from kraken.piac.kraken import setup_experiment_outdir
from kraken.scenario.preprocessor import Preprocessor
from kraken.util.n_ary_tree import TreeNode
from kraken.util.stats import PIACStats
from kraken.util.utils import get_optimizer, read_traj_file

THREADS = 8
SEMAPHORE = multiprocessing.Semaphore(THREADS)
ISAC_STATS = []


def get_isac_conf(partition_list: List[TreeNode], scenario: Scenario, inst: str):
    """
    Out of all partitions / clusters found with isac, determine which to apply for a new instance
    :param partition_list: list of TreeNode objects that contain all the ISAC training results
    :param scenario: SMACScenario that contains all infos about the instances
    :param inst: instance_id (as str)
    :return:
    """
    global ISAC_STATS
    if not ISAC_STATS:
        ISAC_STATS = [0 for _ in partition_list]
    dist_list = [np.float('inf') for _ in partition_list]
    preprocessor = partition_list[0].preprocessor
    inst_feat = preprocessor(scenario.feature_dict[inst].reshape(1, -1))
    for idx, partition in enumerate(partition_list):
        if partition.name != '-1':
            logging.debug('Checking %s' % partition.name)
            tmp_dist = euclidean_distances(inst_feat.reshape(1, -1), partition.centroid.reshape(1, -1)).flatten()[0]
            dist_list[idx] = (tmp_dist, tmp_dist < partition.split_criterion, idx, partition.instances.shape[0])
            logging.debug(dist_list[idx])
            logging.debug(partition.split_criterion)
        else:
            dist_list[idx] = (np.float('inf'), True, idx, len(partition_list[idx].instances))
            partition.split_criterion = np.float('inf')
    dist_list = sorted(dist_list, key=lambda x: x[0])  # type: List[tuple]
    logging.debug('dist_list: %s' % str(dist_list))
    for d in dist_list:
        if d[1]:
            ISAC_STATS[d[2]] += 1
            logging.debug(ISAC_STATS)
            return partition_list[d[2]].configuration, partition_list[d[2]].name


def isac(args):
    if not args.cont:
        logger, rng, scen_file, working_dir, return_to_dir = setup_experiment_outdir(
            scenario_file=args.scenario_file, working_dir=args.working_dir, seed=args.seed, name='piac',
            verbosity=args.verbosity,
            resource_dir=args.resource_dir)
        logger.info('Seed: %d' % args.seed)
        # logger, rng = setup_experiment_outdir(args, 'isac')

        ####################################################################################################################
        # Generate rudimentary objects that keep track of budget and where resources are located
        scenario = Scenario(scenario=scen_file,
                            cmd_options={'output_dir': ""})
        # scenario.output_dir = "ISAC-output_%s" % (
        #     datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S'))
        scenario.output_dir = working_dir
        if scenario.wallclock_limit > args.max_time:
            remaining_time = scenario.wallclock_limit - args.max_time
            scenario.wallclock_limit = args.max_time
        else:
            remaining_time = 0
        preprocessor = Preprocessor(random_state=rng, isac=True)
        store_preprocessor = False
        if scenario.feature_array is not None:
            scenario.feature_array = preprocessor(scenario.feature_array)
            scenario.n_features = scenario.feature_array.shape[1]
            for feat, inst_ in zip(scenario.feature_array, scenario.train_insts):
                scenario.feature_dict[inst_] = feat
            store_preprocessor = True

        ####################################################################################################################
        # This initial cluster will be used to compute a configuration over
        # all instances in case a TEST instance might not fall in a gmeans cluster
        tmp_scen = deepcopy(scenario)
        tmp_scen.output_dir = os.path.join(scenario.output_dir, '-1')
        partition_list = [TreeNode(instance_names=scenario.train_insts,
                                   instances=list(range(len(scenario.train_insts))),
                                   instance_features=scenario.feature_array,
                                   scenario=tmp_scen, name='-1', is_root=False,
                                   depth=-1, split_criterion=np.float('inf'))]
        gmeans = GMeans(scenario=scenario, minimum_samples_per_cluster=int(len(scenario.train_insts)*0.125), rng=rng)

        split_start = time.time()
        label_list = gmeans.partition()
        partition_list.extend(gmeans.nodes)
        for p in partition_list:
            p.scenario.wallclock_limit = scenario.wallclock_limit

        append_ = []
        names = []
        r = 0
        while len(partition_list) < args.cores:     # If more cores might be available
                                                    # larger partitions are configured more often
            if append_ == []:
                partition_list = sorted(partition_list, key=lambda x: len(x.instances), reverse=True)
                diff = args.cores - len(partition_list)
                for a in partition_list[:diff]:
                    append_.append(deepcopy(a))
                for i in range(len(append_)):
                    append_[i].scenario.output_dir = append_[i].scenario.output_dir + '_%d_%d' % (r, i)
                    append_[i].scenario.output_dir_for_this_run = append_[i].scenario.output_dir
                    append_[i].scenario.seed = rng.randint(9999999)
                    names.append(append_[i].name)
            else:
                for idx, a in enumerate(append_):
                    append_[idx] = deepcopy(a)
                for i in range(len(append_)):
                    append_[i].scenario.output_dir = scenario.output_dir + os.path.sep + names[i] + '_%d_%d' % (r, i)
                    append_[i].scenario.output_dir_for_this_run = append_[i].scenario.output_dir
                    append_[i].scenario.seed = rng.randint(9999999)
            partition_list.extend(append_)
            r += 1
        partition_list = sorted(partition_list, key=lambda x: len(x.instances), reverse=True)
        init_rhs = [None for _ in partition_list]
        init_conf = [None for _ in partition_list]

        logger.info('Gmeans found %d centroids' % gmeans.num_clusters)
        for i in range(np.max(label_list) + 1):
            logger.info('%d instances in cluster %d' % (len(label_list[label_list == i]), i))
        logger.info('Min samples required per cluster: %d' % gmeans.minimum_samples_per_cluster)
        if store_preprocessor:
            for idx in range(len(partition_list)):
                partition_list[idx].preprocessor = preprocessor
        logger.debug('Splitting took ~%3.2f minutes' % ((time.time() - split_start) / 60.))

    else:
        logging.basicConfig(level=args.verbosity,
                            format="%(asctime)s:%(processName)s:%(levelname)s:%(name)s:%(message)s",
                            datefmt="%Y-%m-%d %H:%M:%S")
        name = 'piac'
        logger = logging.getLogger('%s_MAIN' % name.upper())
        args.working_dir = os.path.abspath(args.working_dir)
        assert os.path.exists(args.working_dir), 'The scenario you try to load does not exist'
        os.chdir(args.working_dir)
        with open(os.path.join('.meta_info', 'time.pkl'), 'rb') as pf:
            remaining_time = pickle.load(pf)
        with open(os.path.join('.meta_info', 'rng.pkl'), 'rb') as pf:
            rng = pickle.load(pf)
        with open('isac_partition_list.pkl', 'rb') as f:
            partition_list = pickle.load(f)
        with open(os.path.join('.meta_info', 'scenario.pkl'), 'rb') as pf:
            scenario = pickle.load(pf)
        with open(os.path.join('.meta_info', 'args.pkl'), 'rb') as pf:
            args = pickle.load(pf)
        scenario.wallclock_limit = remaining_time
        if scenario.wallclock_limit > args.max_time:
            remaining_time = scenario.wallclock_limit - args.max_time
            scenario.wallclock_limit = args.max_time
        else:
            remaining_time = 0

        init_conf = []
        init_rhs = []
        for p in partition_list:
            p.scenario.wallclock_limit = scenario.wallclock_limit
            logger.info(p.scenario.wallclock_limit)
            init_conf.append(p.configuration)
            all_rh = RunHistory(aggregate_func=average_cost)
            all_rh.load_json(os.path.join(p.scenario.output_dir, 'runhistory.json'), p.scenario.cs)
            init_rhs.append(all_rh)

    # optimize using SMAC/ROAR
    optimize_partitions(args, partition_list=partition_list, rng=rng, init_confs=init_conf, init_rhs=init_rhs)

    for idx, partition in enumerate(partition_list):
        tmp = partition.scenario.output_dir
        logging.debug('LOADING TRAJECTORY FOR PARTITION %s FROM %s' % (partition_list[idx].name, tmp))
        tmp = glob.glob(os.path.join(tmp, '*', 'traj_aclib2.json'), recursive=True)[0]
        partition.configuration, _ = read_traj_file(partition.scenario.cs, tmp)

    for partition in partition_list:
        logging.info(partition.name)
        logging.info(partition.configuration)
    logger.info('Saving partition_list in %s' % os.getcwd())
    save_meta_info(remaining_time, rng, scenario, args)
    with open('isac_partition_list.pkl', 'wb') as partition_file:
        pickle.dump(partition_list, partition_file)


def save_meta_info(remaining_time, rng, scenario, args):
    if not os.path.exists('.meta_info'):
        os.mkdir('.meta_info')
    with open(os.path.join('.meta_info', 'time.pkl'), 'wb') as pf:
        pickle.dump(remaining_time, pf)
    with open(os.path.join('.meta_info', 'rng.pkl'), 'wb') as pf:
        pickle.dump(rng, pf)
    with open(os.path.join('.meta_info', 'scenario.pkl'), 'wb') as pf:
        pickle.dump(scenario, pf)
    with open(os.path.join('.meta_info', 'args.pkl'), 'wb') as pf:
        pickle.dump(args, pf)


def _optimize_partition(args, partition: TreeNode, rng: np.random.RandomState, parallel: bool = False, init_conf=None,
                        init_runhist=None):
    with SEMAPHORE:
        stats = SMACstats(partition.scenario)
        stats.start_timing()
        runhist = RunHistory(aggregate_func=average_cost)
        optimizer = get_optimizer(args.tae, args.modus, scenario=partition.scenario, runhist=runhist, stats=stats,
                                  rng=rng, init_conf=init_conf, init_runhist=init_runhist)
        logger = logging.getLogger('OptimizerCall')
        logger.critical('PARALLELLLLLLL: %s\t%s' % (str(parallel), partition.name))

        incumbent = optimizer.optimize()
        optimizer.runhistory.save_json(save_external=True)
        return optimizer.runhistory, optimizer.solver.stats


def _sequentially_optimize_partitions(args, partition_list: list, rng: np.random.RandomState,
                                      master_stats_object: PIACStats = None):
    """
    Sequential optimization loop, for args see optimize_partitions
    """
    logger = logging.getLogger('SequentialCallToSMAC')
    for id, partition in enumerate(partition_list):
        p_runhist, p_stats = _optimize_partition(args, partition, rng, parallel=False)
        try:
            master_stats_object.update(p_stats)
        except TypeError:
            logger.warning('No master_stats_object specified!')
    logger.debug('Finished sequential optimization')


def _optimize_partitions_in_parallel(args, partition_list: list, rng: np.random.RandomState,
                                     init_rhs: list, init_confs: list):
    """
    Parallel use of optimizers, for args, see optimize_partitions
    """
    processes = []
    for partition, rh, conf in zip(partition_list, init_rhs, init_confs):
        p = multiprocessing.Process(target=_optimize_partition, args=(args, partition, rng, True, conf, rh))
        p.daemon = True
        p.start()
        processes.append(p)
    for p in processes:
        p.join()


def optimize_partitions(args, partition_list: list,
                        rng: np.random.RandomState,
                        master_stats_object: PIACStats = None, init_rhs = None, init_confs = None):
    """
    Method to use SMAC/ROAR to optimize on the resulting partitions
    :param args: argpars_object used to determine if SMAC or ROAR is used
    :param partition_list: all leaf nodes of the partition Tree, to optimize on
    :param rng: rng for SMAC/ROAR
    :param master_stats_object: Stats object to keep track of the overall budget spent
    """
    if THREADS is None:
        _sequentially_optimize_partitions(args=args,
                                          partition_list=partition_list,
                                          rng=rng, master_stats_object=master_stats_object)
    else:
        _optimize_partitions_in_parallel(args=args,
                                         partition_list=partition_list,
                                         rng=rng, init_rhs=init_rhs, init_confs=init_confs)


def cmd_line_call():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('scenario_file', help='Path to the scenario File')
    parser.add_argument('working_dir', help='Directory to run in')
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=['INFO', 'DEBUG'])
    parser.add_argument('-s', '--seed', default=None, help='Seed', type=int)
    parser.add_argument('--configurator', dest='modus', default='SMAC', choices=['SMAC', 'ROAR'], type=str)
    parser.add_argument('-c', '--cores', default=-1, type=int, help="How many cores ISAC is allowed to use. If it"
                        "finds more partitions, it ignores this value!!! If it finds less, it sorts the partitions by"
                        "size and configures the larger partitions on the available extra cores")
    parser.add_argument('-r', '--resources', default=None, dest='resource_dir', help='Dir to load resources from.' \
                        ' If none is specified, the call directory will be used!'
                        ' !!!REQUIRES ABSOLUTE PATHS!!!')
    parser.add_argument('--continue', dest='cont', help='Continue', action='store_true')
    parser.add_argument('--max_time', type=float, default=np.float('inf'))

    args, unknown = parser.parse_known_args()
    if args.seed is None:
        args.seed = int.from_bytes(os.urandom(4), byteorder="big")
    isac(args=args)


if __name__ == '__main__':
    cmd_line_call()
