import os
import sys
import time
import json
import copy
import pickle
import logging
import datetime
from typing import Union, List, Dict
from multiprocessing import Process
import multiprocessing

import numpy as np

from smac.scenario.scenario import Scenario
from smac.stats.stats import Stats as SMACstats
from smac.runhistory.runhistory import RunHistory, DataOrigin
from smac.optimizer.objective import average_cost
from smac.utils.io.traj_logging import TrajLogger
from smac.utils.util_funcs import get_types
from smac.runhistory.runhistory2epm import RunHistory2EPM4LogCost, RunHistory2EPM4Cost
from smac.epm.rfr_imputator import RFRImputator

from kraken.epm.rf_with_instances import RandomForestWithInstances
from kraken.tae import StatusType
from kraken.util.stats import PIACStats
from kraken.util.n_ary_tree import TreeNode
from kraken.configspace.partition_config_space import CSHCNoEPM as CSHC
from kraken.optimization.improvement_profiler import ImpProfiler
from kraken.scenario.preprocessor import Preprocessor
from kraken.util.utils import scenario_option_names, PIACBudgetExhaustedException
from kraken.util.utils import refit_model, get_optimizer, get_tae, construct_scenario
from kraken.configspace import Configuration

RESTARTS = 2
MAX_SEARCH_STEPS = 3
MAX_NEIGHBOURS = 5
CSHC_BUDGET = 60 * 60  # 45 min in sec


def trickle_down_tree(node: TreeNode, inst_feat_dict: dict,
                      instance: str, depth: int = 0) -> (Configuration, str):
    """
    Recursive method that climbs up the tree to determine which configuration to apply to a given instance
    :param node: n_ary_tree
    :param inst_feat_dict: Dictionary with instance_name -> instance_feature mapping
    :param instance: instance_name to lookup features in inst_feat_dict
    :param depth: Used for debug log output
    :return: (Configuration to apply to instance, Name of cluster this configuration belongs to)
    """
    if node.split_criterion:
        go_left = node.split_criterion(inst_feat_dict, instance)
        if go_left and node.children[0].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 0))
            return trickle_down_tree(node.children[0], inst_feat_dict, instance, depth + 1)
        elif go_left and node.children[1].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 1))
            return trickle_down_tree(node.children[1], inst_feat_dict, instance, depth + 1)
        if not go_left and not node.children[0].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 0))
            return trickle_down_tree(node.children[0], inst_feat_dict, instance, depth + 1)
        elif not go_left and not node.children[1].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 1))
            return trickle_down_tree(node.children[1], inst_feat_dict, instance, depth + 1)
    else:
        logging.debug('Found Configuration at depth {:>3d}'.format(depth))
        return node


def inc():
    """
    Generator that generates the scaling factor of the configuration budget in budget mode 'inc' for at most 10
    iterations.
    """
    start = 0.25
    i = 0
    while i < 20:
        yield 2 ** (start * i) - (1 - start)
        i += 1


def setup_experiment_outdir(scenario_file: str, working_dir: str = 'piac', seed: int = 12345, name: str = 'piac',
                            verbosity: Union[int, int, int] = logging.INFO,
                            resource_dir: Union[str, None] = None) -> (logging.Logger, np.random.RandomState):
    """
    Function to prepare the output directory for the experiment
    :scenario_file: str path to the scenario file to load
    :working_dir: str name of the working directory
    :seed: int Seed used
    :name: str Name of the root logger
    :resource_dir: If not none, used to load the data from the specified dir.
    :verbosity: logging level
    """
    logging.basicConfig(level=verbosity, format="%(asctime)s:%(processName)s:%(levelname)s:%(name)s:%(message)s",
                        datefmt="%H:%M:%S")
    logger = logging.getLogger('%s_MAIN' % name.upper())

    return_to_dir = os.getcwd()

    resource_dir = resource_dir if resource_dir else return_to_dir

    scenario_file = os.path.abspath(scenario_file)
    time_str = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S_(%f)')
    original_work_dir = working_dir
    working_dir = '_'.join((original_work_dir, time_str, 'run' + str(seed)))
    not_created = True
    if not os.path.exists(working_dir) and not os.path.isdir(working_dir):
        while not_created:
            try:
                os.mkdir(working_dir)
            except FileExistsError:
                time_str = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')
                working_dir = '_'.join((original_work_dir, time_str, 'run' + str(seed)))
            else:
                not_created = False
    out_f = open(os.path.join(working_dir, 'scenario.txt'), 'w')
    with open(scenario_file) as in_fh:
        for line in in_fh:
            out_f.write(line)
            header, line = line.strip().split('=')
            header = header.strip()
            line = line.strip()
            if scenario_option_names[header] in ['paramfile', 'feature_file', 'instance_file', 'test_instance_file']:
                fn = line.split(os.path.sep)
                while fn[0].strip() == '.':
                    del fn[0]
                fn = fn[0].strip()
                try:
                    os.symlink(os.path.join(resource_dir, fn), os.path.join(working_dir, fn))
                except FileExistsError:
                    logger.debug('Not creating symlink for %s' % fn)
            elif scenario_option_names[header] in ['algo']:
                if len(line.split(' ')) > 1:
                    fn = line.split(' ')
                    for tmp_fn in fn:
                        if os.path.sep not in tmp_fn:
                            continue
                        else:
                            fn = tmp_fn
                            break
                if os.path.sep in fn:
                    tmp_fn = fn.split(os.path.sep)
                    if tmp_fn[0] == '.':
                        del tmp_fn[0]
                    fn = tmp_fn[0]
                try:
                    os.symlink(os.path.join(resource_dir, fn), os.path.join(working_dir, fn))
                except FileExistsError:
                    logger.debug('Not creating symlink for %s' % fn)
    out_f.close()
    os.chdir(os.path.abspath(working_dir))
    # with open('%s_args.json' % name.lower(), 'w') as out_f:
    #     json.dump(__dict__, out_f, sort_keys=True, indent=4, separators=(',', ': '))
    rng = np.random.RandomState(seed)
    return logger, rng, scenario_file, working_dir, return_to_dir


def _timing(per_partition_time: float, budget_strat: str, save_rounds: bool,
            runs: List[float], round_count: int, splitting_time: float,
            stats: PIACStats, partition_tree: TreeNode) -> (bool, float):
    """
    Deprecated!!!
    :param per_partition_time:
    :param budget_strat:
    :param save_rounds:
    :param runs:
    :param round_count:
    :param splitting_time:
    :param stats:
    :param partition_tree:
    :return:
    """
    budget = per_partition_time if budget_strat != 'inc' else runs[min(round_count - 1, len(runs) - 1)]
    budget += splitting_time
    if per_partition_time + splitting_time > stats.get_remaing_time_budget():
        if partition_tree is None:
            per_partition_time = stats.get_remaing_time_budget() * 0.75
            logging.warning('RUNNING *FIRST* OPTIMIZATION WITH SHORTER BUDGET')
            logging.warning('SINCE NOT ENOUGH BUDGET REMAINING FOR SPECIFIED BUDGET')
        elif save_rounds and stats.get_remaing_time_budget() / per_partition_time > 0.75:
            logging.warning('RUNNING *LAST* OPTIMIZATION WITH SHORTER BUDGET')
            logging.warning('SINCE NOT ENOUGH BUDGET REMAINING FOR FINAL PASS')
            per_partition_time = stats.get_remaing_time_budget()
        else:
            logging.warning('NOT ENOUGH BUDGET REMAINING FOR OPTIMIZATION PASS')
            return False, per_partition_time
    return True, per_partition_time


def exploration(exploration_time_budget, scenario: Scenario, stats: PIACStats, tae_str: str, rng: np.random.RandomState,
                per_partition_time: float, init_rand_exploration_evaluations: int, init_default_evaluations: int,
                exploration_evaluations: int, insts_for_PEI: int, only_explore_best: bool, round_info: List[list],
                logger):
    ################################################################################################################
    # Initialize the target algorithm executor which will track empirical performance and which configs where run on
    # which instances
    runhist = RunHistory(aggregate_func=average_cost)
    tae = get_tae(tae_str, scenario, runhist, stats)

    ################################################################################################################
    # Use RandomSearch and Profile Expected Improvement explore the joint config/instance space
    # Use data to construct diverse EPM to map to the performance space
    profiler = ImpProfiler(scenario=scenario, tae=tae, stats=stats, rng=rng,
                           per_partition_time=per_partition_time)
    exploration_start = time.time()
    model = profiler.explore(
        initial_runs=init_rand_exploration_evaluations, initial_def_runs=init_default_evaluations,
        exploration_evaluations=exploration_evaluations, num_instances_per_round=insts_for_PEI,
        only_eval_best=only_explore_best)
    logger.debug('Exploration done!')
    exploration_time = time.time() - exploration_start
    round_info.append([0, exploration_time, exploration_time_budget])
    stats.set_algo_runs_timelimit(scenario.algo_runs_timelimit)
    stats.set_ta_run_limit(scenario.ta_run_limit)
    stats.print_stats()
    return model, profiler.runhist, profiler.rh2EPM, round_info, profiler.partition_list, profiler.split_time, \
           profiler.round_instances, profiler.round_inst_ids


def setup_preproc(rng: np.random.RandomState, scenario: Scenario, preprocess: bool=True):
    preprocessor = Preprocessor(rng)
    store_preprocessor = False
    if scenario.feature_array is not None and preprocess:
        scenario.feature_array = preprocessor(scenario.feature_array)
        scenario.n_features = scenario.feature_array.shape[1]
        for feat, inst_ in zip(scenario.feature_array, scenario.train_insts):
            scenario.feature_dict[inst_] = feat
        store_preprocessor = True
    return store_preprocessor, scenario, preprocessor


def get_optimization_times(optimization_time: float):
    runs = []
    for idx, i in enumerate(inc()):
        runs.append(i * optimization_time)
    return runs


def fit_to_cores(partition_list, n_threads, scenario):
    # Making sure all available cores are used
    append_ = []  # type: List[TreeNode]
    names = []
    diff = 0
    r = 0
    while len(partition_list) < n_threads:  # If more cores might be available
        # larger partitions are configured more often
        if append_ == []:
            partition_list = sorted(partition_list, key=lambda x: len(x.instances), reverse=True)
            diff = n_threads - len(partition_list)
            for a in partition_list[:diff]:
                append_.append(copy.deepcopy(a))
            for i in range(len(append_)):
                append_[i].scenario.output_dir = append_[i].scenario.output_dir + '_%d_%d' % (r, i)
                append_[i].name += '_%d_%d' % (r, i)
                names.append(append_[i].name)
        else:
            for idx, a in enumerate(append_):
                append_[idx] = copy.deepcopy(a)
            for i in range(len(append_)):
                append_[i].scenario.output_dir = scenario.output_dir + os.path.sep + names[i] + '_%d_%d' % (
                    r, i)
        partition_list.extend(append_)
        r += 1
    partition_list = sorted(partition_list, key=lambda x: len(x.instances), reverse=True)
    part_at_dict = dict([(partition.name, idx) for idx, partition in enumerate(partition_list)])
    return partition_list, part_at_dict


def select_config_for_tree(partition_list, partition_tree, valid_res, part_at_dict):
    logging.info('+' * 120)
    inst_part = dict()
    configs = []
    for instance in valid_res:
        for config in valid_res[instance]:
            if config not in configs:
                configs.append(config)
            valid_res[instance][config] = np.mean(valid_res[instance][config])
        treeNode = trickle_down_tree(partition_tree, partition_tree.instance_features,
                                     instance)
        try:
            inst_part[treeNode.name].append(instance)
        except KeyError:
            inst_part[treeNode.name] = [instance]
    logging.info(inst_part)
    for part in sorted(inst_part, reverse=True):
        logging.info(';' * 120)
        best = None
        best_conf = None
        idf = -1
        for idx, config in enumerate(configs):
            logging.debug('%d Checking config:\n%s' % (idx, str(config)[:80] + '...' + str(config)[-80:]))
            vals = []
            for i in inst_part[part]:
                vals.append(valid_res[i][config])
            n_mean = np.mean(vals)
            if best is None or n_mean < best:
                logging.debug('Keeping with mean %f' % n_mean)
                best = n_mean
                best_conf = config
                idf = partition_list[part_at_dict[part]].instance_names[0]
            else:
                logging.debug('Rejecting with mean %f' % n_mean)
        treeNode = trickle_down_tree(partition_tree, partition_tree.instance_features, idf)
        logging.info(treeNode.name)
        logging.info(treeNode.configuration)
        treeNode.configuration = None
        treeNode.configuration = best_conf
        logging.info(treeNode.configuration)
    return partition_tree


def ensure_budget_is_not_overspend(optimization_time, splitting_time, runs, partition_list, stats, round_info,
                                   budget_strat, round_count, scenario):
    expected_time_to_take = (optimization_time - splitting_time)
    logging.info(str(runs))
    if budget_strat == 'inc':
        expected_time_to_take += runs[min(round_count + 1, len(runs) - 1)]  # will contain the time for split!
    else:
        expected_time_to_take += (optimization_time - splitting_time)
    r_budget = stats.get_remaing_time_budget()
    for p in partition_list:
        logging.debug(';´;´' * 30)
        logging.debug(p.name)
        logging.debug('Scen_wallclock: %s' % str(p.scenario.wallclock_limit))
        logging.debug('Expected time to take: %s' % str(expected_time_to_take))
        if expected_time_to_take < stats.get_remaing_time_budget():
            p.scenario.wallclock_limit = min(abs(optimization_time - splitting_time), r_budget)
            logging.debug('Scen_wallclock: %s' % str(p.scenario.wallclock_limit))
        else:
            p.scenario.wallclock_limit = r_budget
            logging.debug('Scen_wallclock: %s' % str(p.scenario.wallclock_limit))
        if p.scenario.wallclock_limit * 2 > stats.get_remaing_time_budget():  # Just to make sure!
            p.scenario.wallclock_limit = stats.get_remaing_time_budget()
            logging.debug('Scen_wallclock: %s' % str(p.scenario.wallclock_limit))
        p.scenario.cs = scenario.cs  # Just to make sure cs is never None
        if p.scenario.wallclock_limit <= 0:
            raise PIACBudgetExhaustedException
    round_info.append([splitting_time, p.scenario.wallclock_limit, optimization_time])
    return expected_time_to_take, partition_list


def setup_scenario_per_partition(partition_list, scenario, optimization_time, runs, round_count, stats):
        for partition in partition_list:
            partition.scenario = construct_scenario(scenario, partition, optimization_time)
            if optimization_time + runs[min(round_count + 1, len(runs) - 1)] < stats.get_remaing_time_budget():
                partition.scenario.wallclock_limit = optimization_time
                logging.debug('Scen_wallclock: %s' % str(partition.scenario.wallclock_limit))
            else:
                partition.scenario.wallclock_limit = stats.get_remaing_time_budget()
                logging.debug('Scen_wallclock: %s' % str(partition.scenario.wallclock_limit))
            if partition.scenario.wallclock_limit * 2 > stats.get_remaing_time_budget():  # Just to make sure!
                partition.scenario.wallclock_limit = stats.get_remaing_time_budget()
                logging.debug('Scen_wallclock: %s' % str(partition.scenario.wallclock_limit))
            partition.scenario.cs = scenario.cs  # Just to make sure cs is never None
        return partition_list


def get_contributing_incumbents(valid_res, incumbent_list):
    # compute contribution set
    contribution_set = []
    for inst in valid_res:
        min_ = float('inf')
        min_idx = -1
        for idx, c in enumerate(incumbent_list):
            if np.mean(valid_res[inst][c]) < min_:
                min_ = np.mean(valid_res[inst][c])
                min_idx = idx
        if min_idx not in contribution_set:
            contribution_set.append(min_idx)
            if len(contribution_set) == len(incumbent_list):
                break
    del_at = 0
    while del_at < len(incumbent_list):  # remove all non_contributing configurations
        if del_at not in contribution_set:
            del incumbent_list[del_at]
        else:
            del_at += 1
    return incumbent_list


def get_validation_data(validation_set, scenario, exploration_instances):
    del_at = 0
    validation_features = []
    validation_feature_dict = {}
    validation_instances = exploration_instances
    if validation_set == 'EXP':
        # This ensures that validation instances are no longer in TRAIN
        while del_at < len(scenario.train_insts):
            if scenario.train_insts[del_at] in exploration_instances:
                validation_feature_dict[
                    scenario.train_insts[del_at]] = scenario.feature_dict[scenario.train_insts[del_at]]
                del scenario.train_insts[del_at]
                validation_features.append(scenario.feature_array[del_at])
                scenario.feature_array = np.delete(scenario.feature_array, del_at, axis=0)
            else:
                del_at += 1
        validation_features = np.array(validation_features)
        logging.debug('%d instances remain in training. Using %d instances for validation' % (
            del_at, len(exploration_instances)
        ))
    elif validation_set == 'TRAIN':
        validation_instances = copy.deepcopy(scenario.train_insts)
        validation_feature_dict = copy.deepcopy(scenario.feature_dict)
        validation_features = copy.deepcopy(scenario.feature_array)
    else:
        raise NotImplementedError
    return validation_instances, validation_feature_dict, validation_features


def kraken(scen_file: str, per_partition_time: float, working_dir: str = 'piac', seed: Union[int, None] = None,
           verbosity: int = logging.INFO, exploration_time_budget: float = 1.0,
           init_rand_exploration_evaluations: int = 10, init_default_evaluations: int = 10,
           exploration_evaluations: int = 10, insts_for_PEI: int = -1,
           max_num_partitions: int = 8, tae_str: str = 'aclib', save_rounds: bool = False,
           min_partition_size: int = 1, regularize: bool = True, modus: str = 'SMAC', only_known_configs: bool = True,
           budget_strat: str = 'inc', only_explore_best: bool = True, n_rand_splits: int = 1000,
           resource_dir: Union[str, None] = None, available_cores: int = -1,
           time_allowed_to_run_continously: float = -1, validation_set: str='EXP', preprocess: bool=False):
    """
    Main method of the PIAC module
    :param scen_file: scenario file to load
    :param per_partition_time: time in seconds used for optimization on each partition
    :param working_dir: the directory to work in.
    :param seed: random seed
    :param verbosity: log level
    :param exploration_time_budget: budget in seconds used for exploration
    :param init_rand_exploration_evaluations: number of random config instance pairs that are evaluated during
           exploration
    :param init_default_evaluations: number of default config instance pairs used for evaluation
    :param exploration_evaluations: max number of exploration evaluations
    :param insts_for_PEI: number of instances that are sampled during exploration
    :param max_num_partitions: max allowed number of partitions. (recommended to be set to max_num_cores)
    :param tae_str: which target_algorithm_evaluater to use [aclib or old]
    :param save_rounds: save the partition tree after each iteration
    :param min_partition_size: min number of instances per partition
    :param regularize: If set to true, balanced splits are favored
    :param modus: Which configurator to use [i.e. SMAC or ROAR]
    :param only_known_configs: only consider configurations that were evaluated on at least one configurations.
           Recomended to be set to true as otherwise local search will be used to determine configurations. If the
           model has not seen enough data this might find bad configurations and therefor bad splits.
    :param budget_strat: either 'inc' or 'equal'. If set to inc, an incresing budget will be used. otherwise each
            iteration equal configuration budgets
    :param only_explore_best Flag that regulates how the exploration works. (i.e. True=> algorithm by Bossek et al.,
           2015 [LEARNING FEATURE-PARAMETER MAPPINGS FOR PARAMETER TUNING VIA THE PROFILE EXPECTED IMPROVEMENT],
           False=> a slightly modified version)
    :param n_rand_splits Integer that defines how many random splits should be evaluated.
    :param resource_dir If not none this is used as reference dir to load necessary data
    :param time_allowed_to_run_continously float that specifies how long Kraken can run before it has to checkpoint,
           from which point on it shall be able to continue running.
    :return: None. Everything is saved in working_dir. To use the learned selector/model, load the partition_tree.pkl
             file (read mode 'rb').
    """
    if seed is None:
        seed = int.from_bytes(os.urandom(4), byteorder="big")
    logger, rng, scen_file, working_dir, return_to_dir = setup_experiment_outdir(
        scenario_file=scen_file, working_dir=working_dir, seed=seed, name='piac', verbosity=verbosity,
        resource_dir=resource_dir)
    logger.info('Seed: %d' % seed)
    round_info = []

    try:
        ################################################################################################################
        # Generate rudimentary objects that keep track of budget and where resources are located
        scenario = Scenario(scenario=scen_file,
                            cmd_args={'output_dir': ""})
        scenario.output_dir = "PIAC-output_%s" % (
            datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S'))

        # num_instances = len(scenario.feature_dict.keys())
        stats = PIACStats(wallclock_limit=scenario.wallclock_limit, algo_runs_time_limit=exploration_time_budget)
        stats.start_timing()

        store_preprocessor, scenario, preprocessor = setup_preproc(rng, scenario, preprocess)
        runs = get_optimization_times(per_partition_time)
        if budget_strat == 'equal':
            runs = [per_partition_time for _ in range(len(runs))]

        model, all_runs_runhist, rh2EPM, round_info, partition_list, split_time, exploration_instances, \
            exploration_inst_id = exploration(
                exploration_time_budget, scenario, stats, tae_str, rng, per_partition_time,
                init_rand_exploration_evaluations, init_default_evaluations, exploration_evaluations,
                insts_for_PEI, only_explore_best, round_info, logger
            )

        validation_instances, validation_feature_dict, validation_features = get_validation_data(
            validation_set, scenario, exploration_instances
        )

        ################################################################################################################
        # Main Loop:
        # While budget available, use CSHC to partition instances that perform similar under given config
        # use SMAC/ROAR to optimize on these partitions, until no budget left or no change in partitions
        n_threads = max(max(max_num_partitions, 1), available_cores)
        partition_tree = None  # type: Union[None, TreeNode]
        round_count = 0
        if n_threads <= 1:
            parallel = None
        else:
            parallel = n_threads
        splitting_time = 0
        valid_res = {}
        incumbent_list = []

        while not stats.is_budget_exhausted():
            start_opt = time.time()
            optimization_time = runs[min(round_count, len(runs) - 1)]
            partition_list = setup_scenario_per_partition(partition_list, scenario, optimization_time, runs,
                                                          round_count, stats)
            partition_list, part_at_dict = fit_to_cores(partition_list, n_threads, scenario)
            stats.print_stats()
            # optimize using SMAC/ROAR
            all_runs_runhist, partition_list, valid_res, incumbent_list = optimize_partitions(
                tae_str, modus, partition_list=partition_list, init_runhist=all_runs_runhist, valid_res=valid_res,
                threads=parallel, master_stats_object=stats, use_pred_start=True, rng=rng,
                valid_insts=validation_instances, partition_inst_dict=part_at_dict, incumbent_list=incumbent_list
            )

            opt_valid_time = time.time() - start_opt
            split_start = time.time()
            logging.debug('Current number of incumbents: %d' % len(incumbent_list))
            incumbent_list = get_contributing_incumbents(valid_res, incumbent_list)
            logging.debug('Current number of contributing incumbents: %d' % len(incumbent_list))
            cshc = CSHC(scenario, np.random.RandomState(12345),
                        min_partition_size=min_partition_size, max_partitions=max_num_partitions,
                        random_splits=n_rand_splits, regularize=regularize, incumbent_list=incumbent_list,
                        validation_results=valid_res, validation_instances=validation_instances,
                        validation_features=validation_features)
            try:
                partition_list, partition_tree = cshc.partition(stats)
            except PIACBudgetExhaustedException:
                pass
            splitting_time = time.time() - split_start
            round_info.append([splitting_time, opt_valid_time, optimization_time])

            if partition_tree is not None:
                if store_preprocessor:
                    partition_tree.preprocessor = preprocessor
                stats.print_stats()
                if save_rounds:
                    save_meta_info(preprocessor, store_preprocessor, all_runs_runhist, round_count + 1,
                                   runs, stats, round_info,
                                   rng, scenario, splitting_time, validation_instances, exploration_inst_id, valid_res,
                                   validation_features, incumbent_list, partition_list)
                    partition_tree.lighten()
                    with open('piac_partition_tree_%s.pkl' % (
                            datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')),
                              'wb') as partition_file:
                        pickle.dump(partition_tree, partition_file)
                if (stats.get_used_wallclock_time() >=
                        time_allowed_to_run_continously) or (stats.get_used_wallclock_time() +
                                                             runs[round_count + 1] +
                                                             splitting_time) >= time_allowed_to_run_continously:
                    logging.warning('No time left to run continuously! Start from last checkpoint to complete run!')
                    break
            round_count += 1

        stats.print_stats()
        partition_tree.print_tree()
        partition_tree.lighten()
        logger.info('Saving tree to piac_partition_tree_final.pkl in %s' % os.getcwd())
        with open('piac_partition_tree_final.pkl', 'wb') as partition_file:
            pickle.dump(partition_tree, partition_file)
        save_meta_info(preprocessor, store_preprocessor, all_runs_runhist, round_count + 1, runs, stats, round_info,
                       rng, scenario, splitting_time, validation_instances, exploration_inst_id, valid_res,
                       validation_features, incumbent_list, partition_list)
    except (RuntimeError, NameError, KeyboardInterrupt):
        logging.exception("Exception occurred in the main method!")
        os.chdir(return_to_dir)
    finally:
        os.chdir(return_to_dir)
        for info in round_info:
            logging.info(info)


def save_meta_info(preprocessor, store_preprocessor, all_runs_runhist, round_count, runs, stats, round_info,
                   rng, scenario, splitting_time, validation_instances, exploration_inst_id, valid_res,
                   validation_features, incumbent_list, partition_list):
    if not os.path.exists('.meta_info'):
        os.mkdir('.meta_info')
    with open(os.path.join('.meta_info', 'preproc.pkl'), 'wb') as pf:
        pickle.dump([preprocessor, store_preprocessor], pf)
    all_runs_runhist.save_json(save_external=True)
    round_dict = {'round_count': round_count, 'runs': runs, 'remaining_time': stats.get_remaing_time_budget(),
                  'used_time': stats.get_used_wallclock_time(), 'round_info': round_info,
                  'splitting_time': splitting_time}
    with open(os.path.join('.meta_info', 'round_info.json'), 'w') as mf:
        json.dump(round_dict, mf)
    with open(os.path.join('.meta_info', 'rng.pkl'), 'wb') as pf:
        pickle.dump(rng, pf)
    with open(os.path.join('.meta_info', 'scenario.pkl'), 'wb') as sf:
        pickle.dump(scenario, sf)
    with open(os.path.join('.meta_info', 'exploration.pkl'), 'wb') as sf:
        pickle.dump([validation_instances, exploration_inst_id], sf)
    with open(os.path.join('.meta_info', 'valid_res.pkl'), 'wb') as sf:
        pickle.dump(valid_res, sf)
    with open(os.path.join('.meta_info', 'valid_features.pkl'), 'wb') as sf:
        pickle.dump(validation_features, sf)
    with open(os.path.join('.meta_info', 'partition_list.pkl'), 'wb') as sf:
        pickle.dump(partition_list, sf)
    with open(os.path.join('.meta_info', 'incumbent_list.pkl'), 'wb') as sf:
        pickle.dump(incumbent_list, sf)


def continue_kraken(scen_file: str, per_partition_time: float, working_dir: str = 'piac', seed: Union[int, None] = None,
                    verbosity: int = logging.INFO, exploration_time_budget: float = 1.0,
                    init_rand_exploration_evaluations: int = 10, init_default_evaluations: int = 10,
                    exploration_evaluations: int = 10, insts_for_PEI: int = -1,
                    max_num_partitions: int = 8, tae_str: str = 'aclib', save_rounds: bool = False,
                    min_partition_size: int = 1, regularize: bool = True, modus: str = 'SMAC',
                    only_known_configs: bool = True, budget_strat: str = 'inc', only_explore_best: bool = True,
                    n_rand_splits: int = 1000, resource_dir: Union[str, None] = None, available_cores: int = -1,
                    time_allowed_to_run_continously: float = np.float('inf')):
    return_to_dir = os.getcwd()
    logging.basicConfig(level=verbosity, format="%(asctime)s:%(processName)s:%(levelname)s:%(name)s:%(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")
    name = 'piac'
    logger = logging.getLogger('%s_MAIN' % name.upper())
    working_dir = os.path.abspath(working_dir)
    assert os.path.exists(working_dir), 'The scenario you want to continue does not exist!'
    os.chdir(working_dir)
    assert os.path.exists('.meta_info'), 'The scenario you want to continue does not have a .meta_info folder'
    with open(os.path.join('.meta_info', 'preproc.pkl'), 'rb') as pf:
        list_ = pickle.load(pf)
    preprocessor = list_[0]
    store_preprocessor = list_[1]
    del list_
    with open(os.path.join('.meta_info', 'scenario.pkl'), 'rb') as sf:
        scenario = pickle.load(sf)
    all_runs_runhist = RunHistory(aggregate_func=average_cost)
    all_runs_runhist.load_json('runhistory.json', scenario.cs)
    with open(os.path.join('.meta_info', 'round_info.json'), 'r') as mf:
        round_dict = json.load(mf)
    with open(os.path.join('.meta_info', 'rng.pkl'), 'rb') as pf:
        rng = pickle.load(pf)
    with open(os.path.join('.meta_info', 'exploration.pkl'), 'rb') as pf:
        validation_instances, exploration_inst_id = pickle.load(pf)
    with open(os.path.join('.meta_info', 'valid_res.pkl'), 'rb') as sf:
        valid_res = pickle.load(sf)
    with open(os.path.join('.meta_info', 'valid_features.pkl'), 'rb') as sf:
        validation_features = pickle.load(sf)
    with open(os.path.join('.meta_info', 'incumbent_list.pkl'), 'rb') as sf:
        incumbent_list = pickle.load(sf)
    with open(os.path.join('.meta_info', 'partition_list.pkl'), 'rb') as sf:
        partition_list = pickle.load(sf)

    round_count = round_dict['round_count']
    runs = round_dict['runs']
    scenario.wallclock_limit = round_dict['remaining_time']
    round_info = round_dict['round_info']
    splitting_time = round_dict['splitting_time']

    try:
        scenario.output_dir = "PIAC-output_%s" % (
            datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S'))

        # num_instances = len(scenario.feature_dict.keys())
        stats = PIACStats(wallclock_limit=scenario.wallclock_limit)
        stats.start_timing()

        ################################################################################################################
        # Main Loop:
        # While budget available, use CSHC to partition instances that perform similar under given config
        # use SMAC/ROAR to optimize on these partitions, until no budget left or no change in partitions
        n_threads = max(max(max_num_partitions, 1), available_cores)
        partition_tree = None  # type: Union[None, TreeNode]
        if n_threads <= 1:
            parallel = None
        else:
            parallel = n_threads

        while not stats.is_budget_exhausted():
            start_opt = time.time()
            optimization_time = runs[min(round_count, len(runs) - 1)]
            partition_list = setup_scenario_per_partition(partition_list, scenario, optimization_time, runs,
                                                          round_count, stats)
            partition_list, part_at_dict = fit_to_cores(partition_list, n_threads, scenario)
            stats.print_stats()
            # optimize using SMAC/ROAR
            all_runs_runhist, partition_list, valid_res, incumbent_list = optimize_partitions(
                tae_str, modus, partition_list=partition_list, init_runhist=all_runs_runhist, valid_res=valid_res,
                threads=parallel, master_stats_object=stats, use_pred_start=True, rng=rng,
                valid_insts=validation_instances, partition_inst_dict=part_at_dict, incumbent_list=incumbent_list
            )

            opt_valid_time = time.time() - start_opt
            split_start = time.time()
            logging.debug('Current number of incumbents: %d' % len(incumbent_list))
            incumbent_list = get_contributing_incumbents(valid_res, incumbent_list)
            logging.debug('Current number of contributing incumbents: %d' % len(incumbent_list))
            cshc = CSHC(scenario, np.random.RandomState(12345),
                        min_partition_size=min_partition_size, max_partitions=max_num_partitions,
                        random_splits=n_rand_splits, regularize=regularize, incumbent_list=incumbent_list,
                        validation_results=valid_res, validation_instances=validation_instances,
                        validation_features=validation_features)
            try:
                partition_list, partition_tree = cshc.partition(stats)
            except PIACBudgetExhaustedException:
                pass
            splitting_time = time.time() - split_start
            round_info.append([splitting_time, opt_valid_time, optimization_time])

            if partition_tree is not None:
                if store_preprocessor:
                    partition_tree.preprocessor = preprocessor
                stats.print_stats()
                if save_rounds:
                    save_meta_info(preprocessor, store_preprocessor, all_runs_runhist, round_count + 1,
                                   runs, stats, round_info,
                                   rng, scenario, splitting_time, validation_instances, exploration_inst_id, valid_res,
                                   validation_features, incumbent_list, partition_list)
                    partition_tree.lighten()
                    with open('piac_partition_tree_%s.pkl' % (
                            datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')),
                              'wb') as partition_file:
                        pickle.dump(partition_tree, partition_file)
                if (stats.get_used_wallclock_time() >=
                        time_allowed_to_run_continously) or (stats.get_used_wallclock_time() +
                                                             runs[round_count + 1] +
                                                             splitting_time) >= time_allowed_to_run_continously:
                    logging.warning('No time left to run continuously! Start from last checkpoint to complete run!')
                    break
            round_count += 1

        stats.print_stats()
        partition_tree.print_tree()
        partition_tree.lighten()
        logger.info('Saving tree to piac_partition_tree_final.pkl in %s' % os.getcwd())
        with open('piac_partition_tree_final.pkl', 'wb') as partition_file:
            pickle.dump(partition_tree, partition_file)
        save_meta_info(preprocessor, store_preprocessor, all_runs_runhist, round_count + 1, runs, stats, round_info,
                       rng, scenario, splitting_time, validation_instances, exploration_inst_id, valid_res,
                       validation_features, incumbent_list, partition_list)
    except (RuntimeError, NameError, KeyboardInterrupt):
        logging.exception("Exception occurred in the main method!")
        os.chdir(return_to_dir)
    finally:
        os.chdir(return_to_dir)
        for info in round_info:
            logging.info(info)


def _optimize_partition(tae_str, modus, partition: TreeNode, rng: np.random.RandomState,
                        parallel: bool = False, init_conf=None, init_runhist=None,
                        valid_insts: Union[List[str], np.array] = None, q: multiprocessing.Queue = None):
    """
    The actual optimization is done here after it was decided if stuff is run in parallel or sequential
    :param : argparse object
    :param partition: the partition to optimize
    :param rng: RandomState passed to smac
    :param parallel: if run in parallel or not. Basically only used for log output
    :param init_conf: Initial config to start the smac run from. Obtained via cshc
    :param init_runhist: initial runhist containing all the runs from previous iterations
    """
    stats = SMACstats(partition.scenario)
    stats.start_timing()
    runhist = RunHistory(aggregate_func=average_cost)

    optimizer = get_optimizer(tae_str, modus, partition.scenario, runhist, stats, rng, init_conf, init_runhist)
    logger = logging.getLogger('OptimizerCall')
    logger.info('PARALLELLLLLLL: %s\t%s' % (str(parallel), partition.name))

    if parallel:
        optimizer.optimize()
        incumbent = optimizer.solver.incumbent
        tae = optimizer.get_tae_runner()
        rh = tae.runhistory
        if valid_insts is not None:
            all_ = len(valid_insts)
            tae.stats.ta_runs = 0
            tae.stats.wallclock_limit_used = 0
            tae.stats.ta_time_used = 0
            tae.stats.inc_changed = 0
            valid_dict = {'config': incumbent, 'partition': partition.name, 'valid_insts': {}}
            logger.debug('======================== RH size: %d' % len(tae.runhistory.data))
            for idx, inst in enumerate(valid_insts):
                logger.info('~~~~~~~~~~~~~~~~~~~~~~~~ Inst %d of %d / Partition %s on inst: %s' % (
                    idx + 1, all_, partition.name, inst))

                # smac stats object depends on allowed wallclock stored in scenario
                # thus we have to always reset it to avoid "Budget exhausted error
                tae.stats.start_timing()
                empirical_result = tae.start(optimizer.solver.incumbent, instance=inst,
                                             cutoff=partition.scenario.cutoff,
                                             seed=12345,
                                             instance_specific="0")
                valid_dict['valid_insts'][inst] = {incumbent: empirical_result[1]}
            valid_dict['rh'] = tae.runhistory
            q.put(valid_dict)
            logger.info('~~~~~~~~~~~~~~~~~~~~~~~~ Validation for Partition %s done' % partition.name)
            logger.debug('======================== RH size: %d' % len(tae.runhistory.data))
        else:
            q.put({'rh': rh, 'config': incumbent, 'partition': partition.name})
    else:
        try:
            optimizer.optimize()
        except KeyboardInterrupt:
            logger.critical('##########################################################')
            logger.critical('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            logger.critical('Interrupted. Trying to salvage last incumbent of Optimizer')
            logger.critical('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            logger.critical('##########################################################')
        finally:
            # ensure that the runhistory is always dumped in the end
            if partition.scenario.output_dir is not None:
                tmp = partition.scenario.output_dir
                optimizer.solver.runhistory.save_json(
                    fn=os.path.join(tmp, "runhistory.json"))
                if not os.path.exists(os.path.join(tmp, 'traj_aclib2.json')):
                    traj = optimizer.solver.intensifier.traj_logger.trajectory
                    tmp_logger = TrajLogger(tmp, optimizer.stats)
                    for entry in traj:
                        tmp_logger.add_entry(entry.train_perf, entry.incumbent_id, entry.incumbent)
    return optimizer.runhistory, optimizer.solver.stats


def _sequentially_optimize_partitions(tae_str, modus, partition_list: list, init_runhist: RunHistory,
                                      rng: np.random.RandomState,
                                      master_stats_object: PIACStats = None, use_pred_as_start: bool = True):
    """
    DEPRECATED
    Sequential optimization loop, for  see optimize_partitions
    DEPRECATED
    DON'T USE !!!!!
    """
    logger = logging.getLogger('SequentialCallToSMAC')
    logger.debug('runhist_size before: %d' % len(init_runhist.data.keys()))
    for _, partition in enumerate(partition_list):
        p_runhist, p_stats = _optimize_partition(tae_str, modus, partition, rng, parallel=False,
                                                 init_conf=partition.configuration if use_pred_as_start else None,
                                                 init_runhist=init_runhist)
        init_runhist.update(p_runhist)
        master_stats_object.update(p_stats)
    logger.debug('runhist_size after: %d' % len(init_runhist.data.keys()))
    logger.debug('Finished sequential optimization')
    return init_runhist, None


def _optimize_partitions_in_parallel(tae_str, modus, partition_list: List[TreeNode], init_runhist: RunHistory,
                                     rng: np.random.RandomState, use_pred_as_start: bool = True,
                                     valid_insts: Union[List[str], np.array] = None,
                                     partition_inst_dict: Dict[(str, int)] = None,
                                     valid_res: Dict[(str, Dict[(Configuration, float)])] = {},
                                     incumbent_list: List[Configuration] = []):
    """
    Parallel use of optimizers, for , see optimize_partitions
    """
    man = multiprocessing.Manager()
    q = man.Queue()  # type: multiprocessing.Queue
    processes = []
    for idx, partition in enumerate(partition_list):
        p = Process(target=_optimize_partition, args=(tae_str, modus, partition,
                                                      np.random.RandomState(rng.randint(123456789)), True,
                                                      partition.configuration if use_pred_as_start else None,
                                                      init_runhist, valid_insts, q), name="{:>2d}:{:s}-on-{:s}".format(
            idx, modus, partition.name
        ))
        p.daemon = True
        p.start()
        processes.append(p)
    for p in processes:
        p.join()

    while not q.empty():
        result = q.get()
        incumbent = result['config']
        if incumbent not in incumbent_list:
            incumbent_list.append(incumbent)
        logging.info('Loading results for %s ' % result['partition'])
        logging.info(incumbent)
        logging.info(partition_inst_dict[result['partition']])
        partition_list[partition_inst_dict[result['partition']]].configuration = incumbent
        logging.debug(partition_list[partition_inst_dict[result['partition']]].instances[:10])
        init_runhist.update(result['rh'], origin=DataOrigin.INTERNAL)
        if not valid_res:
            valid_res = result['valid_insts']
        else:
            for inst in valid_res:
                if incumbent not in valid_res[inst]:
                    valid_res[inst][incumbent] = result['valid_insts'][inst][incumbent]
                else:
                    try:
                        valid_res[inst][incumbent].append(result['valid_insts'][inst][incumbent])
                    except AttributeError:
                        valid_res[inst][incumbent] = [valid_res[inst][incumbent],
                                                      result['valid_insts'][inst][incumbent]]
            logging.debug(valid_res[inst].keys())

    return init_runhist, partition_list, valid_res, incumbent_list


def optimize_partitions(tae_str, modus, partition_list: list, init_runhist: RunHistory,
                        rng: np.random.RandomState, threads: int = None, valid_res=dict(),
                        master_stats_object: PIACStats = None, use_pred_start: bool = True,
                        valid_insts: Union[List[str], np.array] = None, partition_inst_dict: Dict[(str, int)] = None,
                        incumbent_list: List[Configuration] = []):
    """
    Method to use SMAC/ROAR to optimize on the resulting partitions
    :param : argpars_object used to determine if SMAC or ROAR is used
    :param partition_list: all leaf nodes of the partition Tree, to optimize on
    :param init_runhist: Overall runhistory of
    :param rng: rng for SMAC/ROAR
    :param threads: num_threads
    :param master_stats_object: Stats object to keep track of the overall budget spent
    :param use_pred_start: Flag that tells the optimization process to start from the partition configuration CSHC
           deemed best or not
    :return: updated init_runhist, updated_partition_list and associated costs per partition
    """
    if threads is None:
        init_runhist, partition_list = _sequentially_optimize_partitions(
            tae_str, modus, partition_list=partition_list, init_runhist=init_runhist, rng=rng,
            master_stats_object=master_stats_object, use_pred_as_start=use_pred_start)
    else:
        init_runhist, partition_list, valid_res, incumbent_list = _optimize_partitions_in_parallel(
            tae_str, modus, partition_list=partition_list, init_runhist=init_runhist, valid_res=valid_res,
            rng=rng, use_pred_as_start=use_pred_start, valid_insts=valid_insts, partition_inst_dict=partition_inst_dict,
            incumbent_list=incumbent_list)
    return init_runhist, partition_list, valid_res, incumbent_list


def cmd_line_call():
    import argparse
    import multiprocessing
    parser = argparse.ArgumentParser(description='Command line interface for kraken',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('scenario_file', help='Path to the scenario File')
    parser.add_argument('working_dir', help='Directory to run in')

    exploration_group = parser.add_argument_group('exploration')
    cshc_group = parser.add_argument_group('partitioning')
    configuration_group = parser.add_argument_group('configuration')
    # OPTIONAL
    parser.add_argument('-v', '--verbosity', default='INFO', help='Verbosity', choices=['INFO', 'DEBUG'])
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-r', '--resources', default=None, dest='resource_dir', help='Dir to load resources from.' \
                                                                                     ' If none is specified, the call directory will be used!'
                                                                                     ' !!!REQUIRES ABSOLUTE PATHS!!!')
    parser.add_argument('-c', '--available_cores', default=min(8, multiprocessing.cpu_count()),
                        type=int, help='Number of available cores. If more cores are available than found partitions'
                                       'kraken will configure large partitions in paralell')
    parser.add_argument('-s', '--seed', default=None, help='Seed', type=int)
    parser.add_argument('--save_rounds', action='store_true', help='Save the partition tree after each round')
    parser.add_argument('--allowed_max_time', default=np.float('inf'),
                        type=float, help='Allowed max time before having to restart'
                                         ' from last checkpoint')
    parser.add_argument('--continue', action='store_true', help='Continue from checkpoint', dest='cont')
    parser.add_argument('--preproc', action='store_true', help='PCA the data and store preprocessor', dest='preprocess')

    # EXPLORATION
    exploration_group.add_argument('-ed', '--init_default_evaluations', default=5, help='Number of default ' \
                                                                                        'configuration evaluations',
                                   type=int, dest='init_default_evaluations')
    exploration_group.add_argument('-er', '--init_rand_exploration_evaluations',
                                   default=10, help='Num exploring evaluations', type=int,
                                   dest='init_rand_exploration_evaluations')
    exploration_group.add_argument('-ee', '--exploration_evaluations', default=85, help='Num exploring evaluations',
                                   type=int, dest='exploration_evaluations')
    exploration_group.add_argument('-ei', '--insts_for_PEI', default=-1, help='Number of instances to use for PEI. If' \
                                                                              ' set to -1, all instances will be used!',
                                   type=int,
                                   dest='insts_for_PEI')
    exploration_group.add_argument('-eb', '--exploration_time_budget', type=float, default=np.float('inf'),
                                   help='Time budget for exploration [sec]', dest='exploration_time_budget')

    # PARTITIONING
    cshc_group.add_argument('-ps', '--min_partition_size', default=1, help='Minimum number of instances required to ' \
                                                                           'form a partition', type=int,
                            dest='min_partition_size')
    cshc_group.add_argument('-pp', '--max_num_partitions', default=multiprocessing.cpu_count(),
                            help='Maximum allowed number of partitions', type=int, dest='max_num_partitions')
    cshc_group.add_argument('-pk', '--local_search', action='store_false', dest='only_use_known_configs',
                            help='Use local search on the EPM to find well performing configurations during CSHC.' \
                                 'Otherwise only configurations that were evaluated on at least one instance are '
                                 'checked!')
    cshc_group.add_argument('-pc', '--splits', default=1000, type=int, help='Number of random splits for CSHC per partition')
    cshc_group.add_argument('-pr', '--regularize', action='store_true', help='Regularization that prefers balanced ' \
                                                                             'splits')

    # CONFIGURATION
    configuration_group.add_argument('-cb', '--budget', default='equal', choices=['equal', 'inc'],
                                     help='Which budget strategy to use')
    configuration_group.add_argument('-cc', '--configurator', dest='modus', default='SMAC', choices=['SMAC', 'ROAR'],
                                     type=str)
    configuration_group.add_argument('-ct', '--per_partition_time', default=100,
                                     help='Time budget to optimize on partitions', type=float,
                                     dest='per_partition_time')
    configuration_group.add_argument('-cv', '--validation_set', default='EXP', choices=['EXP', 'TRAIN'],
                                     help='Set to validate on', type=str,
                                     dest='validation_set')

    # parser.add_argument()
    args, unknown = parser.parse_known_args()
    logging.basicConfig(level=args.verbosity, format="%(asctime)s:%(processName)s:%(levelname)s:%(name)s:%(message)s",
                        datefmt="%Y-%m-%d %H:%M:%S")
    if len(unknown) >= 1:
        logging.warning('Found the following unknown arguments:')
        m_l = np.max(list(map(lambda x: len(str(x)), unknown)))
        for u in unknown:
            if u.startswith('-'):
                logging.warning('{:>{width}s}:'.format(str(u), width=m_l))
            else:
                logging.warning('{:>{width}s}  {:>s}'.format(' ', str(u), width=m_l))
        logging.warning('Ignoring unknown arguments')
    if args.seed is None:
        args.seed = int.from_bytes(os.urandom(4), byteorder="big")
    if not args.cont:
        kraken(scen_file=args.scenario_file, per_partition_time=args.per_partition_time,
               working_dir=args.working_dir, verbosity=args.verbosity, seed=args.seed,
               exploration_time_budget=args.exploration_time_budget,
               init_rand_exploration_evaluations=args.init_rand_exploration_evaluations,
               init_default_evaluations=args.init_default_evaluations,
               exploration_evaluations=args.exploration_evaluations, insts_for_PEI=args.insts_for_PEI,
               max_num_partitions=args.max_num_partitions, tae_str=args.tae, save_rounds=args.save_rounds,
               min_partition_size=args.min_partition_size, regularize=args.regularize, modus=args.modus,
               only_known_configs=args.only_use_known_configs, budget_strat=args.budget,
               resource_dir=args.resource_dir, available_cores=args.available_cores,
               time_allowed_to_run_continously=args.allowed_max_time, validation_set=args.validation_set,
               n_rand_splits=args.splits, preprocess=args.preprocess
               )
    else:
        continue_kraken(scen_file=args.scenario_file, per_partition_time=args.per_partition_time,
                        working_dir=args.working_dir, verbosity=args.verbosity, seed=args.seed,
                        exploration_time_budget=args.exploration_time_budget,
                        init_rand_exploration_evaluations=args.init_rand_exploration_evaluations,
                        init_default_evaluations=args.init_default_evaluations,
                        exploration_evaluations=args.exploration_evaluations, insts_for_PEI=args.insts_for_PEI,
                        max_num_partitions=args.max_num_partitions, tae_str=args.tae, save_rounds=args.save_rounds,
                        min_partition_size=args.min_partition_size, regularize=args.regularize, modus=args.modus,
                        only_known_configs=args.only_use_known_configs, budget_strat=args.budget,
                        resource_dir=args.resource_dir, available_cores=args.available_cores,
                        time_allowed_to_run_continously=args.allowed_max_time
                        )


if __name__ == '__main__':
    cmd_line_call()
