piac
====

This folder will contain the source code

**tae**, **scenario**, **epm** and **configspace** currently only used for convenience
imports of SMAC modules

Optimization
------------
Contains:
    **P**rofile **E**xpected **I**mprovement: used for the initial exploration of the configuration space
    *Local Search* import from SMAC
    **acquisition function** The acquisition function to guide PEI
    