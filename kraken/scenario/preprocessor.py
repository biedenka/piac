import logging

import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler, StandardScaler, MaxAbsScaler


class Preprocessor:
    """
    Simple preprocessor object that stores all the necessary information about how features get preprocessed
    """
    def __init__(self, random_state: np.random.RandomState, pca_dim: int=7, normalize: bool=False, isac: bool=False):
        self.random_state = random_state
        assert pca_dim >= 1, 'PCA dimension has to be larger than 0!'
        self.pca_dim = pca_dim
        self.normalize = normalize
        self.PCA = None
        self.scaler = None
        self.isac = isac
        self.logger = logging.getLogger(self.__class__.__name__)

    def __call__(self, data, *args, **kwargs):
        if not self.isac:
            if self.pca_dim < data.shape[1]:
                if self.scaler is None:
                    self.scaler = MinMaxScaler()
                    data = self.scaler.fit_transform(data)
                    self.logger.debug('Fitting MinMaxScaler')
                else:
                    self.logger.debug('Reusing MinMaxScaler')
                    data = self.scaler.transform(data)
                if self.PCA is None:
                    self.logger.debug('Fitting PCA')
                    self.PCA = PCA(self.pca_dim, random_state=self.random_state)
                    data = self.PCA.fit_transform(data)
                else:
                    self.logger.debug('Reusing PCA')
                    data = self.PCA.transform(data)
                self.logger.debug('PCA transformed data. New dimensions: %s' % str(data.shape))
            else:
                if self.normalize:
                    if self.scaler is None:
                        self.logger.debug('Fitting StandardScaler')
                        self.scaler = StandardScaler()
                        data = self.scaler.fit_transform(data)
                    else:
                        self.logger.debug('Reusing StandardScaler')
                        data = self.scaler.transform(data)
        else:
            if self.scaler is None:
                self.logger.debug('Fitting MaxAbsScaler')
                self.scaler = MaxAbsScaler()
                data = self.scaler.fit_transform(data)
            else:
                self.logger.debug('Reusing MaxAbsScaler')
                data = self.scaler.transform(data)
        return data

    def __getstate__(self):
        d = dict(self.__dict__)
        del d['logger']
        return d

    def __setstate__(self, d):
        self.__dict__.update(d)
        self.logger = logging.getLogger(self.__class__.__name__)
