import abc
import numpy as np

"""
Created on March 6th, 2017

@author: Andre Biedenkapp <biedenka@cs.uni-freiburg.de>
"""


class AbstractSplit(object):
    @abc.abstractclassmethod
    def __call__(self, *args, **kwargs):
        raise NotImplementedError


class NonAxisAlignedSplit(AbstractSplit):
    """
    Class to make Split_Crit callable
    """
    def __init__(self, support_vector, dir_vector):
        self.sup = support_vector
        self.dir = dir_vector

    def __call__(self, feat_dict, inst):  # True -> left, False -> right
        """
        Returns True if instance belongs into left split, else False
        :param feat_dict: instance feature dict
        :param inst: instance
        :return: boolean, weather it belongs into the left or right split
        """
        return np.dot(feat_dict[inst] - self.sup, self.dir) > 0

    def __str__(self):
        if len(self.sup) <= 4:
            return '(feat(inst) - %s) dot %s > 0' % (str(self.sup), str(self.dir))
        else:
            return '(feat(inst) - [{:= 3.3f} {:= 3.3f} ... ' \
                   '{:= 3.3f} {:= 3.3f}]) dot [{:= 3.3f} {:= 3.3f}' \
                   ' ... {:= 3.3f} {:= 3.3f}] > 0'.format(*self.sup[[0, 1, -2, -1]], *self.dir[[0, 1, -2, -1]])


class AxisAlignedSplit(AbstractSplit):
    """
    Class to make Split_crit callable
    """
    def __init__(self, split_index, split_value):
        self.idx = split_index
        self.val = split_value

    def __call__(self, feat_dict, inst):  # True -> left, False -> right
        """
        Returns True if instance belongs into left split, else False
        :param feat_dict: instance feature dict
        :param inst: instance
        :return: boolean, weather it belongs into the left or right split
        """
        return feat_dict[inst][self.idx] > self.val

    def __str__(self):
        return 'feat(inst)[%d] > %s' % (self.idx, str(self.val))
