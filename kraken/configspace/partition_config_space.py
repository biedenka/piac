import os
import abc
import time
import logging
import datetime
import functools
import multiprocessing

from typing import Union, List
import copy

import numpy as np
from sklearn.cluster import KMeans, MiniBatchKMeans
from scipy.stats import anderson
from sklearn.metrics import euclidean_distances

from smac.epm.rf_with_instances import RandomForestWithInstances
from smac.scenario.scenario import Scenario
from smac.runhistory.runhistory import RunHistory

from kraken.configspace.split_criteria import AxisAlignedSplit, NonAxisAlignedSplit, AbstractSplit
from kraken.optimization.iterated_local_search import IteratedLocalSearch
from kraken.optimization.search_best_seen_config import BruteForceBestSeenConfig
from kraken.util.n_ary_tree import TreeNode
from kraken.util.utils import construct_scenario, PIACBudgetExhaustedException
from kraken.util.stats import PIACStats

"""
Created on January 3, 2017

@author: André Biedenkapp
"""


class AbstractSpacePartitioning(object):
    """
    Base class in case we want to implement different methods to partition the feature space
    """
    __metaclass__ = abc.ABCMeta

    def __str__(self):
        return type(self).__name__ + " (" + self.long_name + ")"

    def __init__(self, scenario: Scenario, name: str, rng: np.random.RandomState):
        """
        Base
        """
        self.long_name = name
        self.short_name = ''.join(c for c in name if c.isupper())
        self.scenario = scenario
        self.instance_feat_dict = self.scenario.feature_dict
        self.partitions = list()
        self.random_state = rng

        self.logger = logging.getLogger(self.short_name)

    def update(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

    @abc.abstractmethod
    def partition(self, *args, **kwargs) -> Union[None, np.ndarray, list]:
        raise NotImplementedError

    def cluster(self, *args, **kwargs):
        self.partition(*args, **kwargs)


class GMeans(AbstractSpacePartitioning):
    """
    Implementation of GMeans Clustering to be used with ISAC
    """
    def __init__(self, scenario: Scenario, rng: np.random.RandomState,
                 minimum_samples_per_cluster: int=2, n_init: int=10, significance: int=4,
                 restarts: int=10, per_partition_time: Union[None, float]=None):

        super().__init__(scenario, 'GMeans Clustering', rng=rng)
        self.minimum_samples_per_cluster = minimum_samples_per_cluster
        self.n_init = n_init
        self.significance = significance
        self.restarts = restarts
        self.KMobject = None  # type: Union[KMeans, MiniBatchKMeans, None]
        self._BestKMobject = None  # type: Union[KMeans, MiniBatchKMeans, None]
        self.inertia = np.float('inf')
        self.logger = logging.getLogger('GMeans')
        self.num_clusters = None
        self.partitions = list()
        self.tree = None  # type: Union[None, TreeNode]
        self._tree = None  # type: Union[None, TreeNode]
        self.nodes = []
        self.base_out_dir = self.scenario.output_dir
        self.per_partition_time = per_partition_time

    def partition(self, X=None, use_mini_batch: bool=False, *args, **kwargs):
        if X is None:
            X = self.scenario.feature_array
        inertia_ = np.float('inf')
        for _ in range(self.restarts):
            self.partitions = []
            self.logger.info('Round %d' % (_ + 1))
            self._fit_recursive(X, use_mini_batch=use_mini_batch)
            if self.KMobject.inertia_ < inertia_:
                inertia_ = self.KMobject.inertia_
                self._BestKMobject = copy.deepcopy(self.KMobject)
                self._tree = copy.deepcopy(self.tree)
        self.KMobject = copy.deepcopy(self._BestKMobject)
        self.tree = copy.deepcopy(self._tree)
        self._tree = None
        self._BestKMobject = None
        self.num_clusters = self.KMobject.cluster_centers_.shape[0]
        preds = self.KMobject.predict(X)
        self.logger.debug('Scenario: (#insts/#feats) ({0[0]:>6d}/{0[1]:>6d})'.format(self.scenario.feature_array.shape))
        for label in range(np.max(preds) + 1):
            indices = preds == label
            c_insts_i = np.empty((sum(indices),), dtype=int)
            c_insts_n = np.empty((sum(indices),), dtype=object)
            c_insts_f = {}
            c_insts_fa = np.empty((sum(indices), self.scenario.feature_array.shape[1]), dtype=np.ndarray)
            at = 0
            for idx, val in enumerate(indices):
                if val:
                    c_insts_i[at] = idx
                    c_insts_n[at] = self.scenario.train_insts[idx]
                    c_insts_f[self.scenario.train_insts[idx]] = self.scenario.feature_dict[
                        self.scenario.train_insts[idx]]
                    c_insts_fa[at] = self.scenario.feature_array[idx]
                    at += 1
            c_scen = copy.deepcopy(self.scenario)
            euc_dist = euclidean_distances(c_insts_fa, [self.KMobject.cluster_centers_[label]])
            c_split_crit = np.mean(euc_dist) + 2 * np.std(euc_dist)
            c_scen_out_dir = os.path.join(self.base_out_dir, str(label))
            c_scen.output_dir = c_scen_out_dir
            c_scen.train_insts = c_insts_n
            if self.per_partition_time is not None:
                c_scen.wallckock_limit = self.per_partition_time
            node = TreeNode(instances=c_insts_i, instance_names=c_insts_n,
                            parent=None, depth=0, instance_features=c_insts_f,
                            left=None, children=[], is_root=False, split_criterion=c_split_crit,
                            scenario=c_scen, name=str(label), loss=None, configuration=None)
            node.scenario.wallclock_limit = self.per_partition_time

            self.logger.debug(' Node {1:>2d}: (#insts/#feats) ({0[0]:>6d}/{0[1]:>6d})'.format(c_insts_fa.shape, label))
            self.logger.debug('\t(max/mean/min) dist from centroid: ({0:>3.2f} / {1:>3.2f} / {2:>3.2f})'.format(
                np.max(euc_dist), np.mean(euc_dist), np.min(euc_dist)
            ))
            node.centroid = self.KMobject.cluster_centers_[label]
            self.nodes.append(node)
        return preds

    def _fit_recursive(self, X: np.ndarray, label: int=0, use_mini_batch: bool=False,
                       depth: int=0, parent_center: Union[np.ndarray, None]=None, node: Union[None, TreeNode] = None):
        """
        Implementation of the algorithm to learn the correct k for KMeans clustering presented in
        'Hamerly, Greg, and Charles Elkan. "Learning the k in k-means." NIPS. Vol. 3. 2003.'
        and used in
        'Kadioglu, Serdar, et al. "ISAC-Instance-Specific Algorithm Configuration." ECAI. Vol. 215. 2010.'
        :param X: Feature Array
        :param use_mini_batch: bool to determine which sklearn KMeans variant to use
        :param parent_center: start point
        :param: depth: recursion depth
        """
        if node is None:
            root = TreeNode(instances=np.array(list(range(len(self.scenario.train_insts)))),
                            instance_names=np.array(self.scenario.train_insts),
                            instance_features=self.instance_feat_dict,
                            is_root=True, children=[], split_criterion=np.float('inf'),
                            parent=None, depth=0, scenario=self.scenario, left=None, name='S', configuration=None)
            self.tree = root
        self.logger.debug('%sAt depth %d' % (' ' * 2 * depth, depth))
        KM = MiniBatchKMeans if use_mini_batch else KMeans
        # First treat everything as belonging to one large partition
        if depth == 0:
            km_object = KM(n_clusters=1, n_init=self.n_init, random_state=self.random_state)
            km_object.fit(X)
            self._fit_recursive(X, use_mini_batch=use_mini_batch,
                                depth=depth+1, parent_center=km_object.cluster_centers_, node=root)
            self.KMobject = KM(n_clusters=len(self.partitions), init=np.array(self.partitions),
                               random_state=self.random_state)
            self.KMobject.fit(X)
        else:
            if X.shape[0] <= self.minimum_samples_per_cluster:
                self.logger.error('X.shape: %s | %d :Min_Samples_Per_Cluster' % (str(X.shape),
                                                                                   self.minimum_samples_per_cluster))
                self.logger.error('Too small to even start. This should never happen!')
                raise Exception('Too small to even start at depth % d' % depth)
            else:
                km_object = KM(n_clusters=2, n_init=self.n_init, random_state=self.random_state)
                km_object.fit(X)
                # test the split for its gaussianity by ...
                centroid_0, centroid_1 = km_object.cluster_centers_
                diff = centroid_0 - centroid_1
                # ... projecting it onto a line
                projected_X = np.inner(diff, X) / np.linalg.norm(diff, ord=2)
                mean, std = np.mean(projected_X), np.std(projected_X)
                projected_X = (projected_X - mean) / std  # and making it zero mean and std 1
                statistic, critical, _ = anderson(projected_X)  # and then using the anderson-darling test
                left_indices = km_object.labels_ == 0  # type: list
                right_indices = km_object.labels_ == 1  # type: list
                if statistic >= critical[self.significance]:  # accept the split
                    if sum(left_indices) <= self.minimum_samples_per_cluster or \
                                    sum(right_indices) <= self.minimum_samples_per_cluster:
                        if len(self.partitions) < 2 and depth == 1:
                            self.partitions.append(centroid_0)
                            self.partitions.append(centroid_1)
                        else:
                            self.partitions.append(parent_center)
                        self.logger.warning('TOOO SMAAALLLLL')
                        self.logger.info('%sRejecting Split for label %d' % (' ' * 2 * depth, label))
                        self.logger.info('%s-> new #clusters %d' % (' ' * 2 * depth, len(self.partitions)))
                    else:
                        l_insts = node.instances[left_indices]
                        l_insts_n = node.instance_names[left_indices]
                        l_feats = node.scenario.feature_array[left_indices]
                        l_scen = copy.deepcopy(node.scenario)  # type: Scenario
                        l_scen.feature_array = l_feats
                        split_crit = None
                        l_scen.output_dir = os.path.join(self.base_out_dir, node.name + '-L')
                        left_node = TreeNode(instances=l_insts, instance_names=l_insts_n,
                                             parent=node, depth=node.depth + 1,
                                             instance_features=dict((name_,
                                                                     node.instance_features[name_]
                                                                     ) for name_ in l_insts_n),
                                             left=True, children=[], is_root=False, split_criterion=split_crit,
                                             scenario=l_scen, name=node.name + '-L', loss=None, configuration=None)
                        r_insts = node.instances[right_indices]
                        r_insts_n = node.instance_names[right_indices]
                        r_feats = node.scenario.feature_array[right_indices]
                        r_scen = copy.deepcopy(node.scenario)  # type: Scenario
                        r_scen.feature_array = r_feats
                        split_crit = None
                        r_scen.output_dir = os.path.join(self.base_out_dir, node.name + '-R')
                        right_node = TreeNode(instances=r_insts, instance_names=r_insts_n,
                                              parent=node, depth=node.depth + 1,
                                              instance_features=dict((name_,
                                                                      node.instance_features[name_]
                                                                      ) for name_ in r_insts_n),
                                              left=False, children=[], is_root=False, split_criterion=split_crit,
                                              scenario=r_scen, name=node.name + '-R', loss=None, configuration=None)
                        node.children.append(left_node)
                        node.children.append(right_node)
                        node.instance_names = None
                        node.instances = None
                        node.instance_features = None
                        self._fit_recursive(X[left_indices], 0, use_mini_batch=use_mini_batch, depth=depth+1,
                                            parent_center=km_object.cluster_centers_[0], node=left_node)
                        self._fit_recursive(X[right_indices], 1, use_mini_batch=use_mini_batch, depth=depth+1,
                                            parent_center=km_object.cluster_centers_[1], node=right_node)
                        self.logger.info('%sAccepting Split for label %d' % (' ' * 2 * depth, label))
                        self.logger.info('%s-> new #clusters %d' % (' ' * 2 * depth, len(self.partitions)))
                else:
                    self.partitions.append(parent_center.flatten())
                    self.logger.info('%sRejecting Split for label %d' % (' ' * 2 * depth, label))
                    self.logger.info('%s-> new #clusters %d' % (' ' * 2 * depth, len(self.partitions)))


class CSHC(AbstractSpacePartitioning):
    """
        Implementation of Cost Sensitive Hierarchical Clustering
    """
    def __init__(self, scenario: Scenario, model: RandomForestWithInstances,
                 opt_time_per_partition: float, rng: np.random.RandomState, min_partition_size: int=1,
                 max_partitions: int=-1, random_splits: int=1000, regularize: bool=False,
                 allowed_budget: float=float('inf'), evaluated_configs_rh: RunHistory = None,
                 **kwargs):
        super().__init__(scenario, 'Cost Sensitive Hierarchical Clustering', rng)
        self.model = model
        self._evaluated_configs = False
        if evaluated_configs_rh is None:
            self.optimizer = IteratedLocalSearch(rng=rng, **kwargs)
        else:
            self._evaluated_configs = True
            self.optimizer = BruteForceBestSeenConfig(runHist=evaluated_configs_rh)
        self.min_partition_size = min_partition_size
        if max_partitions == -1:
            self.max_partitions = multiprocessing.cpu_count()
        else:
            self.max_partitions = max_partitions
        self.predicted_default_performance = None
        self.mean_feature_values = np.mean(self.model.instance_features, axis=0)
        self.n_features = self.model.instance_features.shape[1]
        self.max_feature_values = np.max(self.model.instance_features, axis=0)
        self.min_feature_values = np.min(self.model.instance_features, axis=0)
        self._random_splits = random_splits
        self._predicted_lower_bound = None
        self.opt_time_per_partition = opt_time_per_partition
        self.regularize = regularize
        self.mean_time_per_split = 0.
        self._num_performed_splits = 0
        self.allowed_budget = allowed_budget
        self._num_restarts = kwargs['restarts']
        self._num_steps = kwargs['max_steps']
        self.budget_per_partition = float('inf')

        self.partial_partition_loss_function = functools.partial(self._get_partition_loss, self.scenario.feature_dict)

        # THis part is to get a rough estimate of how long the whole splitting process will take and to predict
        # the lower bounds if one was to perform perfect per instance algorithm configuration
        start = time.time()
        self.logger.debug('Predicting per-instance lower-bounds')
        res = list(map(lambda x: self.partial_partition_loss_function(x),
                       list(map(lambda x: [x], self.scenario.train_insts))))
        self._predicted_lower_bound = dict((inst[0], prediction) for prediction, inst, conf in res)
        taken = time.time() - start
        self.mean_time_per_split = (taken * self._num_restarts * .5 * self._num_steps) / len(self.scenario.train_insts)
        self._num_performed_splits = len(self.scenario.train_insts)
        self.logger.debug('Prediction took %3.5f sec on average' % self.mean_time_per_split)
        self.allowed_budget -= taken
        self.logger.debug('Remaining budget: %3.5fsec' % self.allowed_budget)
        self.budget_per_partition = self._random_splits * self.mean_time_per_split
        self.logger.debug('BPP[sec]: %f' % self.budget_per_partition)
        self.logger.debug('BPP[min]: %f' % (self.budget_per_partition / 60.))
        self.logger.info('Will approximately take %3.2f minutes for splitting' % ((
            self.budget_per_partition * (self.max_partitions - 1)) / 60))

    def _get_partition_loss(self, inst_feat_dict: dict, insts: list):
        """
            get loss of choosing a per-instance-set configuration in comparison to a per-instance configuration
        """
        partition_feats = np.array([inst_feat_dict[i] for i in insts])
        partition_config, partition_loss = self.optimizer.optimize(self.model.predict, self.scenario.cs,
                                                                   partition_feats)

        return partition_loss, insts, partition_config

    def partition(self, stats: PIACStats):
        """
        Method that groups instances together for wich a given performance is deemed the most optimal
        :return: list of partitions (leaves of the partition tree), root_node of the partition_tree
        """
        root = TreeNode(instances=list(range(len(self.scenario.train_insts))),
                        instance_names=np.array(self.scenario.train_insts),
                        instance_features=self.instance_feat_dict,
                        is_root=True, children=[], split_criterion=None,
                        parent=None, depth=0, scenario=None, left=None, name='S', configuration=None)

        partitions = [root]
        changed = True
        p_loss = {}
        p_split = {}
        while len(partitions) < self.max_partitions and changed:
            changed = False
            for idx, partition in enumerate(partitions):
                if partition.name in p_loss:
                    continue
                split_loss, split_, split_crit = self.split(partition, self.partial_partition_loss_function, stats)
                if split_:
                    p_loss[partition.name] = split_loss
                    p_split[partition.name] = (split_, split_crit, idx)
                    changed = True
            if p_loss:
                split_partition_name = min(p_loss, key=p_loss.get)  # Find the key with the minimum value in the dictionary
                split_, split_crit, idx = p_split[split_partition_name]
            else:
                assert not changed
            if changed or p_loss:
                split_partition = partitions.pop(idx)
                left_node = TreeNode(instances=split_[0][2], instance_names=split_[0][1],
                                     parent=split_partition, depth=split_partition.depth + 1,
                                     instance_features=split_partition.instance_features, left=True, children=[],
                                     is_root=False, split_criterion=None, scenario=None,
                                     name=split_partition.name + '-L', loss=split_[0][0], configuration=split_[0][3])
                right_node = TreeNode(instances=split_[1][2], instance_names=split_[1][1],
                                      parent=split_partition, depth=split_partition.depth + 1,
                                      instance_features=split_partition.instance_features, left=False, children=[],
                                      is_root=False, split_criterion=None, scenario=None,
                                      name=split_partition.name + '-R', loss=split_[1][0], configuration=split_[1][3])
                split_partition.instance_names = None
                split_partition.instances = None
                split_partition.instance_features = None
                split_partition.split_criterion = split_crit
                split_partition.children.append(left_node)
                split_partition.children.append(right_node)
                partitions.append(left_node)
                partitions.append(right_node)
            if p_loss:
                del p_loss[split_partition_name], p_split[split_partition_name]
        if len(partitions) > 1:
            for partition in partitions:
                partition.scenario = construct_scenario(self.scenario, partition, self.opt_time_per_partition)
        else:
            partitions[0].scenario = self.scenario
        return np.array(partitions), root

    def _random_axis_aligned_split(self, instances: list):
        """
        Randomly select a random instance and feature value (at index i) in that instance as splitting criterion.
        Will be used to sort instances as follows:
        if inst_j[i] > selected_inst[i] do insertion into set A else B
        :param instances: list of instance names. A random instance will be selected from here
        :return: splitting_criterion (split_index, split_value)
        """
        random_instance = self.random_state.choice(instances)
        feature_vector = self.instance_feat_dict[random_instance]
        split_index = self.random_state.randint(0, self.n_features)
        split_value = feature_vector[split_index]
        return AxisAlignedSplit(split_index, split_value)

    def _random_non_axis_aligned_split(self, mean=None, max_=None):
        """
        Generate a support vector and random direction vector to split all resulting values.
        Will be used to sort instances as follows:
        if np.dot((instance_features - support_vector), direction_vector) > 0 insert into set A else B
        :return: splitting_criterion (support_vector, direction_vector)
        """
        if mean is None:
            mean = self.mean_feature_values
        if max_ is None:
            max_ = self.max_feature_values
        support_vector = self.random_state.randn(self.n_features) + mean
        support_vector[max_ == 0] = 0
        dir_vector = self.random_state.randn(self.n_features)
        return NonAxisAlignedSplit(support_vector, dir_vector)

    def _split_insts_given_crit(self, instance_names: list, instance_ids: list, crit: AbstractSplit):
        """
        Method to split a given set of instances (as names and ids) using the specified criterion
        :param instance_names: List of instance names ['instance_xyz' , ...]
        :param instance_ids: List of instance ids [9, 15, 19, 100, ...]
        :param crit: The splitting criterion as tuple (see _random_..._split methods)
        :return: list of instance ids "over" the split value
                 list of instance names "over" the split value
                 list of instance ids "below or equal to" the split value
                 list of instance names "below oor equal to" the split value
        """
        id_set_a = []
        name_set_a = []
        name_set_b = []
        id_set_b = []
        for instance, id_ in zip(instance_names, instance_ids):
            if crit(self.instance_feat_dict, instance):
                id_set_a.append(id_)
                name_set_a.append(instance)
            else:  # IDEA it might be possible to predict where to split not just randomly split
                id_set_b.append(id_)
                name_set_b.append(instance)
        return id_set_a, name_set_a, id_set_b, name_set_b

    def split(self, partition: TreeNode, partition_loss_function: callable,
              stats: PIACStats, axis_aligned: bool=False):
        """
        Method to split an existing partition into smaller subsets
        :param partition: The partition in question to split
        :param partition_loss_function: loss_function to determine best split
        :param stats: Stats object to keep track of remaining time
        :param axis_aligned: boolean, determines if the split is along the axis or not
        :return: tuple (resulting partitions), float (loss as sum of the splits), tuple (splitting criterion see
                                                                                       _random_..._split methods)
        """
        best_loss = np.float('inf')
        best_split = None
        best_split_crit = None

        # IDEA Learn the partition sizes in subsequent iterations
        if len(partition.instance_names) < self.min_partition_size:
            self.logger.debug('Too small to further split!')
            return best_loss, best_split, best_split_crit

        empty_count = 0
        left_ = [np.float('inf'), np.array([])]
        right_ = [np.float('inf'), np.array([])]
        split_num = 0
        while split_num < self._random_splits:  # randomly split M times
            if stats.is_budget_exhausted():
                raise PIACBudgetExhaustedException
            self._num_performed_splits += 1
            start = time.time()
            if axis_aligned:
                split_crit = self._random_axis_aligned_split(partition.instance_names)
            else:
                part_feats = self.model.instance_features[partition.instances]
                mean = np.mean(part_feats, axis=0)
                max_ = np.max(part_feats, axis=0)
                split_crit = self._random_non_axis_aligned_split(mean=mean, max_=max_)

            left_ids, left_names, right_ids, right_names = self._split_insts_given_crit(partition.instance_names,
                                                                                        partition.instances,
                                                                                        split_crit)
            if not (left_ids and right_ids):
                empty_count += 1
                if self._random_splits <= 1:
                    if empty_count % 10 == 1:
                        split_num += 1
                else:
                    split_num += 1
                # logging.debug("EMPTY SPLIT!!!")
                continue
            if len(left_ids) < self.min_partition_size or len(right_ids) < self.min_partition_size:
                logging.debug('SPLIT TOO SMALL')
                if self._random_splits <= 1:
                    if empty_count % 10 == 1:
                        split_num += 1
                else:
                    split_num += 1
                continue

            # compute the loss of the individual partitions
            left_loss, _, left_conf = partition_loss_function(left_names)
            right_loss, _, right_conf = partition_loss_function(right_names)

            loss, sum_lower_bounds = self._get_split_loss(left_names, left_loss, right_names, right_loss)
            if loss < best_loss:  # track best loss
                # sort IDS for easier comparison of partitions later on
                left_indices = [i[0] for i in sorted(enumerate(left_ids), key=lambda x: x[1])]
                right_indices = [i[0] for i in sorted(enumerate(right_ids), key=lambda x: x[1])]
                left_ = [left_loss, np.array(left_names)[left_indices], np.array(left_ids)[left_indices], left_conf]
                right_ = [right_loss, np.array(right_names)[right_indices], np.array(right_ids)[right_indices],
                          right_conf]
                best_loss = loss
                best_split = [left_, right_]
                best_split_crit = split_crit

            self.logger.info('SplitNum %d of %d' % (split_num, self._random_splits))
            self.logger.info('SplitCrit: %s' % str(split_crit))
            self.logger.info('SplitLoss %3.3f / Sum left+right %3.3f / Sum_Lower_Bounds %3.3f' % (
                loss, left_loss + right_loss, sum_lower_bounds))
            self.logger.info('    Split: %s' % str([len(left_names), len(right_names)]))
            self.logger.info('~'*80)
            self.logger.info('Best SplitCrit: %s' % str(best_split_crit))
            self.logger.info('Best loss %3.3f / Sum left+right %3.3f' % (best_loss, left_[0] + right_[0]))
            self.logger.info('    Best Split: %s' % str([left_[1].shape[0], right_[1].shape[0]]))
            self.logger.info('#'*80)
            taken = time.time() - start
            self.mean_time_per_split = self.mean_time_per_split + ((taken - self.mean_time_per_split
                                                                    ) / self._num_performed_splits)
            self.logger.debug('Split takes %3.5f sec on average' % self.mean_time_per_split)
            split_num += 1

        self.logger.debug('percent empty splits %2.2f' % (empty_count/float(self._random_splits)))
        return best_loss, best_split, best_split_crit

    def _get_split_loss(self, left_partition_instances: list, left_partition_loss: float,
                        right_partition_instances: list, right_partition_loss: float):
        """
        Helper method to compute the loss of the current partition split as a function of
        the mean_partition_loss - predicted_instance_specific_loss
        :param left_partition_instances: list of instances in the left child
        :param left_partition_loss: associated loss for left child
        :param right_partition_instances: list of instances in the right child
        :param right_partition_loss: associated loss of right child
        :return: sum((left_loss - predicted_lower_bound_inst_specific_in_left_child),
                     (right_loss - predicted_lower_bound_ins_specific_in_right_child))
        """
        left_lower_bound = sum(self._predicted_lower_bound[inst_] for inst_ in left_partition_instances)
        right_lower_bound = sum(self._predicted_lower_bound[inst_] for inst_ in right_partition_instances)

        left_loss = (left_partition_loss - left_lower_bound)
        right_loss = (right_partition_loss - right_lower_bound)

        dist = abs(len(left_partition_instances) - len(right_partition_instances))
        scale = min(1., 1. / np.log(dist))
        self.logger.debug(dist * scale)

        if self.regularize:
            return (left_loss + right_loss + dist * scale), (left_lower_bound + right_lower_bound)
        else:
            return left_loss + right_loss, (left_lower_bound + right_lower_bound)


class CSHCNoEPM(AbstractSpacePartitioning):
    """
        Implementation of Cost Sensitive Hierarchical Clustering
    """
    def __init__(self, scenario: Scenario, rng: np.random.RandomState, min_partition_size: int=1,
                 max_partitions: int=-1, random_splits: int=1000, regularize: bool=False,
                 incumbent_list: Union[List, np.ndarray]=[], validation_results: dict()={},
                 validation_instances: Union[List, np.ndarray]=[],
                 validation_features: Union[List, np.ndarray]=[]):
        super().__init__(scenario, 'Cost Sensitive Hierarchical Clustering', rng)
        self.min_partition_size = min_partition_size
        if max_partitions == -1:
            self.max_partitions = multiprocessing.cpu_count()
        else:
            self.max_partitions = max_partitions
        self.n_features = validation_features.shape[1]
        self.mean_feature_values = np.mean(validation_features, axis=0)
        self.max_feature_values = np.max(validation_features, axis=0)
        self.min_feature_values = np.min(validation_features, axis=0)
        self.validation_features = validation_features
        self.instances = validation_instances
        self.validation_results = validation_results
        self.incumbent_list = incumbent_list
        self._random_splits = random_splits
        self._lower_bound = None
        self.regularize = regularize
        self.mean_time_per_split = 0.
        self._num_performed_splits = 0
        self.budget_per_partition = float('inf')

        self.partial_partition_loss_function = functools.partial(self._get_partition_loss,
                                                                 validation_results, incumbent_list)

        # THis part is to get a rough estimate of how long the whole splitting process will take and to predict
        # the lower bounds if one was to perform perfect per instance algorithm configuration
        self.logger.debug('Predicting per-instance lower-bounds')
        res = list(map(lambda x: self.partial_partition_loss_function(x),
                       list(map(lambda x: [x], self.instances))))
        self._lower_bound = dict((inst[0], prediction) for prediction, inst, conf in res)

    def _get_partition_loss(self, valid_dict: dict, incumbents: List, insts: list):
        """
            get loss of choosing a per-instance-set configuration in comparison to a per-instance configuration
        """
        perf = [[] for _ in incumbents]
        for inst in insts:
            for idx, c in enumerate(incumbents):
                perf[idx].append(np.mean(valid_dict[inst][c]))
        #import pdb; pdb.set_trace()
        perf = np.sum(perf, axis=1)
        logging.debug('Partition loss: ' + str(perf))

        return np.min(perf), insts, incumbents[np.argmin(perf)]

    def partition(self, stats: PIACStats):
        """
        Method that groups instances together for wich a given performance is deemed the most optimal
        :return: list of partitions (leaves of the partition tree), root_node of the partition_tree
        """
        root = TreeNode(instances=list(range(len(self.instances))),
                        instance_names=np.array(self.instances),
                        instance_features=self.instance_feat_dict,
                        is_root=True, children=[], split_criterion=None,
                        parent=None, depth=0, scenario=None, left=None, name='S', configuration=None)

        partitions = [root]
        changed = True
        p_loss = {}
        p_split = {}
        while len(partitions) < self.max_partitions and changed:
            changed = False
            for idx, partition in enumerate(partitions):
                if partition.name in p_loss:
                    continue
                split_loss, split_, split_crit = self.split(partition, self.partial_partition_loss_function, stats)
                if split_:
                    p_loss[partition.name] = split_loss
                    p_split[partition.name] = (split_, split_crit, idx)
                    changed = True
            if p_loss:
                split_partition_name = min(p_loss, key=p_loss.get)  # Find the key with the minimum value in the dictionary
                split_, split_crit, idx = p_split[split_partition_name]
            else:
                assert not changed
            if changed or p_loss:
                split_partition = partitions.pop(idx)
                left_node = TreeNode(instances=split_[0][2], instance_names=split_[0][1],
                                     parent=split_partition, depth=split_partition.depth + 1,
                                     instance_features=self.instance_feat_dict, left=True, children=[],
                                     is_root=False, split_criterion=None, scenario=None,
                                     name=split_partition.name + '-L', loss=split_[0][0], configuration=split_[0][3])
                right_node = TreeNode(instances=split_[1][2], instance_names=split_[1][1],
                                      parent=split_partition, depth=split_partition.depth + 1,
                                      instance_features=self.instance_feat_dict, left=False, children=[],
                                      is_root=False, split_criterion=None, scenario=None,
                                      name=split_partition.name + '-R', loss=split_[1][0], configuration=split_[1][3])
                split_partition.instance_names = None
                split_partition.instances = None
                if not split_partition.name == 'S':
                    split_partition.instance_features = None
                split_partition.split_criterion = split_crit
                split_partition.children.append(left_node)
                split_partition.children.append(right_node)
                partitions.append(left_node)
                partitions.append(right_node)
            if p_loss:
                del p_loss[split_partition_name], p_split[split_partition_name]
        return np.array(partitions), root

    def _random_axis_aligned_split(self, instances: list):
        """
        Randomly select a random instance and feature value (at index i) in that instance as splitting criterion.
        Will be used to sort instances as follows:
        if inst_j[i] > selected_inst[i] do insertion into set A else B
        :param instances: list of instance names. A random instance will be selected from here
        :return: splitting_criterion (split_index, split_value)
        """
        random_instance = self.random_state.choice(instances)
        feature_vector = self.instance_feat_dict[random_instance]
        split_index = self.random_state.randint(0, self.n_features)
        split_value = feature_vector[split_index]
        return AxisAlignedSplit(split_index, split_value)

    def _random_non_axis_aligned_split(self, mean=None, max_=None):
        """
        Generate a support vector and random direction vector to split all resulting values.
        Will be used to sort instances as follows:
        if np.dot((instance_features - support_vector), direction_vector) > 0 insert into set A else B
        :return: splitting_criterion (support_vector, direction_vector)
        """
        if mean is None:
            mean = self.mean_feature_values
        if max_ is None:
            max_ = self.max_feature_values
        support_vector = self.random_state.randn(self.n_features) + mean
        support_vector[max_ == 0] = 0
        dir_vector = self.random_state.randn(self.n_features)
        return NonAxisAlignedSplit(support_vector, dir_vector)

    def _split_insts_given_crit(self, instance_names: list, instance_ids: list, crit: AbstractSplit):
        """
        Method to split a given set of instances (as names and ids) using the specified criterion
        :param instance_names: List of instance names ['instance_xyz' , ...]
        :param instance_ids: List of instance ids [9, 15, 19, 100, ...]
        :param crit: The splitting criterion as tuple (see _random_..._split methods)
        :return: list of instance ids "over" the split value
                 list of instance names "over" the split value
                 list of instance ids "below or equal to" the split value
                 list of instance names "below oor equal to" the split value
        """
        id_set_a = []
        name_set_a = []
        name_set_b = []
        id_set_b = []
        for instance, id_ in zip(instance_names, instance_ids):
            if crit(self.instance_feat_dict, instance):
                id_set_a.append(id_)
                name_set_a.append(instance)
            else:  # IDEA it might be possible to predict where to split not just randomly split
                id_set_b.append(id_)
                name_set_b.append(instance)
        return id_set_a, name_set_a, id_set_b, name_set_b

    def split(self, partition: TreeNode, partition_loss_function: callable,
              stats: PIACStats, axis_aligned: bool=False):
        """
        Method to split an existing partition into smaller subsets
        :param partition: The partition in question to split
        :param partition_loss_function: loss_function to determine best split
        :param stats: Stats object to keep track of remaining time
        :param axis_aligned: boolean, determines if the split is along the axis or not
        :return: tuple (resulting partitions), float (loss as sum of the splits), tuple (splitting criterion see
                                                                                       _random_..._split methods)
        """
        best_loss = np.float('inf')
        best_split = None
        best_split_crit = None

        # IDEA Learn the partition sizes in subsequent iterations
        if len(partition.instance_names) < self.min_partition_size:
            self.logger.debug('Too small to further split!')
            return best_loss, best_split, best_split_crit

        empty_count = 0
        left_ = [np.float('inf'), np.array([])]
        right_ = [np.float('inf'), np.array([])]
        split_num = 0
        while split_num < self._random_splits:  # randomly split M times
            if stats.is_budget_exhausted():
                self.logger.warning('Budget exhausted!')
            self._num_performed_splits += 1
            start = time.time()
            if axis_aligned:
                split_crit = self._random_axis_aligned_split(partition.instance_names)
            else:
                part_feats = self.validation_features[partition.instances]
                mean = np.mean(part_feats, axis=0)
                max_ = np.max(part_feats, axis=0)
                split_crit = self._random_non_axis_aligned_split(mean=mean, max_=max_)

            left_ids, left_names, right_ids, right_names = self._split_insts_given_crit(partition.instance_names,
                                                                                        partition.instances,
                                                                                        split_crit)
            if not (left_ids and right_ids):
                empty_count += 1
                if self._random_splits <= 1:
                    if empty_count % 10 == 1:
                        split_num += 1
                else:
                    split_num += 1
                # logging.debug("EMPTY SPLIT!!!")
                continue
            if len(left_ids) < self.min_partition_size or len(right_ids) < self.min_partition_size:
                logging.debug('SPLIT TOO SMALL')
                if self._random_splits <= 1:
                    if empty_count % 10 == 1:
                        split_num += 1
                else:
                    split_num += 1
                continue

            # compute the loss of the individual partitions
            left_loss, _, left_conf = partition_loss_function(left_names)
            right_loss, _, right_conf = partition_loss_function(right_names)

            loss, sum_lower_bounds = self._get_split_loss(left_names, left_loss, right_names, right_loss)
            if loss < best_loss:  # track best loss
                # sort IDS for easier comparison of partitions later on
                left_indices = [i[0] for i in sorted(enumerate(left_ids), key=lambda x: x[1])]
                right_indices = [i[0] for i in sorted(enumerate(right_ids), key=lambda x: x[1])]
                left_ = [left_loss, np.array(left_names)[left_indices], np.array(left_ids)[left_indices], left_conf]
                right_ = [right_loss, np.array(right_names)[right_indices], np.array(right_ids)[right_indices],
                          right_conf]
                best_loss = loss
                best_split = [left_, right_]
                best_split_crit = split_crit

            self.logger.info('SplitNum %d of %d' % (split_num, self._random_splits))
            self.logger.info('SplitCrit: %s' % str(split_crit))
            self.logger.info('SplitLoss %3.3f / Sum left+right %3.3f / Sum_Lower_Bounds %3.3f' % (
                loss, left_loss + right_loss, sum_lower_bounds))
            self.logger.info('    Split: %s' % str([len(left_names), len(right_names)]))
            self.logger.info('~'*80)
            self.logger.info('Best SplitCrit: %s' % str(best_split_crit))
            self.logger.info('Best loss %3.3f / Sum left+right %3.3f' % (best_loss, left_[0] + right_[0]))
            self.logger.info('    Best Split: %s' % str([left_[1].shape[0], right_[1].shape[0]]))
            self.logger.info('#'*80)
            taken = time.time() - start
            self.mean_time_per_split = self.mean_time_per_split + ((taken - self.mean_time_per_split
                                                                    ) / self._num_performed_splits)
            self.logger.debug('Split takes %3.5f sec on average' % self.mean_time_per_split)
            split_num += 1

        self.logger.debug('percent empty splits %2.2f' % (empty_count/float(self._random_splits)))
        return best_loss, best_split, best_split_crit

    def _get_split_loss(self, left_partition_instances: list, left_partition_loss: float,
                        right_partition_instances: list, right_partition_loss: float):
        """
        Helper method to compute the loss of the current partition split as a function of
        the mean_partition_loss - predicted_instance_specific_loss
        :param left_partition_instances: list of instances in the left child
        :param left_partition_loss: associated loss for left child
        :param right_partition_instances: list of instances in the right child
        :param right_partition_loss: associated loss of right child
        :return: sum((left_loss - predicted_lower_bound_inst_specific_in_left_child),
                     (right_loss - predicted_lower_bound_ins_specific_in_right_child))
        """
        left_lower_bound = sum(self._lower_bound[inst_] for inst_ in left_partition_instances)
        right_lower_bound = sum(self._lower_bound[inst_] for inst_ in right_partition_instances)

        left_loss = (left_partition_loss - left_lower_bound)
        right_loss = (right_partition_loss - right_lower_bound)

        dist = abs(len(left_partition_instances) - len(right_partition_instances))
        scale = min(1., 1. / np.log(dist))
        self.logger.debug(dist * scale)

        if self.regularize:
            return (left_loss + right_loss + dist * scale), (left_lower_bound + right_lower_bound)
        else:
            return left_loss + right_loss, (left_lower_bound + right_lower_bound)
