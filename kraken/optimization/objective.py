import logging

import numpy as np

from smac.runhistory.runhistory import RunKey
from smac.optimizer.objective import _runtime, _cost


#
# RunKey = collections.namedtuple(
#     'RunKey', ['config_id', 'instance_id', 'seed'])
#
# InstSeedKey = collections.namedtuple(
#     'InstSeedKey', ['instance', 'seed'])
#
# RunValue = collections.namedtuple(
#     'RunValue', ['cost', 'time', 'status', 'additional_info'])
#

logger = logging.getLogger(__name__)


def _portfolio_runtime(config, run_history, instance_seed_pairs=None, portfolio=None, portfolio_runhistory=None):
    if portfolio:
        portfolio_ids = []
        id_to_inst_seed = {}
        for portfolio_config in portfolio:
            try:
                portfolio_ids.append(portfolio_runhistory.config_ids[portfolio_config])
            except KeyError:
                raise Exception('Portfolio Configuration has to have at least one run!')
        try:
            id_ = run_history.config_ids[config]
        except KeyError:  # challenger was not running so far
            logger.debug('=============================> Challenger was not running so far!')
            return []

        if instance_seed_pairs is None:
            instance_seed_pairs = run_history.get_runs_for_config(config)
            id_to_inst_seed[id_] = instance_seed_pairs
        for portfolio_config, portfolio_id in zip(portfolio, portfolio_ids):
            id_to_inst_seed[portfolio_id] = sorted(portfolio_runhistory.get_runs_for_config(portfolio_config),
                                                   key=lambda x: x[0])

        runtimes = []
        for i, r in instance_seed_pairs:
            tmp = []
            k = RunKey(id_, i, r)
            tmp.append(run_history.data[k].time)
            logger.debug('Config has runtime: %f' % tmp[-1])
            for idx, _id in enumerate(portfolio_ids):
                k = RunKey(_id, i, r)
                try:
                    tmp.append(portfolio_runhistory.data[k].time)
                    logger.debug('Portfolio %d has runtime: %f' % (idx, tmp[-1]))
                except KeyError:
                    # logger.debug('Portfolio %d not run with inst/seed pair %s' % (idx, str((i, r))))
                    tmp_ = []
                    for j, s in id_to_inst_seed[_id]:
                        if j == i:
                            tmp_.append((j, s))
                    tm_str = ''
                    if tmp_:
                        # logger.debug('Using mean across instance instead')
                        tmp.append(np.mean(_runtime(portfolio[idx], portfolio_runhistory, tmp_)))
                        tm_str = '\tmean'
                    else:
                        tmp.append(np.float('inf'))
                    logger.debug('Portfolio %d has runtime: %f%s' % (idx, tmp[-1], tm_str))
            at = np.argmin(tmp)
            logger.debug('Chose %d' % at)
            runtimes.append(tmp[at])
        return runtimes
    else:
        return _runtime(config, run_history, instance_seed_pairs)


def _portfolio_cost(config, run_history, instance_seed_pairs=None, portfolio=None, portfolio_runhistory=None):
    if portfolio:
        portfolio_ids = []
        id_to_inst_seed = {}
        for portfolio_config in portfolio:
            try:
                portfolio_ids.append(portfolio_runhistory.config_ids[portfolio_config])
            except KeyError:
                raise Exception('Portfolio Configuration has to have at least one run!')
        try:
            id_ = run_history.config_ids[config]
        except KeyError:  # challenger was not running so far
            logger.debug('=============================> Challenger was not running so far!')
            return []

        if instance_seed_pairs is None:
            instance_seed_pairs = run_history.get_runs_for_config(config)
            id_to_inst_seed[id_] = instance_seed_pairs
        for portfolio_config, portfolio_id in zip(portfolio, portfolio_ids):
            id_to_inst_seed[portfolio_id] = sorted(portfolio_runhistory.get_runs_for_config(portfolio_config),
                                                   key=lambda x: x[0])

        costs = []
        for i, r in instance_seed_pairs:
            tmp = []
            k = RunKey(id_, i, r)
            tmp.append(run_history.data[k].cost)
            logger.debug('Config has cost: %f' % tmp[-1])
            for idx, _id in enumerate(portfolio_ids):
                k = RunKey(_id, i, r)
                try:
                    tmp.append(portfolio_runhistory.data[k].cost)
                    logger.debug('Portfolio %d has cost: %f' % (idx, tmp[-1]))
                except KeyError:
                    # logger.debug('Portfolio %d not run with inst/seed pair %s' % (idx, str((i, r))))
                    tmp_ = []
                    for j, s in id_to_inst_seed[_id]:
                        if j == i:
                            tmp_.append((j, s))
                    tm_str = ''
                    if tmp_:
                        # logger.debug('Using mean across instance instead')
                        tmp.append(np.mean(_cost(portfolio[idx], portfolio_runhistory, tmp_)))
                        tm_str = '\tmean'
                    else:
                        tmp.append(np.float('inf'))
                    logger.debug('Portfolio %d has cost: %f%s' % (idx, tmp[-1], tm_str))
            at = np.argmin(tmp)
            logger.debug('Chose %d' % at)
            costs.append(tmp[at])
        return costs
    else:
        return _cost(config, run_history, instance_seed_pairs)


def total_portfolio_runtime(config, run_history, instance_seed_pairs=None, portfolio=None, portfolio_runhistory=None):
    return np.sum(_portfolio_runtime(config, run_history, instance_seed_pairs, portfolio, portfolio_runhistory))


def average_portfolio_cost(config, run_history, instance_seed_pairs=None, portfolio=None, portfolio_runhistory=None):
    return np.mean(_portfolio_cost(config, run_history, instance_seed_pairs, portfolio, portfolio_runhistory))


def sum_portfolio_cost(config, run_history, instance_seed_pairs=None, portfolio=None, portfolio_runhistory=None):
    return np.sum(_portfolio_cost(config, run_history, instance_seed_pairs, portfolio, portfolio_runhistory))
