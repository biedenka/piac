import numpy as np
from scipy.stats import norm

from smac.optimizer.acquisition import AbstractAcquisitionFunction


"""
Created on November 14, 2016

@author: André Biedenkapp
"""


class PEI(AbstractAcquisitionFunction):
    """
    Implementation of Profile Expected Improvement.
    """
    def __init__(self, model, min_cost=None):
        super(PEI, self).__init__(model=model)
        self.long_name = 'Profile Expected Improvement'
        self.min_ = min_cost

    def _compute(self, X, derivative=False, *args):
        """
        Implementation of the profile expected improvement
        :param X: Configuration to evaluate
        :param derivative:
        :param args: instance (features) to evaluate X on and observed instance optimum (value) to guide sampling
        :return:
        """
        if derivative:
            raise NotImplementedError

        if len(X.shape) == 1:
            X = X[:, np.newaxis]

        assert X.shape[0] == 1
        inst = args[0]

        min_ = self.min_
        if len(args) == 2:
            min_ = args[1]

        m, v = self.model.predict(np.hstack((X[0], inst))[np.newaxis, :])

        m = m[:, 0]
        v = v[:, 0]
        g = (min_ - m) / v
        pei = v * (g * norm.cdf(g) + norm.pdf(g))

        pei[v == 0.0] = 0

        return pei

    def __call__(self, X, *args):
        """
        TODO Had to override since AbstractAcqFunc wouldn't correctly pass args (due to conflicts with derivative)
        :param X:
        :param args:
        :return:
        """
        if len(X.shape) == 1:
            X = X[np.newaxis, :]

        acq = self._compute(X, False, *args)
        if np.any(np.isnan(acq)):
            idx = np.where(np.isnan(acq))[0]
            acq[idx, :] = -np.finfo(np.float).max
        return acq


class ExpectedImprovement(AbstractAcquisitionFunction):
    """
    Taken from SMAC and adjusted it to marginalize over samples of the
    """
    def __init__(self,
                 model,
                 par=0.01,
                 **kwargs):
        r"""
        Computes for a given x the expected improvement as
        acquisition value.
        :math:`EI(X) :=
            \mathbb{E}\left[ \max\{0, f(\mathbf{X^+}) -
                f_{t+1}(\mathbf{X}) - \xi\right] \} ]`, with
        :math:`f(X^+)` as the incumbent.

        Parameters
        ----------
        model: Model object
            A model that implements at least
                 - predict(X)
                 - getCurrentBestX().
            If you want to calculate derivatives than it should also support
                 - predictive_gradients(X)
        par: float
            Controls the balance between exploration
            and exploitation of the acquisition function. Default is 0.01
        """

        super().__init__(model)
        self.long_name = 'Expected Improvement'
        self.par = par
        self.eta = None

    def _compute(self, X, derivative=False, *args):
        """
        Computes the EI value and its derivatives.

        Parameters
        ----------
        X: np.ndarray(N, D), The input points where the acquisition function
            should be evaluated. The dimensionality of X is (N, D), with N as
            the number of points to evaluate at and D is the number of
            dimensions of one X.

        derivative: Boolean
            If is set to true also the derivative of the acquisition
            function at X is returned

        Returns
        -------
        np.ndarray(N,1)
            Expected Improvement of X
        np.ndarray(N,1)
            Derivative of Expected Improvement at X (only if derivative=True)
        """

        if len(X.shape) == 1:
            X = X[:, np.newaxis]


        m, v = self.model.predict_marginalized_over_instances(X, instances=None if len(args) == 0 else args[0])
        assert m.shape[1] == 1
        assert v.shape[1] == 1
        s = np.sqrt(v)

        if self.eta is None:
            raise ValueError('No current best specified. Call update('
                             'eta=<int>) to inform the acquisition function '
                             'about the current best value.')

        z = (self.eta - m - self.par) / s
        f = (self.eta - m - self.par) * norm.cdf(z) + s * norm.pdf(z)
        f[s == 0.0] = 0.0

        if derivative:
            dmdx, ds2dx = self.model.predictive_gradients(X)
            dmdx = dmdx[0]
            ds2dx = ds2dx[0][:, None]
            dsdx = ds2dx / (2 * s)
            df = (-dmdx * norm.cdf(z) + (dsdx * norm.pdf(z))).T
            df[s == 0.0] = 0.0

        if (f < 0).any():
            self.logger.error("Expected Improvement is smaller than 0!")
            raise ValueError

        if derivative:
            return f, df
        else:
            return f

    def __call__(self, X, *args):
        """
        TODO Had to override since AbstractAcqFunc wouldn't correctly pass args (due to conflicts with derivative)
        :param X:
        :param args:
        :return:
        """
        if len(X.shape) == 1:
            X = X[np.newaxis, :]

        acq = self._compute(X, False, *args)
        if np.any(np.isnan(acq)):
            idx = np.where(np.isnan(acq))[0]
            acq[idx, :] = -np.finfo(np.float).max
        return acq
