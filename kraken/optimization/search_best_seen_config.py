import logging
import numpy
from ConfigSpace.util import impute_inactive_values
from smac.runhistory.runhistory import RunHistory
from smac.configspace import Configuration

"""
Created on March 1st, 2017

@author: Andre Biedenkapp <biedenka@cs.uni-freiburg.de>
"""


class BruteForceBestSeenConfig(object):
    """
         iterated local search to optimize a function over a given parameter configuration space
    """

    def __init__(self, runHist: RunHistory, logy: bool=True):
        """
        Constructor
        """
        self.runHist = runHist
        self.configs = self.runHist.get_all_configs()
        self.logy = logy
        self.logger = logging.getLogger('BFBSC')

    def optimize(self, func: callable, pcs, feats: numpy.array, init_x: Configuration=None):
        """
        Only predict for configurations we've seen to prevent starting from bad locations
        :param func:
        :param pcs: Only used for compatibility reasons
        :param feats:
        :param init_x: Only used for compatibility reasons
        :return:
        """

        def opt_func(x: Configuration):
            x = impute_inactive_values(x).get_array()
            X = numpy.repeat(numpy.reshape(x, (1, x.shape[0])), feats.shape[0], axis=0)
            X = numpy.concatenate((X, feats), axis=1)
            means_, _ = func(X)
            if self.logy:
                return sum(numpy.power(10, means_))
            return sum(means_)

        best_x = None
        best_y = numpy.float('inf')
        for config in self.configs:
            current_y = opt_func(config)
            if current_y < best_y:
                best_x = config
                best_y = current_y

        return best_x, best_y
