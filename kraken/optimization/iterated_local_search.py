import logging
import numpy
import random
import copy

import numpy as np
from ConfigSpace.util import impute_inactive_values

from kraken.configspace import get_one_exchange_neighbourhood
from kraken.configspace import ConfigurationSpace, Configuration


"""
Created on March 1st, 2017

@author: Andre Biedenkapp <biedenka@cs.uni-freiburg.de>
"""


class IteratedLocalSearch(object):
    """
         iterated local search to optimize a function over a given parameter configuration space
    """

    def __init__(self, rng, restarts: int=4, max_steps: int=10, max_neighbors: int=20, logy: bool=True):
        """
        Constructor
        """
        self.rng = rng
        self.__RESTARTS = restarts
        self.__MAX_STEPS = max_steps
        self.__MAX_NEIGHBORS = max_neighbors
        self.logy = logy
        self.logger = logging.getLogger('ILS')

    def optimize(self, func: callable, pcs: ConfigurationSpace, feats: np.array, init_x: Configuration=None):
        """
            find a configuration in pcs (ConfigSpace) that optimizes func(x);
            starts at init if given or else with pcs.get_default_config_dict()
            :param func: function with input vec x that returns the predicted performance of x
            :param pcs: ConfigSpace object
            :param feats: matrix of instance features
            :param init_x: initial x (if None, use default config)
            :return: tuple of (i) optimized x on <func>  given <feats> and (ii) its performance
        """

        if not init_x:
            init_x = pcs.get_default_configuration()

        def opt_func(x: Configuration):
            x = impute_inactive_values(x).get_array()
            X = numpy.repeat(numpy.reshape(x, (1, x.shape[0])), feats.shape[0], axis=0)
            X = numpy.concatenate((X, feats), axis=1)
            means_, _ = func(X)
            if self.logy:
                return sum(np.power(10, means_))
            return sum(means_)

        best_y = opt_func(init_x)
        best_x = init_x
        # logging.debug("Init y: %.2f " % (best_y))
        confs_looked_at = 0

        for r in range(self.__RESTARTS):
            # logging.debug("ILS %d Restart" %(r))
            if r == 0:  # start with init in first iteration
                x = copy.deepcopy(best_x)
                y = best_y
            else:  # next iteration at new random x
                confs_looked_at += 1
                x = pcs.sample_configuration()
                y = opt_func(x)
            for _ in range(self.__MAX_STEPS):
                updated = False
                neighbors = list(get_one_exchange_neighbourhood(x, seed=12345))
                self.rng.shuffle(neighbors)
                neighbors = neighbors[:self.__MAX_NEIGHBORS]
                for new_x in neighbors:  # selection: first improving neighbor
                    confs_looked_at += 1
                    new_x = new_x
                    new_y = opt_func(new_x)
                    # logging.debug("New y: %.5f" % (new_y))
                    if new_y < y:
                        self.logger.debug('Switching to one of the neighbors')
                        y, x = new_y, new_x
                        updated = True
                        break
                if not updated:
                    break
            if y < best_y:
                self.logger.debug('Switched to one of the neighbors')
                best_y, best_x = y, x
        # logging.debug("Best y:x %.4f %s" % (best_y, str(best_x)))
        self.logger.debug('DONE: Looked at %d configurations' % confs_looked_at)

        return best_x, best_y




