# -*- coding: utf-8 -*-

import logging
import time

import numpy as np

from smac.utils.util_funcs import get_types
from smac.runhistory.runhistory import RunHistory
from smac.runhistory.runhistory2epm import RunHistory2EPM4LogCost, RunHistory2EPM4Cost
from smac.optimizer.objective import average_cost
from smac.epm.rfr_imputator import RFRImputator
from smac.scenario.scenario import Scenario

from kraken.epm.rf_with_instances import RandomForestWithInstances
from kraken.optimization import LocalSearch
from kraken.optimization.profile_expected_improvement import PEI
from kraken.util.n_ary_tree import TreeNode
from kraken.tae import ExecuteTARun, ExecuteTARunOld, StatusType

from kraken.configspace.partition_config_space import GMeans

""""
Created on November 8, 2016

@author: André Biedenkapp
"""


class BudgetExhaustedException(Exception):
    pass


class ImpProfiler(object):
    """
    Improvement Profiler. Uses the profile expected improvement to guide the sampling of configurations per instance
    to learn a diverse Model wich can further be used in the PIAC workflow.
    """

    def __init__(self, scenario: Scenario, rng: np.random.RandomState, acq=PEI, tae: ExecuteTARun = None, stats=None,
                 per_partition_time: float=None):
        """
        Constructor
        :param scenario: SMAC scenario object
        :param rng: seed for the random state
        :param acq: Acquisition function to use. Default Profile Expected Improvement
        """
        self.logger = logging.getLogger('ImpProfiler')
        self.scenario = scenario
        self.default = self.scenario.cs.get_default_configuration()

        self.best_on_inst = None
        self.predicted_best = None
        self.model = None
        if tae is None:
            self.runhist = RunHistory(aggregate_func=average_cost)
            self.tae = ExecuteTARunOld(ta=self.scenario.ta,
                                       run_obj=self.scenario.run_obj,
                                       par_factor=self.scenario.par_factor,
                                       runhistory=self.runhist,
                                       stats=stats)
        else:
            self.tae = tae
            self.runhist = tae.runhistory

        self.stats = stats

        self.random_state = rng
        self.train_insts = np.array(self.scenario.train_insts)
        self.num_train_insts = self.train_insts.shape[0]

        params = self.scenario.cs.get_hyperparameters()
        self.n_feats = len(list(self.scenario.feature_dict[self.train_insts[0]]))
        self.n_param = len(params)
        self.model_types, self.model_bounds = get_types(self.scenario.cs, scenario.feature_array)
        self.logger.debug(self.model_types.shape)

        inst_matrix = np.full((self.train_insts.shape[0], self.n_feats), 0., dtype=float)
        for idx, inst in enumerate(self.train_insts):
            inst_matrix[idx] = self.scenario.feature_dict[inst]

        self.model = RandomForestWithInstances(types=self.model_types, bounds=self.model_bounds,
                                               instance_features=inst_matrix, seed=rng.randint(10000))
        self.optimizer = None
        self.acq = acq

        num_params = len(scenario.cs.get_hyperparameters())

        if scenario.run_obj == "runtime":
            # if we log the performance data,
            # the RFRImputator will already get
            # log transform data from the runhistory
            cutoff = np.log10(scenario.cutoff)
            threshold = np.log10(scenario.cutoff *
                                 scenario.par_factor)

            imputor = RFRImputator(rng=self.random_state,
                                   cutoff=cutoff,
                                   threshold=threshold,
                                   model=self.model,
                                   change_threshold=0.01,
                                   max_iter=2)

            runhistory2epm = RunHistory2EPM4LogCost(
                scenario=scenario, num_params=num_params,
                success_states=[StatusType.SUCCESS, ],
                impute_censored_data=True,
                impute_state=[StatusType.CAPPED, StatusType.TIMEOUT],
                imputor=imputor,
                rng=self.random_state)
        elif scenario.run_obj == 'quality':
            runhistory2epm = RunHistory2EPM4Cost(scenario=scenario, num_params=num_params,
                                                 success_states=[
                                                     StatusType.SUCCESS, ],
                                                 impute_censored_data=False, impute_state=None,
                                                 rng=self.random_state)
        else:
            raise ValueError('Unknown run objective: %s. Should be either '
                             'quality or runtime.' % self.scenario.run_obj)
        self.rh2EPM = runhistory2epm
        self.overall_evals = 0

        split_start = time.time()
        gmeans = GMeans(scenario=scenario, minimum_samples_per_cluster=int(len(scenario.train_insts) * 0.125),
                        rng=rng, per_partition_time=per_partition_time)
        _ = gmeans.partition()
        self.split_time = time.time() - split_start
        self.partition_list = gmeans.nodes

    def _sample_config(self, num_configs: int = 1):
        configs = self.scenario.cs.sample_configuration(size=num_configs)
        for idx, config in enumerate(configs):
            configs[idx] = config
        return configs

    def _run_conf_on_inst(self, config, inst):
        """
        Runs config on inst
        :param config: config (dict)
        :param inst: inst id (string)
        :return: (Status [1-5], runtime, cost, inst_specific) 1 = SUCCESS, 2 = TIMEOUT other Status IDS indicate crashed
        """

        if self.stats.is_budget_exhausted():
            raise BudgetExhaustedException('Budget exhausted! Skipping algorithm run.')
        config._populate_values()
        empirical_result = self.tae.start(config, instance=inst,
                                          cutoff=self.scenario.cutoff,
                                          seed=self.random_state.randint(0, 999999),
                                          instance_specific="0")

    def _sample_inst(self, num_insts: int = 1, equal=False):  # Allows for duplicates!!!
        if num_insts < len(self.partition_list):
            rand_list = self.random_state.randint(0, self.num_train_insts, size=num_insts)
            return self.train_insts[rand_list], rand_list
        else:
            per_part = int(num_insts / len(self.partition_list))
            res = []
            res_id = []
            for p in self.partition_list:
                if not equal:
                    per_part = round((p.instances.shape[0] / self.num_train_insts) * num_insts)
                tmp = np.random.permutation(p.instances.shape[0])[:per_part]
                res.extend(p.instance_names[tmp])
                res_id.extend(p.instances[tmp])
            tmp_res, tmp_id = self._sample_inst(max(num_insts - len(res), 0), equal=equal)
            res.extend(tmp_res)
            res_id.extend(tmp_id)
            res = np.array(res)
            res_id = np.array(res_id)
            return res, res_id

    def initial_random_sampling(self, random_samples: int = 100, run_default: int = 10):
        """
        Create random configuration and instance combinations to evaluate. It is so "expensive" as in practice
        We expect data-sets with hundreds to thousands of instances. Thus we might get
        :param random_samples: Number of completely random samples
        :param run_default: How many additional samples are (default_config, instance) pairs
        :return: List of random configurtions, List of random instances, List of size m where each entry is the def
        configuration, List of m additional random instances
        """
        if not (random_samples or run_default):
            raise ValueError('At least one of the sample sizes has to be greater than 0!', random_samples, run_default)
        if random_samples > 0:
            random_configs = np.array(self._sample_config(num_configs=random_samples))
        else:
            random_configs = np.array([])
        random_insts, _ = self._sample_inst(num_insts=random_samples, equal=True)
        default_insts, _ = self._sample_inst(num_insts=run_default, equal=True)
        return random_configs, random_insts, np.array([self.default for _ in range(run_default)]), default_insts

    def evaluate_configuration_instance_pairs(self, configs: np.array, insts: np.array) -> np.array:
        """
        Method to easily call SMAC's TAE and evaluate n configs on n instances
        :param configs: list of n configurations (dictionaries)
        :param insts: list of n instances (instance_ids i.e. strings)
        """
        assert configs.shape == insts.shape, 'For every configuration, one instance has to be specified to be run on!' \
                                             '\n (num_insts != num_configs) %d != %d' % (insts.shape[0],
                                                                                         configs.shape[0])
        for idx, config_inst in enumerate(zip(configs, insts)):
            self._run_conf_on_inst(config=config_inst[0], inst=config_inst[1])
            self.overall_evals += 1
            self.logger.info('Algorithm runs: %d\n%s\n' % (self.overall_evals, '~' * 120))

    def explore(self, initial_runs: int = 100, initial_def_runs: int = 10, exploration_evaluations: int = 10,
                num_instances_per_round: int = -1, only_eval_best: bool=True):
        """
        Main Method to construct a diverse model. First random instance configuration pairs are sampled as well as
        random instance, default configuration pairs which are queried and used to construct the initial model.
        This model is used to query new interesting instance/configuration pairs guided by the Profile Expected
        Improvement. These promising new pairs are evaluated and used to update the model.

        Intuitively a low number of initial runs will result in a less diverse model as few instance/configuration
        pairs will be evaluated.
        A low number of exploration rounds will result in few optimal configuration/instance pairs.
        => Good balance between initial random exploration and individual intensification!

        :param initial_runs: How many random samples are used to construct the initial Model
        :param initial_def_runs: How many additional runs are made for the default configuration to
                                 construct the initial model
        :param exploration_evaluations: additional guided exploration evaluations (using PEI)
        :param num_instances_per_round: Number of instances per exploration round
        :param only_eval_best
        :return: The trained Model
        """

        if num_instances_per_round <= 0:
            num_instances_per_round = len(self.train_insts)

        # Initial sampling and training
        rc, ri, dc, di = self.initial_random_sampling(random_samples=initial_runs, run_default=initial_def_runs)
        configs = np.hstack((rc, dc))
        insts = np.hstack((ri, di))
        try:
            self.evaluate_configuration_instance_pairs(configs=configs, insts=insts)
        except BudgetExhaustedException as e:
            if len(self.tae.runhistory.data) == 0:
                self.logger.exception(e)
                self.logger.error('No budget to create an initial Matrix! This has to fail!!!')
                raise BudgetExhaustedException('No budget to even start with one sample.')
        X, y = self.rh2EPM.transform(self.tae.runhistory)
        self.logger.debug('X.shape: %s' % str(X.shape))
        self.logger.debug('y.shape: %s' % str(y.shape))
        self.logger.debug('model_types.shape: %s' % str(self.model_types.shape))
        self.model.train(X, y)
        curr_best = y[np.argmin(y)]  # Get the current best as initial best

        # Main exploration loop.
        self.best_on_inst = np.array([curr_best for _ in range(len(self.train_insts))])
        configs = np.full((num_instances_per_round,), self.default, dtype=dict)
        self.optimizer = LocalSearch(acquisition_function=self.acq(self.model), config_space=self.scenario.cs,
                                     rng=self.random_state)
        tmp_round = 0
        self.stats.restart_ta_runs()

        if only_eval_best:
            self.stats.set_ta_run_limit(exploration_evaluations)
        else:
            self.stats.set_ta_run_limit(exploration_evaluations*num_instances_per_round)
        # randomly select #num_instances_per_round instances for this round
        round_instances, round_inst_ids = self._sample_inst(num_insts=num_instances_per_round)
        self.logger.debug('#Instances per round: %d' % len(round_instances))
        # Apparently the sampling sometimes returns too many instances! (Quick fix for that)
        self.round_instances = round_instances[:num_instances_per_round]
        self.logger.debug('#Instances per round: %d' % len(round_instances))
        self.round_inst_ids = round_inst_ids[:num_instances_per_round]
        cont = True
        while cont:
            acq_vs = []
            for idx, inst in enumerate(round_instances):  # First determine which configurations to sample next on the
                # given instances
                if not self.stats.is_budget_exhausted():
                    incumbent, acq_val_incumbent = self.optimizer.maximize(configs[idx],
                                                                           self.scenario.feature_dict[inst],
                                                                           self.best_on_inst[round_inst_ids[idx]])
                    acq_vs.append(acq_val_incumbent)
                    configs[idx] = incumbent
                else:
                    cont = False
                    break
            try:
                if only_eval_best:
                    at_ = np.argmin(acq_vs)
                    self.evaluate_configuration_instance_pairs(configs=np.array([configs[at_]]),
                                                               insts=np.array([round_instances[at_]]))
                else:
                    self.evaluate_configuration_instance_pairs(configs=configs, insts=round_instances)
            except (BudgetExhaustedException, ValueError):
                cont = False
            X, y = self.rh2EPM.transform(self.tae.runhistory)
            self.logger.debug('X.shape: %s' % str(X.shape))
            self.logger.debug('y.shape: %s' % str(y.shape))
            self.logger.debug('model_types.shape: %s' % str(self.model_types.shape))
            self.model.train(X, y)
            tmp_round += 1

        self.stats.print_stats(debug_out=False)
        self.logger.debug(X.shape)
        self.logger.debug(y.shape)
        return self.model
