import logging
import multiprocessing
import queue

"""
Created on March 6th, 2017

@author: Andre Biedenkapp <biedenka@cs.uni-freiburg.de>
"""

class Parallelizer(object):
    """
    Class to handle parallelization of splitting Clusters
    """
    def __init__(self, threads):
        self._num_threads = threads
        self.logger = logging.getLogger("Parallelizer")

    def _paralell_func(self, func, in_: multiprocessing.Queue, out_: multiprocessing.Queue):
        """
        Get output of paralell function from queues
        :param func: function to apply in parallel
        :param in_: in Queue
        :param out_: out Queue
        :return: None
        """
        while True:
            try:
                element = in_.get_nowait()
            except queue.Empty:
                break
            out_.put(func(element))

    def parallel_map(self, func, map_list):
        """
        Implementation of map in parallel
        :param func: function to be applied in parallel
        :param map_list: list of elements to apply function to
        :return: list of results of applying func in parallel to each element of map_list
        """
        in_queue = multiprocessing.Queue()
        out_queue = multiprocessing.Queue()

        for element in map_list:
            in_queue.put(element)

        processes = [multiprocessing.Process(target=self._paralell_func(func, in_queue, out_queue))
                     for _ in range(self._num_threads)]

        for process in processes:
            process.start()

        result_list = []
        for _ in range(len(map_list)):
            self.logger.debug('At %d' % _)
            res_ = out_queue.get()
            result_list.append(res_)
        assert len(result_list) == len(map_list), 'Parallelizer failed'
        return result_list
