import logging
import copy
import json
import os
import datetime
import time
from typing import Union

import numpy as np

from smac.runhistory.runhistory2epm import RunHistory2EPM4Cost, RunHistory2EPM4LogCost, RunHistory
from smac.tae.execute_ta_run import StatusType
from smac.tae.execute_ta_run_aclib import ExecuteTARunAClib
from smac.tae.execute_ta_run_old import ExecuteTARunOld, ExecuteTARun
from smac.facade.roar_facade import ROAR
from smac.facade.smac_facade import SMAC
from smac.stats.stats import Stats
from smac.scenario.scenario import Scenario

from kraken.util.n_ary_tree import TreeNode
from kraken.util.stats import PIACStats
from kraken.configspace import CategoricalHyperparameter, Configuration, FloatHyperparameter, IntegerHyperparameter
from kraken.configspace import ConfigurationSpace

scenario_option_names = {'algo-exec': 'algo',
                         'algo': 'algo',
                         'algoExec': 'algo',
                         'algo-exec-dir': 'execdir',
                         'exec-dir': 'execdir',
                         'execDir': 'execdir',
                         'execdir': 'execdir',
                         'algo-deterministic': 'deterministic',
                         'deterministic': 'deterministic',
                         'paramFile': 'paramfile',
                         'paramfile': 'paramfile',
                         'pcs-file': 'paramfile',
                         'param-file': 'paramfile',
                         'run-obj': 'run_obj',
                         'run_obj': 'run_obj',
                         'run-objective': 'run_obj',
                         'runObj': 'run_obj',
                         'overall_obj': 'overall_obj',
                         'intra-obj': 'overall_obj',
                         'intra-instance-obj': 'overall_obj',
                         'overall-obj': 'overall_obj',
                         'intraInstanceObj': 'overall_obj',
                         'overallObj': 'overall_obj',
                         'intra_instance_obj': 'overall_obj',
                         'algo-cutoff-time': 'cutoff_time',
                         'abort_on_first_run_crash': 'abort_on_first_run_crash',
                         'intensification_percentage': 'intensification_percentage',
                         'cost_for_crash': 'cost_for_crash',
                         'minR': 'minR',
                         'maxR': 'maxR',
                         'shared_model': 'shared_model',
                         'initial_incumbent': 'initial_incumbent',
                         'always_race_default': 'always_race_default',
                         'target-run-cputime-limit': 'cutoff_time',
                         'target_run_cputime_limit': 'cutoff_time',
                         'cutoff_time': 'cutoff_time',
                         'cutoff-time': 'cutoff_time',
                         'cutoffTime': 'cutoff_time',
                         'cputime-limit': 'tunerTimeout',
                         'cputime_limit': 'tunerTimeout',
                         'tunertime-limit': 'tunerTimeout',
                         'tuner-timeout': 'tunerTimeout',
                         'tunerTimeout': 'tunerTimeout',
                         'wallclock_limit': 'wallclock-limit',
                         'wallclock-limit': 'wallclock-limit',
                         'runtime-limit': 'wallclock-limit',
                         'runtimeLimit': 'wallclock-limit',
                         'wallClockLimit': 'wallclock-limit',
                         'output-dir': 'outdir',
                         'output_dir': 'outdir',
                         'outputDirectory': 'outdir',
                         'instances': 'instance_file',
                         'instance-file': 'instance_file',
                         'instance_file': 'instance_file',
                         'instance-dir': 'instance_file',
                         'instanceFile': 'instance_file',
                         'i': 'instance_file',
                         'instance_seed_file': 'instance_file',
                         'test-instances': 'test_instance_file',
                         'test-instance-file': 'test_instance_file',
                         'test-instance-dir': 'test_instance_file',
                         'testInstanceFile': 'test_instance_file',
                         'test_instance_file': 'test_instance_file',
                         'test_instance_seed_file': 'test_instance_file',
                         'feature-file': 'feature_file',
                         'instanceFeatureFile': 'feature_file',
                         'feature_file': 'feature_file',
                         'runcount-limit': 'runcount-limit',
                         'runcount_limit': 'runcount-limit',
                         'totalNumRunsLimit': 'runcount-limit',
                         'numRunsLimit': 'runcount-limit',
                         'numberOfRunsLimit': 'runcount-limit'}


def construct_scenario(original_scenario: Scenario, cluster: TreeNode, opt_time_per_cluster):
    """
    Helper method to create a new scenario, for a smaller feature_space from an existing scenario
    :param original_scenario: Original Scenario object
    :param cluster: Container to assign Scenario to
    :param opt_time_per_cluster: Time in sec to optimize on cluster
    :return: new scenario
    """
    # init
    logger = logging.getLogger('ScenGen')
    new_scen = copy.deepcopy(original_scenario)
    logger.debug('Creating new Scenario')
    new_scen.feature_dict = cluster.instance_features
    new_scen.train_insts = cluster.instance_names

    # replace info of the old scenario with info of the new scenario (contained in cluster)
    split_ = os.path.split(original_scenario.output_dir)
    if 'PIAC' in split_[1]:
        new_scen_dir = os.path.split(original_scenario.output_dir)[0]
    else:
        new_scen_dir = original_scenario.output_dir
    time_stamp = "PIAC-output_%s" % (datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S'))
    new_scen.output_dir = os.path.join(new_scen_dir, time_stamp, cluster.name)
    new_scen.wallclock_limit = opt_time_per_cluster
    return new_scen


def refit_model(rh2epm: Union[RunHistory2EPM4LogCost,
                              RunHistory2EPM4Cost],
                model, runhistory: RunHistory):
    X, Y = rh2epm.transform(runhistory)

    model.train(X, Y)
    return model


def get_tae(tae_str, scenario: Scenario, runhist: RunHistory, stats: Union[PIACStats, Stats]) -> ExecuteTARun:
    """
    Convenience Method to get the Target Algorithm Executor in one line of code where needed
    :param tae_str: str in [aclib, old]
    :param scenario: scenario object
    :param runhist: runhist object
    :param stats: stats object
    :return: TargetAlgorithmExecutor
    """
    if tae_str == 'aclib':
        tae = ExecuteTARunAClib(ta=scenario.ta,
                                run_obj=scenario.run_obj,
                                par_factor=scenario.par_factor,
                                runhistory=runhist,
                                stats=stats)
    else:
        tae = ExecuteTARunOld(ta=scenario.ta,
                              run_obj=scenario.run_obj,
                              par_factor=scenario.par_factor,
                              runhistory=runhist,
                              stats=stats)
    return tae


def remove_inactive_params(cs: ConfigurationSpace, config: Union[Configuration, dict]) -> Configuration:
    """
    Slightly modified code from ConfigSpace in config_space._check_configuration  (inefficient)
    :param scenario: SMAC scenario that contains a config space object
    :param config: The configuration that has to get it's inactive parameters removed
    :return: config without inactive parameters
    """
    try:
        conf_dict = config.get_dictionary()
    except AttributeError:
        conf_dict = config
    found_inactive = True
    deactivated_list = []
    while found_inactive:
        found_inactive = False
        for param in cs.get_hyperparameters():
            if param.name not in conf_dict or conf_dict[param.name] is None or param.name in deactivated_list:
                continue
            conditions = cs._get_parent_conditions_of(param.name)

            active = True
            for condition in conditions:
                parent_names = [c.parent.name for c in
                                condition.get_descendant_literal_conditions()]

                try:
                    parents = {parent_name: conf_dict[parent_name] for
                               parent_name in parent_names}
                except KeyError:
                    active = False
                    break

                # if one of the parents is None, the hyperparameter cannot be
                # active! Else we have to check this
                if any([parent_value is None for parent_value in
                        parents.values()]):
                    active = False
                    break

                else:
                    if not condition.evaluate(parents):
                        active = False
                        break

            if not active and conf_dict[param.name] is not None:
                for child in cs.get_children_of(param):
                    try:
                        del conf_dict[child.name]
                        deactivated_list.append(child.name)
                        found_inactive = True
                    except KeyError:
                        pass
                try:
                    del conf_dict[param.name]
                    deactivated_list.append(param.name)
                    found_inactive = True
                    logging.debug('\t\tDEACTIVATING INACTIVE PARAM %s' % param)
                except KeyError:
                    pass

    return Configuration(configuration_space=cs, values=conf_dict)


class PIACBudgetExhaustedException(Exception):
    """ Custom exception for exhausted PIAC budget """
    pass


def get_optimizer(tae_str: str, modus: str, scenario: Scenario, runhist: RunHistory, stats: Union[PIACStats, Stats],
                  rng: np.random.RandomState, init_conf: Configuration, init_runhist: RunHistory = None
                  ) -> Union[SMAC, ROAR]:
    """
    Convenience Method to get the optimizier object in one line of code where wanted
    :param tae_str: str in [aclib, old]
    :param modus: str in [SMAC, ROAR]
    :param scenario: Scenario object
    :param runhist: Runhist object
    :param stats: Stats object
    :param rng: Seed
    :param init_conf: Initial Configuration to start from
    :param init_runhist: Initial Runhistory. Used to jumpstart the optimizer with information of previous runs.
    :return: Optimizer object
    """
    optimizer = None
    tae = get_tae(tae_str, scenario, runhist, stats)
    if init_runhist:
        for inst_ in scenario.train_insts:
            for run_key in init_runhist.data.keys():
                config = init_runhist.ids_config[run_key[0]]
                instance_id = run_key[1]
                seed_ = run_key[2]
                cost = init_runhist.data[run_key][0]
                time_ = init_runhist.data[run_key][1]
                status = init_runhist.data[run_key][2]
                additional_info = init_runhist.data[run_key][3]
                if inst_ in run_key:
                    try:
                        runhist.add(config=config, cost=cost, time=time_, status=status, instance_id=instance_id,
                                    seed=seed_, additional_info=additional_info)
                    except TypeError:
                        logging.warning('!' * 120)
                        logging.warning('Could not add datapoint to runhistory')
                        logging.warning('!' * 120)
    if modus == "SMAC":
        optimizer = SMAC(
            scenario=scenario,
            rng=rng,
            runhistory=runhist,
            initial_configurations=[init_conf] if init_conf else None,
            tae_runner=tae)
    elif modus == "ROAR":
        optimizer = ROAR(
            scenario=scenario,
            rng=rng,
            runhistory=runhist,
            initial_configurations=[init_conf] if init_conf else None,
            tae_runner=tae)
    return optimizer


def load_runhistory_from_disc(fn: str, cs: ConfigurationSpace,
                              runhist: RunHistory) -> RunHistory:
    """Load runhistory in json representation from disk.
    Parameters
    ----------
    fn : str
        file name to load from
    cs : ConfigSpace
        instance of configuration space
    runhist : RunHistory
        instance of RunHistory to add values to
    """
    with open(fn) as fp:
        all_data = json.load(fp, object_hook=StatusType.enum_hook)

    ids_config = {int(id_): remove_inactive_params(cs, values)
                  for id_, values in all_data["configs"].items()}

    config_ids = {config: id_ for id_, config in ids_config.items()}

    _n_id = len(config_ids)

    # important to use add method to use all data structure correctly
    for k, v in all_data["data"]:
        runhist.add(config=ids_config[int(k[0])],
                    cost=float(v[0]),
                    time=float(v[1]),
                    status=StatusType(v[2]),
                    instance_id=k[1],
                    seed=int(k[2]),
                    additional_info=v[3])
    return runhist


def read_traj_file(cs: ConfigurationSpace, fn: str) -> (Configuration, float):
    """
    Simple method to read in a trajectory file in the json format / aclib2 format
    :param cs:
        Configspace object used to correctly build Configuration
    :param fn:
        file name
    :return:
        tuple of (incumbent [Configuration], incumbent_cost [float])
    """
    line = ''
    if not (os.path.exists(fn) and os.path.isfile(fn)):  # File existence check
        raise FileNotFoundError('File %s not found!' % fn)
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            pass
    line = line.strip()
    incumbent_dict = json.loads(line)
    inc_dict = {}
    for key_val in incumbent_dict['incumbent']:  # convert string to Configuration
        key, val = key_val.replace("'", '').split('=')
        if isinstance(cs.get_hyperparameter(key), CategoricalHyperparameter):
            inc_dict[key] = val
        elif isinstance(cs.get_hyperparameter(key), FloatHyperparameter):
            inc_dict[key] = float(val)
        elif isinstance(cs.get_hyperparameter(key), IntegerHyperparameter):
            inc_dict[key] = int(val)
    incumbent = Configuration(cs, inc_dict) # remove_inactive_params(cs, inc_dict)
    incumbent_cost = incumbent_dict['cost']
    incumbent._populate_values()
    return incumbent, incumbent_cost
