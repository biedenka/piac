import os
import sys
import csv
import glob
import copy
import pickle
import logging
import inspect
import argparse
from typing import Union, List
from multiprocessing import Process
import multiprocessing as mp

import numpy as np

from ConfigSpace import Configuration

from smac.scenario.scenario import Scenario
from smac.tae.execute_ta_run import StatusType, ExecuteTARun
from smac.tae.execute_ta_run_old import ExecuteTARunOld
from smac.configspace import Configuration
from smac.utils.io.input_reader import InputReader
from smac.runhistory.runhistory import RunHistory
from smac.optimizer.objective import average_cost

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from kraken.util.n_ary_tree import TreeNode
from kraken.util.utils import get_tae, read_traj_file, scenario_option_names
from kraken.util.stats import PIACStats
from kraken.piac.kraken import trickle_down_tree
from kraken.piac.hydra import setup_hydra_selector, get_hydra_conf, setup_hydra_selector_over_time
from kraken.piac.isac import get_isac_conf


TAE = None


def worker(in_q, out_q, func, args_=None):
    while not in_q.empty():
        item = in_q.get_nowait()
        item[0].logger = logging.getLogger("smac.tae")
        try:
            out_q.put(func(tae=TAE, config=item[0], inst=item[1], cutoff=item[2], seed=item[3]))
            # empirical_result = _run_conf_on_inst(tae=tae, config=config, inst=inst, scenario=scen,
            #                                      seed=inst_seed[num_run])
        except Exception as i:
            logging.error("\tProcessing failed with {}.".format(str(i)))


def traverse_tree(node: TreeNode, result: []):
    """
    Helper method to print some contents of the tree
    :param node: n_ary_tree node
    :return: None
    """
    if not node.split_criterion and node.instance_names is not None:
        logging.debug('Leaf')
        result.append([node.configuration, node.name])
    for child in node.children:
        logging.debug('Node')
        result = traverse_tree(child, result)
    return result


def _read_test_instances(fh: str) -> list:
    in_reader = InputReader()
    return in_reader.read_instance_file(fh)


def _read_inst_feats(fh: str) -> dict:
    in_reader = InputReader()
    return in_reader.read_instance_features_file(fh)[1]


def _run_conf_on_inst(tae: Union[ExecuteTARun, ExecuteTARunOld], config: Union[dict, Configuration],
                      inst: str, cutoff: int, seed: int = 12345) -> tuple:
    """
    Runs config on inst
    :param config: config (dict)
    :param inst: inst id (string)
    :return: (Status [1-5], cost, runtime, inst_specific) 1 = SUCCESS, 2 = TIMEOUT other Status IDS indicate crashed
    """
    empirical_result = tae.start(config, instance=inst,
                                 cutoff=cutoff,
                                 seed=seed,
                                 instance_specific="0")
    return empirical_result


def _empirically_validate(n_runs, inst_seed, config, inst, scen, sort_by_runtime):
    empirical_res = [np.empty(n_runs, dtype=object), np.empty(n_runs, dtype=object),
                     np.empty(n_runs, dtype=object), np.empty(n_runs, dtype=object)]
    processes = []
    manager = mp.Manager()
    inq = manager.Queue()
    outq = manager.Queue()
    for num_run in range(n_runs):
        logging.info('\t%s #round %d - seed %d' % ('-' * 5, num_run, inst_seed[num_run]))

        inq.put([config, inst, scen.cutoff, inst_seed[num_run]])
        processes.append(Process(target=worker, args=(inq, outq, _run_conf_on_inst)))
        # empirical_result = _run_conf_on_inst(tae=tae, config=config, inst=inst, scenario=scen,
        #                                      seed=inst_seed[num_run])
    for p in processes:
        p.start()
    for p in processes:
        p.join()
    for num_run in range(len(processes)):
        empirical_result = outq.get_nowait()
        # print(empirical_result)
        empirical_res[0][num_run] = empirical_result[0]
        empirical_res[1][num_run] = empirical_result[1]
        empirical_res[2][num_run] = empirical_result[2]
        empirical_res[3][num_run] = empirical_result[3]
        logging.info('\t%s cost: %3.5f' % ('-' * 10, empirical_result[1]))

    sort_by = 2 if sort_by_runtime else 1

    sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_res[sort_by]), key=lambda x: x[1])))
    median_idx = (n_runs - 1) / 2
    if median_idx == np.floor(median_idx):
        median_idx = int(median_idx)
        empirical_res[0] = empirical_res[0][sort_keys][median_idx]
        empirical_res[1] = empirical_res[1][sort_keys][median_idx]
        empirical_res[2] = empirical_res[2][sort_keys][median_idx]
        empirical_res[3] = empirical_res[3][sort_keys][median_idx]
    else:
        if sort_by_runtime:
            empirical_res[0] = empirical_res[0][sort_keys][int(np.floor(median_idx))]
        else:  # else we have a par factor which will cause the smaller value to have less weight
            empirical_res[0] = empirical_res[0][sort_keys][int(np.ceil(median_idx))]
        empirical_res[1] = np.mean([empirical_res[1][sort_keys][int(np.floor(median_idx))],
                                    empirical_res[1][sort_keys][int(np.ceil(median_idx))]])
        empirical_res[2] = np.mean([empirical_res[2][sort_keys][int(np.floor(median_idx))],
                                    empirical_res[2][sort_keys][int(np.ceil(median_idx))]])
        empirical_res[3] = empirical_res[3][sort_keys][int(np.floor(median_idx))]
    if empirical_res[1] >= scen.cutoff:
        empirical_res[1] = scen.par_factor * scen.cutoff
    return empirical_res


def validate_all_configs(inst: str, configs: Configuration, scen: Scenario, inst_seed: List[int],
                         n_runs: int=3, sort_by_runtime: bool=False):
    p_out = [inst]
    for config, name in configs:
        logging.info('\tEvaluating %s' % name)
        empirical_res = _empirically_validate(n_runs, inst_seed, config, inst, scen, sort_by_runtime)
        p_out.append(empirical_res[1])
        logging.info('\t%s median cost: %3.5f' % (' ' * 20, empirical_res[1]))
    return p_out


def validate_selected_configs(inst: str, config: Configuration, scen: Scenario, inst_seed: List[int],
                              n_runs: int=3, sort_by_runtime: bool=False, name:str='None'):
    p_out = [inst]
    logging.info('\tEvaluating %s' % name)
    empirical_res = _empirically_validate(n_runs, inst_seed, config, inst, scen, sort_by_runtime)
    p_out.append(empirical_res[1])
    p_out.append(name)
    logging.info('\t%s median cost: %3.5f' % (' ' * 20, empirical_res[1]))
    return p_out


def get_config(sys: str, inst, selector, scenario):
    if sys == 'KRAKEN':
        node = trickle_down_tree(selector, scenario.feature_dict, inst)
        config, cluster_name = node.configuration, node.name
    elif sys == 'HYDRA':
        config, cluster_name = get_hydra_conf(selector[0], selector[1], inst, scenario)
    else:
        config, cluster_name = get_isac_conf(selector, scenario, inst)
    return config, cluster_name


def validate_main(fn: str, seed: int=None, system: str='KRAKEN', mode: str='TEST', v_all: bool=False,
                  over_time: bool=False, n_runs: int=3, tae: str='aclib', verbosity: int=logging.DEBUG,
                  scen_fn: str=None, cutoff: float=None):
    """
    TODO but the names should imply most of it!
    """
    logging.basicConfig(level=verbosity)
    if seed is None:
        seed = int.from_bytes(os.urandom(4), byteorder="big")
        logging.info('Using seed: %d' % seed)
    rng = np.random.RandomState(seed)
    working_dir = os.path.abspath(os.path.dirname(fn))
    fn = os.path.abspath(fn)
    orig_dir = os.getcwd()
    if scen_fn:
        scen_fn = os.path.abspath(scen_fn)
    scen = None

    if not scen_fn:
        scen_fn = glob.glob(os.path.join(working_dir, 'scenario*'))[0]
    elif scen_fn.endswith('.pkl'):
        with open(scen_fn, 'rb') as hf:
            scen = pickle.load(hf)
        valid_insts = copy.deepcopy(scen.valid_insts)
        print(valid_insts)
    if system == 'SMAC' or (system == 'DEFAULT' and 'smac' in working_dir):
        out_f = open(os.path.join(working_dir, 'validation_scenario.txt'), 'w')
        with open(scen_fn) as in_fh:
            for line in in_fh:
                line = line.replace(working_dir.split(os.path.sep)[-1], '.')
                out_f.write(line)
                header, line = line.strip().split('=')
                header = header.strip()
                line = line.strip()
                logging.info(line)
                if scenario_option_names[header] in ['paramfile', 'feature_file',
                                                     'instance_file', 'test_instance_file']:
                    if os.path.isdir(line) and not os.path.islink(line):
                        fn_ = line.split(os.path.sep)[0].strip()
                    elif scenario_option_names[header] not in ['feature_file', 'paramfile']:
                        open_name = os.path.join(working_dir, line) if line.startswith('.') else line
                        with open(open_name, 'r') as tmp_fh:
                            l = tmp_fh.readline()
                        fn_ = l.split(',')[0].strip().split(os.path.sep)[0].strip()
                    else:
                        continue
                    try:
                        os.symlink(os.path.join(orig_dir, fn_), os.path.join(working_dir, fn_))
                        logging.info('Creating Symlink for %s' % fn_)
                    except FileExistsError:
                        logging.debug('Not creating symlink for %s' % fn_)
                elif scenario_option_names[header] in ['algo']:
                    if len(line.split(' ')) > 1:
                        fn_ = line.split(' ')
                        for f in fn_:
                            f = f.lower()
                            if 'target' in f or 'wrapper' in f:
                                fn_ = f
                                break
                    if os.path.sep in fn_:
                        tmp_fn = fn_.split(os.path.sep)
                        if tmp_fn[0] == '.':
                            del tmp_fn[0]
                        fn_ = tmp_fn[0]
                    try:
                        os.symlink(os.path.join(orig_dir, fn_), os.path.join(working_dir, fn_))
                    except FileExistsError:
                        logging.debug('Not creating symlink for %s' % fn_)
        out_f.close()
        scen_fn = os.path.join(working_dir, 'validation_scenario.txt')
    print(scen_fn)
    os.chdir(working_dir)

    if scen is None:
        scen = Scenario(scen_fn,  # cmd_args needed to suppres SMAC scenario output folder creation
                        cmd_args={'output_dir': ""})  # type: Scenario
        scen.__dict__['valid_insts'] = scen.train_insts

    if cutoff:
        scen.cutoff = cutoff

    test_inst_array = np.array(_read_test_instances(scen.test_inst_fn)).flatten()
    train_inst_array = np.array(_read_test_instances(scen.train_inst_fn)).flatten()
    if mode == 'TEST':
        inst_array = test_inst_array
    elif mode == 'TRAIN':
        inst_array = train_inst_array
    # else:
    #     inst_array = np.vstack((test_inst_array.reshape(-1, 1), train_inst_array.reshape(-1, 1))).flatten()
    inst_feat_dict = _read_inst_feats(scen.feature_fn)

    config = None
    if system == 'SMAC':
        if not 'traj' in fn and not 'aclib' in fn:
            fn = glob.glob(os.path.join(fn, 'traj*aclib*'))[-1]
        incumbent, cost = read_traj_file(scen.cs, fn)
        config = incumbent
    elif system == 'DEFAULT':
        config = scen.cs.get_default_configuration()
    elif system in ['KRAKEN', 'ISAC', 'HYDRA']:
        with open(fn, 'rb') as fh:
            selector = pickle.load(fh)
        if system == 'KRAKEN':
            if selector.preprocessor:
                k_feature_array = []
                k_inst_feat_dict = {}
                for inst_ in inst_array:
                    k_feature_array.append(inst_feat_dict[inst_])
                k_feature_array = np.array(k_feature_array)
                k_feature_array = selector.preprocessor(k_feature_array)
                for feat, inst_ in zip(k_feature_array, inst_array):
                    k_inst_feat_dict[inst_] = feat
                scen.feature_dict = k_inst_feat_dict
                scen.feature_array = k_feature_array
        elif system == 'HYDRA':
            selector = setup_hydra_selector(scen, fn)

    all_configs = []
    if v_all and system not in ['SMAC', 'DEFAULT']:
        with open(fn, 'rb') as fh:
            all_configs = pickle.load(fh)
        if system == 'KRAKEN':
            all_configs = traverse_tree(all_configs, [])
        elif system == 'HYDRA':
            all_configs = list(map(lambda x: [x[1], 'h_%02d' % x[0]], enumerate(all_configs)))
        else:
            all_configs = list(map(lambda x: [x.configuration, x.name], all_configs))

    if over_time and system not in ['SMAC', 'DEFAULT', 'ISAC']:
        if system == 'KRAKEN':
            selectors = []
            all_configs_over_time = []
            all_trees = glob.glob(os.path.join(working_dir, '*partition_tree*:*.pkl'))
            for f in all_trees:
                with open(f, 'rb') as fh:
                    tree = pickle.load(fh)
                selectors.append(tree)
                all_configs_over_time.append(traverse_tree(tree, []))
        elif system == 'HYDRA':
            selectors = setup_hydra_selector_over_time(scen, fn)
            all_configs_over_time = []
            for s in selectors:
                all_configs_over_time.append(s[0])

    all_configs = np.array(all_configs)
    sort_by_runtime = False
    multiplier = 1
    if v_all:
        multiplier = len(all_configs) + 1
    if over_time:
        multiplier = 999999
    tar_run_limit = len(inst_array) * n_runs * multiplier
    stats = PIACStats(ta_run_limit=tar_run_limit)
    stats.start_timing()
    runhist = RunHistory(aggregate_func=average_cost)
    global TAE
    TAE = get_tae(tae, scen, stats=stats, runhist=runhist)

    write_header = True
    write_mode = 'w'
    already_checked = []

    logging.info('Finished Setup')

    all_name = 'all_configurations_perf_%s.csv' % mode
    sys_name = '%s_selector_perf_%s.csv' % (system, mode)
    tim_name = '%s_perf_over_time_%s.csv' % (system, mode)

    skip = []
    if os.path.exists(sys_name):
        write_header = False
        write_mode = 'a'
        with open(sys_name, 'r') as in_f:
            for line in in_f.readlines():
                skip.append(line.split(',')[0].strip())
        logging.warning('Will skip %d instances as they have been previously validated!' % len(skip))

    sys_fh = open(sys_name, write_mode)
    sys_perf_writer = csv.writer(sys_fh, delimiter=',')
    if write_header:
        perf_header = [' ', 'cost', 'Used config']
        sys_perf_writer.writerow(perf_header)
    if over_time and system not in ['SMAC', 'DEFAULT', 'ISAC']:
        tim_fh = open(tim_name, write_mode)
        tim_perf_writer = csv.writer(tim_fh, delimiter=',')
        if write_header:
            perf_header = [' ']
            perf_header.extend(['VBS %d' % i for i in range(len(selectors))])
            perf_header.extend(['S %d' % i for i in range(len(selectors))])
            tim_perf_writer.writerow(perf_header)
    if v_all and system not in ['SMAC', 'DEFAULT']:
        all_fh = open(all_name, write_mode)
        all_perf_writer = csv.writer(all_fh, delimiter=',')
        if write_header:
            perf_header = [' ']
            perf_header.extend(all_configs[:, 1])
            all_perf_writer.writerow(perf_header)
    count = 0
    for inst in inst_array:
        if inst in skip:
            count += 1
            logging.info('Skipping %s. Already checked!' % inst)
            continue
        logging.info('%3d/%3d insts' % (count, len(inst_array)))
        inst_seed = rng.randint(0, 999999, n_runs)
        logging.info('%s' % inst)
        if inst in already_checked:
            logging.info('%s => Previously checked!' % ('#' * 20))
            count += 1
            continue
        if over_time and system not in ['SMAC', 'DEFAULT', 'ISAC']:
            vbs = []
            sel = []
            for c, s in zip(all_configs_over_time, selectors):
                c = np.array(c)
                p_out_round = np.array(validate_all_configs(inst, c, scen, inst_seed, n_runs, sort_by_runtime),
                                       dtype=object)
                config, config_name = get_config(system, inst, s, scen)
                m_at = np.argmin(p_out_round[1:]) + 1
                vbs.append(p_out_round[m_at])
                logging.debug(p_out_round)
                logging.debug(p_out_round[m_at])
                s = p_out_round[1:][c[:, 1] == config_name]
                logging.debug(s)
                sel.extend(s)
            p_out = [p_out_round[0]]
            p_out.extend(vbs)
            p_out.extend(sel)
            tim_perf_writer.writerow(p_out)
        if v_all and over_time and system not in ['SMAC', 'DEFAULT', 'ISAC']:
            p_out = p_out_round
            all_perf_writer.writerow(p_out)
        elif v_all and system not in ['SMAC', 'DEFAULT']:
            p_out = validate_all_configs(inst, all_configs, scen, inst_seed, n_runs, sort_by_runtime)
            all_perf_writer.writerow(p_out)
        if system in ['SMAC', 'DEFAULT']:
            p_out = validate_selected_configs(inst, config, scen, inst_seed, n_runs, sort_by_runtime,
                                              name=system)
            sys_perf_writer.writerow(p_out)
        elif system in ['KRAKEN', 'HYDRA', 'ISAC'] and not over_time:
            config, config_name = get_config(system, inst, selector, scen)
            p_out = validate_selected_configs(inst, config, scen, inst_seed, n_runs, sort_by_runtime,
                                              name=config_name)
            sys_perf_writer.writerow(p_out)
        elif system in ['KRAKEN', 'HYDRA'] and over_time:
            p_out = [p_out_round[0]]
            p_out.extend(s)
            p_out.append(config_name)
            sys_perf_writer.writerow(p_out)
        elif system in ['ISAC'] and over_time:
            config, config_name = get_config(system, inst, selector, scen)
            p_out = validate_selected_configs(inst, config, scen, inst_seed, n_runs, sort_by_runtime,
                                              name=config_name)
            sys_perf_writer.writerow(p_out)
        sys_fh.flush()
        if v_all and system not in ['SMAC', 'DEFAULT']:
            all_fh.flush()
        if over_time and system not in ['SMAC', 'DEFAULT', 'ISAC']:
            tim_fh.flush()
        count += 1
    runhist.save_json('all_configs_on_%s_runhist.json' % mode)


def cmd_line_call():
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()

    parser.add_argument('file', help='Path to the file to load')

    parser.add_argument('-m', '--mode', default='TEST', help='Validate on which set(s)',
                        choices=['TRAIN', 'TEST'])
    parser.add_argument('-n', '--n_runs', help='Num runs per instance', default=3, type=int)
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-s', '--seed', default=None, type=int)
    parser.add_argument('-sys', '--system', help='system to validate', default='KRAKEN', choices=[
                        'KRAKEN', 'ISAC', 'SMAC', 'HYDRA', 'DEFAULT'])
    parser.add_argument('-a', '--all', help='Evaluate all found configurations', action='store_true')
    parser.add_argument('-o', '--over_time', help='Validate system over time', action='store_true')
    parser.add_argument('-S', '--scen', help='scenario to load (usually no need to specify!)', default=None)
    parser.add_argument('-c', '--cutoff', help='Cutoff to use for validation (instead of scenario cutoff)',
                        default=None, type=float)
    args, unknown = parser.parse_known_args()
    logging.basicConfig(level=verbose_lvl_dict[args.verbosity])
    if args.seed is None:
        args.seed = int.from_bytes(os.urandom(4), byteorder="big")
        logging.info('Using seed: %d' % args.seed)
    validate_main(fn=args.file, seed=args.seed, system=args.system, mode=args.mode, v_all=args.all,
                  over_time=args.over_time, n_runs=args.n_runs,
                  tae=args.tae, verbosity=verbose_lvl_dict[args.verbosity], cutoff=args.cutoff, scen_fn=args.scen)


if __name__ == '__main__':
    cmd_line_call()
