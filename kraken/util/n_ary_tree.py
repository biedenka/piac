import logging
from typing import Union

import numpy as np


class TreeNode(object):
    """
    Simple Class allowing to construct a tree for CSHC
    """
    def __init__(self, children: Union[list, np.ndarray]=list(), split_criterion: callable=None,
                 instances: Union[list, np.ndarray]=None, loss: float=np.float('inf'),
                 instance_names: Union[list, np.ndarray]=None,
                 parent=None, depth=0, instance_features: dict=None, scenario=None, left=None,
                 is_root=False, name=None, configuration=None):
        self.children = children
        self.split_criterion = split_criterion
        self.instances = instances
        self.instance_names = instance_names
        self.instance_features = instance_features
        self.loss = loss
        self.configuration = configuration
        self.performance = None
        self.parent = parent
        self.logger = logging.getLogger('NodeDepth_%d' % depth)
        self.depth = depth
        self.scenario = scenario
        self.left = left
        self.is_root = is_root
        self.opt_stats_object = None
        self.name = name
        self.preprocessor = None
        self.centroid = None  # Needed for ISAC

    def lighten(self, **kwargs):
        if len(kwargs) > 0:
            for key in kwargs:
                self.__dict__[key] = kwargs[key]
        else:
            self.logger = None
            try:
                self.scenario.in_reader = None
            except AttributeError:
                self.scenario = None
        if self.children:
            for child in self.children:
                child.lighten()

    def print_tree(self, depth: int=0):
        """
        Method to simply plot the tree
        :param depth: The depth of the TreeNode
        :return:
        """
        spacing = ''.join([' '] * depth)
        if self.logger:
            if self.split_criterion:  # Branch
                self.logger.info('%sCrit: %s' % (spacing, str(self.split_criterion)))
                for child in self.children:
                    child.print_tree(depth=depth + 2)
            else:  # Leaf
                self.logger.info('%s#Insts: %d' % (spacing, len(self.instances)))
        else:
            if self.split_criterion:  # Branch
                print('%sCrit: %s' % (spacing, str(self.split_criterion)))
                for child in self.children:
                    child.print_tree(depth=depth + 2)
            else:  # Leaf
                print('%s#Insts: %d' % (spacing, len(self.instances)))

    def __getstate__(self):
        d = dict(self.__dict__)
        del d['logger']
        return d

    def __setstate__(self, d):
        self.__dict__.update(d)
        self.logger = logging.getLogger(self.__class__.__name__)
