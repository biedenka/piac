import time
import logging

import numpy as np

__author__ = "Marius Lindauer, Andre Biedenkapp"


class PIACStats(object):
    """
        all statistics collected during configuration run
    """

    def __init__(self, wallclock_limit: float=np.float('inf'),
                 ta_run_limit: float=np.float('inf'),
                 algo_runs_time_limit: float=np.float('inf')):
        self.wallclock_limit = wallclock_limit
        self.ta_run_limit = ta_run_limit
        self.algo_runs_timelimit = algo_runs_time_limit
        if not np.isfinite(wallclock_limit) and not np.isfinite(ta_run_limit) and not np.isfinite(algo_runs_time_limit):
            raise 'One Limit has to be specified!'

        self.ta_runs = 0
        self.wallclock_time_used = 0
        self.ta_time_used = 0

        self._start_time = None
        self._logger = logging.getLogger("PIACStats")

    def update(self, stats):
        self._logger.debug(self.ta_time_used)
        self._logger.debug(self.wallclock_time_used)
        self._logger.debug(self.ta_runs)
        self.ta_time_used += stats.ta_time_used
        # self.wallclock_time_used += stats.wallclock_time_used
        self.ta_runs += stats.ta_runs
        self._logger.debug(self.ta_time_used)
        self._logger.debug(self.wallclock_time_used)
        self._logger.debug(self.ta_runs)

    def set_ta_run_limit(self, ta_run_limit: float):
        """
            Setter for TargetAlgorithm run limit
        """
        self.ta_run_limit = ta_run_limit

    def set_wallclock_limit(self, limit: float):
        """
            Setter for wallclock time limit in seconds
        """
        self.wallclock_limit = limit

    def set_algo_runs_timelimit(self, limit: float):
        """
            Setter for TargetAlgorithm run time limit in seconds
        """
        self.algo_runs_timelimit = limit

    def restart_timing(self):
        """
            Reset start time (i.e. set start time to current time)
        """
        self._start_time = time.time()

    def restart_ta_runs(self):
        """
            Reset used number of TargetAlgorithm runs to 0
        """
        self.ta_runs = 0

    def restart_ta_time_used(self):
        """
            Reset used TargetAlgorithm run time (i.e. set start time to current time)
        """
        self.ta_time_used = 0

    def restart(self):
        """
            Reset everything this Object tracks
        """
        self.restart_ta_runs()
        self.restart_ta_time_used()
        self.restart_timing()

    def start_timing(self):
        """
            starts the timer (for the runtime configuration budget)
        """
        self._start_time = time.time()

    def get_used_wallclock_time(self):
        """
            returns used wallclock time
            Returns
            -------
            wallclock_time : int
                used wallclock time in sec
        """
        self.wallclock_time_used = time.time() - self._start_time
        return self.wallclock_time_used

    def get_remaing_time_budget(self):
        """
            subtracts the runtime configuration budget with the used wallclock time
        """
        return self.wallclock_limit - self.get_used_wallclock_time()

    def get_remaining_ta_runs(self):
        """
           subtract the target algorithm runs in the scenario with the used ta runs
        """
        return self.ta_run_limit - self.ta_runs

    def get_remaining_ta_budget(self):
        """
            subtracts the ta running budget with the used time
        """
        return self.algo_runs_timelimit - self.ta_time_used

    def is_budget_exhausted(self):
        """
            check whether the configuration budget for time budget, ta_budget and ta_runs is empty

            Returns
            -------
                true if one of the budgets is exhausted
        """
        return self.get_remaing_time_budget() < 0 or \
               self.get_remaining_ta_budget() < 0 or \
               self.get_remaining_ta_runs() <= 0

    def print_stats(self, debug_out: bool = False):
        """
            prints all statistics

            Arguments
            ---------
            debug_out: bool
                use logging.debug instead of logging.info if set to true
        """
        log_func = self._logger.info
        if debug_out:
            log_func = self._logger.debug

        log_func("##########################################################")
        log_func("Statistics:")
        log_func("#Target algorithm runs: %d / %s" % (self.ta_runs, str(self.ta_run_limit)))
        log_func(
            "Used wallclock time: %.2f / %.2f sec " % (time.time() - self._start_time, self.wallclock_limit))
        log_func(
            "Used target algorithm runtime: %.2f / %.2f sec" % (self.ta_time_used, self.algo_runs_timelimit))
        log_func("Budget exhausted: %s" % str(self.is_budget_exhausted()))
        log_func("##########################################################")
