import os
import glob
import json
import itertools

import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sb
import pandas as pd
from typing import Union, List


def _check_data(x_data: np.ndarray, y_data: np.ndarray, cutoff: int, value: Union[int, float]):
    """
    Simple helper method to not clutter plot_scatter_plot
    """
    x_data[x_data >= cutoff] = value
    y_data[y_data >= cutoff] = value
    return x_data, y_data


def plot_scatter_plot(x_data: Union[List, np.ndarray], y_data: Union[List, np.ndarray],
                      labels: List[str], title: str = "", cutoff: int = 5, par: int = 10, colors: int = None,
                      linefactors: Union[List, None] = None, lims: Union[List, None] = None, ax=None, dpi: int=500
                      ) -> [plt.axes, [float, float]]:
    """
    Plots x_data vs y_data as scatter plot.
    """
    if ax is None:
        fig = plt.figure(dpi=dpi)
        ax = plt.gca()
    if colors is not None:
        palet = sb.color_palette('dark', 30)
        colors = list(map(lambda x: palet[x], colors))
    sb.set_style('darkgrid')
    cut_replace = 10**(int(np.log10(cutoff)) + 1)
    x_data = list(map(lambda y: cut_replace if y >= cutoff else y, x_data))
    y_data = list(map(lambda y: cut_replace if y >= cutoff else y, y_data))
    x_data = np.array(x_data)
    y_data = np.array(y_data)
    df = {labels[0]: x_data, labels[1]: y_data, 'x_timeouts': x_data >= cut_replace,
          'y_timeouts': y_data >= cut_replace,
          'timeouts': list(map(lambda x, y: x >= cut_replace and y >= cut_replace, x_data, y_data))}
    data = pd.DataFrame(df)
    grid = sb.regplot(labels[0], labels[1],
                       data=data,
                       fit_reg=False, ax=ax, scatter_kws={"s": 5, 'color': 'k' if colors is None else colors})
    grid.set(xscale="log", yscale="log")

    # Colors
    ref_colors = itertools.cycle([
        "#377eb8",  # Blue
        "#4daf4a",  # Green
        "#984ea3",  # Purple
        "#ff7f00",  # Orange
        "#ffff33",  # Yellow
        "#a65628",  # Brown
        "#f781bf",  # Pink
    ])

    # set initial limits
    if not lims:
        auto_min_val = min([np.min(x_data), np.min(y_data)])
        auto_min_val = auto_min_val if auto_min_val != 0.0 else 10**-3
        auto_max_val = max(np.max(x_data), np.max(y_data))
        auto_max_val += auto_max_val
    else:
        auto_min_val, auto_max_val = lims

    # Plot angle bisector and reference_lines
    out_up = cut_replace + cut_replace
    out_lo = max(10 ** -5, auto_min_val)
    if linefactors:
        for f in linefactors:
            c = next(ref_colors)
            # Lower reference lines
            ax.plot([f * out_lo, out_up], [out_lo, (1.0 / f) * out_up], c=c,
                    linestyle='--', linewidth=.5 * 1.5, zorder=0, label='{:>4s}x'.format(str(f)))
            # Upper reference lines
            ax.plot([out_lo, (1.0 / f) * out_up], [f * out_lo, out_up], c=c,
                    linestyle='--', linewidth=.5 * 1.5, zorder=0)
    ax.plot(np.linspace(out_lo, auto_max_val, 3),
            np.linspace(out_lo, auto_max_val, 3), 'r', zorder=0)

    ax.set_xlabel(labels[0])
    y_label_pad = -15 if sum(df['y_timeouts']) > 0 else 0
    ax.set_ylabel(labels[1], labelpad=y_label_pad)
    ax.set_xlim((auto_min_val, auto_max_val))
    ax.set_ylim((auto_min_val, auto_max_val))

    if sum(df['x_timeouts']) > 0 or sum(df['y_timeouts']) > 0:
        ax.plot(np.linspace(out_lo, cutoff, 3), [cutoff] * 3, 'r', zorder=1,
                linestyle='--', linewidth=.5 * 1.5, alpha=0.75)
        ax.plot([cutoff] * 3, np.linspace(out_lo, cutoff, 3), 'r', zorder=1,
                linestyle='--', linewidth=.5 * 1.5, alpha=0.75, label='Cutoff')
        start = int(np.floor(np.log10(np.abs(out_lo))))

        tmp_max = max(int(np.floor(np.log10(np.max(y_data)))), int(np.floor(np.log10(np.max(x_data)))))
        y_labels = [r'$10^{%d}$' % (start + i) if start + i <= tmp_max else '{:^20s}\nTIMEOUTS'.format(
            str(sum(df['y_timeouts']))) for i in range(
            len(ax.get_yticklabels()))]
        ax.set_yticklabels(y_labels)
        x_labels = [r'$10^{%d}$' % (start + i) if start + i <= tmp_max else '{:^8s}\nTIMEOUTS'.format(
            str(sum(df['x_timeouts']))) for i in range(
            len(ax.get_xticklabels()))]
        ax.set_xticklabels(x_labels)
    ax.set_title(title)
    leg = ax.legend(handletextpad=0, loc='center', bbox_to_anchor=(.5, 1.1), fancybox=True, frameon=True,
                    ncol=min(5, len(linefactors) + 1 if linefactors else 4))
    leg.get_frame().set_alpha(0)
    return ax, (auto_min_val, auto_max_val)


def plot_dists_A(ax: plt.axes, x: list, y: list, cutoff: int, legend: list,
                 par: Union[None, int] = None, title: str = '', labels: Union[str, list] = '') -> plt.axes:
    """
    Fit and plot curves to x and y data for visual comparison
    """
    x = np.array(x)
    y = np.array(y)
    try:
        x, y = _check_data(x, y, cutoff, par * cutoff)
    except TypeError:
        x, y = _check_data(x, y, cutoff, 10 * cutoff)
    da = sb.distplot(x[x <= cutoff if not par else cutoff * par], ax=ax)
    db = sb.distplot(y[y <= cutoff], ax=ax)
    ax.set_xlim([0, cutoff if not par else cutoff * par])
    ax.set_ylim([0, 2.5])
    ax.legend(legend)
    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    ax.set_title(title)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    return ax


def plot_dists_B(ax: plt.axes, a: list, b: list, cutoff: int,
                 legend: list, title: str, labels: list, colors: int = None) -> plt.axes:
    """
    Plot a and b data as point-clouds for visual comparison
    """
    try:
        tada = len(set(colors))
    except TypeError:
        tada = -9999
    if colors is not None:
        palet = sb.color_palette('dark', 30)
        _bla = False
        colors = np.array(list(map(lambda x: palet[x], colors)))
    else:
        colors = np.array([sb.color_palette('hls', 20)[13] for _ in range(len(a))])
        _bla = True
    if tada == 1:
        _bla = True

    a = np.array(a)
    b = np.array(b)
    a, b = _check_data(a, b, cutoff, 10 * ((10 % cutoff) + 1))
    plot_rng = np.random.RandomState(349834)
    spread = 0.125
    ax.plot(np.linspace(-1, 2, 3), [cutoff] * 3, 'r', zorder=0,
            linestyle='--', linewidth=.5 * 1.5, alpha=0.75)
    ax.scatter(plot_rng.randn(len(a[a < cutoff])) * spread, a[a < cutoff], alpha=0.9, s=5, marker='o',
               label='SUCCESS', c=colors[a < cutoff])
    ax.scatter(1 + plot_rng.randn(len(b[b < cutoff])) * spread, b[b < cutoff], alpha=0.9, s=5, marker='o',
               c=colors[b < cutoff])

    ax.scatter(plot_rng.randn(len(a[a >= cutoff])) * spread, a[a >= cutoff], alpha=0.9, s=5, marker='8',
               label='TIMEOUT', c=sb.color_palette('hls', 20)[10] if _bla else colors[a >= cutoff])
    ax.scatter(1 + plot_rng.randn(len(b[b >= cutoff])) * spread, b[b >= cutoff], alpha=0.9, s=5, marker='8',
               c=sb.color_palette('hls', 20)[10] if _bla else colors[b >= cutoff])
    ax.set_xlim(-1, 2)

    ax.set_xticks([0, 1], ['a', 'b'])
    if _bla:
        ax.legend(handletextpad=-.5, loc='center right', bbox_to_anchor=(1.15, 1.015), fancybox=True, frameon=True)
    # ax.yaxis.tick_right()
    # ax.yaxis.set_label_position("left")
    ax.set_title(title)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])  # , labelpad=-15)
    x_labels = ['' for i in range(len(ax.get_xticklabels()))]
    x_labels[1] = legend[0]
    x_labels[-2] = legend[1]
    ax.set_xticklabels(x_labels)
    return ax


def read_validation_data(cutoff=5, mode='TEST', num=10):
    print('Loading %s data' % mode)
    kraken_list = sorted(glob.glob('kraken**', recursive=True))[-num:]
    hydra_list = sorted(glob.glob('hydra**', recursive=True))[-num:]
    isac_list = sorted(glob.glob('isac**', recursive=True))
    k_over_time = []
    h_over_time = []
    kraken_sh_list = []
    hydra_sh_list = []

    print('Searching kraken and hydra files')
    for k, h in zip(kraken_list, hydra_list):
        kraken_sh_list.extend(glob.glob(os.path.join(k, '*sh.e*')))
        hydra_sh_list.extend(glob.glob(os.path.join(h, '*sh.e*')))
        k_over_time.extend(glob.glob(os.path.join(k, '*over*%s*' % mode), recursive=True))
        h_over_time.extend(glob.glob(os.path.join(h, '*over*%s*' % mode), recursive=True))
    print(k_over_time)

    print('Loading')
    h_times_list = []
    k_times_list = []
    for idx, k, h in zip(range(len(k_over_time)), k_over_time, h_over_time):
        print('\tFile %d of %d' % (idx + 1, len(k_over_time)))
        k_t = os.path.join(os.path.dirname(k), '.meta_info', 'round_info.json')
        h_t = os.path.join(os.path.dirname(h), '.meta_info', 'round_info.json')
        try:
            with open(k_t, 'r') as mf:
                round_dict = json.load(mf)
            k_times_list.append(round_dict['round_info'])
            with open(h_t, 'r') as mf:
                round_dict = json.load(mf)
            h_times_list.append(round_dict['round_info'])
        except FileNotFoundError:
            print('\t\tNo metadata found! Checking logs instead! Will take much longer!')
            import ast
            ks = kraken_sh_list[idx]
            hs = hydra_sh_list[idx]
            with open(ks, 'r') as fh:
                k_times = []
                necessary = False
                for line in fh:
                    if 'Saving tree' in line and 'final' in line:
                        necessary = True
                        continue
                    if necessary:
                        k_times.append(ast.literal_eval(line.split(':')[-1].strip().replace(
                            '(', '[').replace(')', ']')))
            k_times_list.append(k_times)
            with open(hs, 'r') as fh:
                h_times = []
                maybe = False
                for line in fh:
                    if 'Size rh' in line:
                        maybe = True
                        continue
                    if maybe:
                        l = line.split(':')[-1].strip()
                        try:
                            l = float(l)
                        except ValueError:
                            maybe = False
                            continue
                        h_times.append(l)
            h_times_list.append(h_times)
        k_over_time[idx] = pd.DataFrame.from_csv(k, index_col=0)
        k_over_time[idx][k_over_time[idx] >= cutoff] = cutoff
        h_over_time[idx] = pd.DataFrame.from_csv(h, index_col=0)
        h_over_time[idx][h_over_time[idx] >= cutoff] = cutoff

    k_over_time = k_over_time[:min(len(k_over_time), len(h_over_time))]
    h_over_time = h_over_time[:min(len(k_over_time), len(h_over_time))]

    k_over_time_df = pd.concat(k_over_time)
    h_over_time_df = pd.concat(h_over_time)

    k_over_time_df = k_over_time_df.groupby(k_over_time_df.index).median()
    h_over_time_df = h_over_time_df.groupby(h_over_time_df.index).median()

    print('Searching isac files')
    try:
        for idx, i in enumerate(isac_list):
            isac_list[idx] = glob.glob(os.path.join(i, 'ISAC*%s*' % mode), recursive=True)[0]

        isac_df = []
        i_m_c = []
        i_m_t = []
        print('Loading')
        for i in isac_list:
            isac_df.append(pd.DataFrame.from_csv(i, index_col=0))
            isac_df[idx][isac_df[idx] >= cutoff] = cutoff
            i_m_c.append(isac_df[-1].mean()['cost'])
            try:
                i_m_t.append(isac_df[-1]['cost'].value_counts()[cutoff])
            except KeyError:
                i_m_t.append(0)
        i_sort_idx = [i[0] for i in sorted(enumerate(i_m_c), key=lambda x: x[1])]
        i_m_c = np.array(i_m_c)[i_sort_idx]
        i_m_t = np.array(i_m_t)[i_sort_idx]
    except IndexError:
        print('Failed to find any ISAC files')
        isac_df = None
        i_m_c = np.float('inf')
        i_m_t = np.float('inf')

    smac_list = sorted(glob.glob('smac3**', recursive=True))
    smac = []
    def_ = []
    print('Searching for SMAC and Default files')
    for s in smac_list:
        try:
            smac.append(glob.glob(os.path.join(s, 'SMAC*%s*' % mode))[0])
        except IndexError:
            pass
        def_.append(glob.glob(os.path.join(s, 'DEF*%s*' % mode))[0])
    if not smac:
        smac = def_

    smac_df = []
    def__df = []
    s_m_c = []
    s_m_t = []
    d_m_c = []
    d_m_t = []
    print('Loading')
    for s, d in zip(smac, def_):
        smac_df.append(pd.DataFrame.from_csv(s, index_col=0))
        smac_df[-1][smac_df[-1]['cost'] >= cutoff] = cutoff
        def__df.append(pd.DataFrame.from_csv(d, index_col=0))
        def__df[-1][def__df[-1]['cost'] >= cutoff] = cutoff
        s_m_c.append(smac_df[-1].mean()['cost'])
        try:
            s_m_t.append(smac_df[-1]['cost'].value_counts()[cutoff])
        except KeyError:
            s_m_t.append(0)
        d_m_c.append(def__df[-1].mean()['cost'])
        try:
            d_m_t.append(def__df[-1]['cost'].value_counts()[cutoff])
        except KeyError:
            d_m_t.append(0)
    s_sort_idx = [i[0] for i in sorted(enumerate(s_m_c), key=lambda x: x[1])]
    d_sort_idx = [i[0] for i in sorted(enumerate(d_m_c), key=lambda x: x[1])]
    s_m_c = np.array(s_m_c)[s_sort_idx]
    d_m_c = np.array(d_m_c)[d_sort_idx]
    d_m_t = np.array(d_m_t)[d_sort_idx]
    s_m_t = np.array(s_m_t)[s_sort_idx]
    print('*'*120)
    print('{:>{width}s}'.format('DEFAULT:', width=10))
    print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format(' ', np.median(d_m_c), np.median(d_m_t), width=10))
    print('\n{:>{width}s}'.format('SMAC:', width=10))
    print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format(' ', np.median(s_m_c), np.median(s_m_t), width=10))
    try:
        print('\n{:>{width}s}'.format('ISAC:', width=10))
        print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format(' ', np.median(i_m_c), np.median(i_m_t), width=10))
    except TypeError:
        pass
    print('\n{:>{width}s}'.format('HYDRA:', width=10))
    hydra_s = []
    hydra_vbs = []
    for i in range(len(h_over_time_df)):
        try:
            tmp_h = [h_over_time_df.mean()['VBS %d' % i]]
            try:
                tmp_h.append(h_over_time_df['VBS %d' % i].value_counts()[cutoff])
            except KeyError:
                tmp_h.append(0)
            hydra_vbs.append(tmp_h)
            print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format('|>VBS %d' % i, hydra_vbs[-1][0],
                                                                  hydra_vbs[-1][1], width=10))
        except KeyError:
            break
    for i in range(len(h_over_time_df)):
        try:
            tmp_s = [h_over_time_df.mean()['S %d' % i]]
            try:
                tmp_s.append(h_over_time_df['S %d' % i].value_counts()[cutoff])
            except KeyError:
                tmp_s.append(0)
            hydra_s.append(tmp_s)
            print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format('|>S %d' % i, hydra_s[-1][0],
                                                                  hydra_s[-1][1], width=10))
        except KeyError:
            break
    print('\n{:>{width}s}'.format('KRAKEN:', width=10))
    kraken_s = []
    kraken_vbs = []
    for i in range(len(k_over_time_df)):
        try:
            tmp_h = [k_over_time_df.mean()['VBS %d' % i]]
            try:
                tmp_h.append(k_over_time_df['VBS %d' % i].value_counts()[cutoff])
            except KeyError:
                tmp_h.append(0)
            kraken_vbs.append(tmp_h)
            print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format('|>VBS %d' % i, kraken_vbs[-1][0],
                                                                  kraken_vbs[-1][1], width=10))
        except KeyError:
            break
    for i in range(len(k_over_time_df)):
        try:
            tmp_s = [k_over_time_df.mean()['S %d' % i]]
            try:
                tmp_s.append(k_over_time_df['S %d' % i].value_counts()[cutoff])
            except KeyError:
                tmp_s.append(0)
            kraken_s.append(tmp_s)
            print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format('|>S %d' % i, kraken_s[-1][0],
                                                                  kraken_s[-1][1], width=10))
        except KeyError:
            break
    return (np.median(d_m_c), np.median(d_m_t)), (np.median(s_m_c), np.median(s_m_t)), \
           (np.median(i_m_c), np.median(i_m_t)), hydra_s, kraken_s, hydra_vbs, kraken_vbs, h_times_list, k_times_list,\
        h_over_time_df, k_over_time_df


def read_kraken_validation_data(name, cutoff, mode='TEST', num=10):
    print('Loading %s data' % mode)
    kraken_list = sorted(glob.glob(name, recursive=True))[-num:]
    print(kraken_list)
    assert len(kraken_list) >= 1, 'NO FILES FOUND!'
    print('Found %d files' % len(kraken_list))
    k_over_time = []
    kraken_sh_list = []

    print('Searching kraken files')
    for k in kraken_list:
        kraken_sh_list.extend(glob.glob(os.path.join(k, '*sh.e*')))
        k_over_time.extend(glob.glob(os.path.join(k, '*over*%s*' % mode), recursive=True))
    assert len(k_over_time) >= 1, 'NO VALIDATION FILES WITH THE PATTERN "*over*%s*" FOUND!' % mode

    print('Loading')
    k_times_list = []
    for idx, k in zip(range(len(k_over_time)), k_over_time):
        print('\tFile %d of %d' % (idx + 1, len(k_over_time)))
        k_t = os.path.join(os.path.dirname(k), '.meta_info', 'round_info.json')
        try:
            with open(k_t, 'r') as mf:
                round_dict = json.load(mf)
            k_times_list.append(round_dict['round_info'])
        except FileNotFoundError:
            print('\t\tNo metadata found! Checking logs instead! Will take much longer!')
            import ast
            ks = kraken_sh_list[idx]
            with open(ks, 'r') as fh:
                k_times = []
                necessary = False
                for line in fh:
                    if 'Saving tree' in line and 'final' in line:
                        necessary = True
                        continue
                    if necessary:
                        k_times.append(ast.literal_eval(line.split(':')[-1].strip().replace(
                            '(', '[').replace(')', ']')))
            k_times_list.append(k_times)
        k_over_time[idx] = pd.DataFrame.from_csv(k, index_col=0)
        k_over_time[idx][k_over_time[idx] >= cutoff] = cutoff

    k_over_time_df = pd.concat(k_over_time)

    k_over_time_df = k_over_time_df.groupby(k_over_time_df.index).median()
    print('\n{:>{width}s}'.format('KRAKEN:', width=10))
    kraken_s = []
    kraken_vbs = []
    for i in range(len(k_over_time_df)):
        try:
            tmp_h = [k_over_time_df.mean()['VBS %d' % i]]
            try:
                tmp_h.append(k_over_time_df['VBS %d' % i].value_counts()[cutoff])
            except KeyError:
                tmp_h.append(0)
            kraken_vbs.append(tmp_h)
            print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format('|>VBS %d' % i, kraken_vbs[-1][0],
                                                                  kraken_vbs[-1][1], width=10))
        except KeyError:
            break
    for i in range(len(k_over_time_df)):
        try:
            tmp_s = [k_over_time_df.mean()['S %d' % i]]
            try:
                tmp_s.append(k_over_time_df['S %d' % i].value_counts()[cutoff])
            except KeyError:
                tmp_s.append(0)
            kraken_s.append(tmp_s)
            print('{:>{width}s} {:> 3.2f}[sec], {:> 5.0f}'.format('|>S %d' % i, kraken_s[-1][0],
                                                                  kraken_s[-1][1], width=10))
        except KeyError:
            break
    return kraken_s, kraken_vbs, k_times_list, k_over_time_df
