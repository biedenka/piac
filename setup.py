from setuptools import setup

import kraken

with open('requirements.txt') as fh:
    requirements = fh.read()
requirements = requirements.split('\n')
requirements = [requirement.strip() for requirement in requirements]

with open("kraken/__version__.py") as fh:
    version = fh.readlines()[-1].split()[-1].strip("\"'")

setup(
    name='Kraken',
    version=version,
    packages=['kraken', 'kraken.epm', 'kraken.tae', 'kraken.util', 'kraken.scenario', 'kraken.configspace',
              'kraken.optimization', 'kraken.piac'],
    url='',
    license='BSD 3-clause',
    author=kraken.__author__,
    author_email='biedenka@cs.uni-freiburg.de',
    description='Per Instance Algorithm Configuration',
    entry_points={
        'console_scripts': ['kraken=kraken.piac.kraken:cmd_line_call',
                            'isac=kraken.piac.isac:cmd_line_call',
                            'hydra=kraken.piac.hydra:cmd_line_call',
                            'validate-piac=kraken.util.validate:cmd_line_call'],
    },
    install_requires=requirements,
    platforms=['Linux'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ],
    keywords="machine learning algorithm configuration hyperparameter "
             "optimization tuning",
)
