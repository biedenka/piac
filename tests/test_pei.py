import logging
import os
import unittest

from smac.smbo.objective import average_cost
from smac.runhistory.runhistory import RunHistory

from kraken.optimization.improvement_profiler import ImpProfiler
from kraken.scenario import Scenario
from kraken.tae import ExecuteTARunAClib
from kraken.util.stats import PIACStats

FILE_PATH = os.path.abspath(__file__)
DATA_PATH = os.path.abspath(os.path.join(FILE_PATH, '..', 'data'))
CALL_PATH = os.getcwd()


class TestPEI(unittest.TestCase):

    def setUp(self):
        os.chdir(DATA_PATH)
        self.scenario = Scenario(scenario=os.path.join(DATA_PATH, 'input_test_scenario.txt'),
                                 cmd_args={'output_dir': 'pei_test'})
        logging.basicConfig(level=logging.DEBUG)
        self.stats = PIACStats(wallclock_limit=999, algo_runs_time_limit=999, ta_run_limit=999)
        self.logger = logging.getLogger('TestPEI')
        tae = ExecuteTARunAClib(ta=self.scenario.ta,
                                run_obj=self.scenario.run_obj,
                                par_factor=self.scenario.par_factor,
                                stats=self.stats,
                                runhistory=RunHistory(average_cost))
        self.logger.debug(tae.stats._start_time)
        self.logger.debug(tae.stats.start_timing())
        self.logger.debug(tae.stats._start_time)
        self.logger.debug(tae.stats.is_budget_exhausted())

        self.logger.debug('#'*120)
        self.logger.debug('TestPEI')
        self.logger.debug('#'*120)
        self.pei = ImpProfiler(self.scenario, tae=tae, stats=self.stats)
        self.logger.debug('%s, %s' % (self.scenario.par_factor, self.scenario.cutoff))

    def test_run_algo(self):
        self.logger.debug(
            self.pei._run_conf_on_inst(config=self.scenario.cs.sample_configuration(), inst='i1'))

    def test_initial_random_sampling(self):
        self.logger.debug('Testing init_sampling')
        rc, ri, dc, di = self.pei.initial_random_sampling(random_samples=10, run_default=0)
        self.assertEqual(rc.shape, (10, ))
        self.assertEqual(ri.shape, rc.shape)

        self.assertEqual(di.shape, dc.shape)
        self.assertEqual(di.shape, (0, ))

        rc, ri, dc, di = self.pei.initial_random_sampling(random_samples=0, run_default=10)
        self.assertEqual(rc.shape, (0, ))
        self.assertEqual(ri.shape, rc.shape)

        self.assertEqual(di.shape, dc.shape)
        self.assertEqual(di.shape, (10, ))

        rc, ri, dc, di = self.pei.initial_random_sampling()
        self.assertEqual(rc.shape, (100, ))
        self.assertEqual(ri.shape, rc.shape)

        self.assertEqual(di.shape, dc.shape)
        self.assertEqual(di.shape, (10, ))

        with self.assertRaises(ValueError):
            self.pei.initial_random_sampling(random_samples=0, run_default=0)

    def test_explore(self):
        self.logger.debug(self.pei.explore())

    def tearDown(self):
        os.chdir(CALL_PATH)
        self.logger.debug('At %s' % os.getcwd())

if __name__ == '__main__':
    unittest.main()
