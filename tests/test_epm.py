import logging
import os
import unittest

import numpy as np

from kraken.epm import RandomForestWithInstances as EPM
from kraken.scenario import Scenario

FILE_PATH = os.path.abspath(__file__)
DATA_PATH = os.path.abspath(os.path.join(FILE_PATH, '..', 'data'))
CALL_PATH = os.getcwd()


class TestEPM(unittest.TestCase):

    def setUp(self):
        os.chdir(DATA_PATH)
        self.scenario = Scenario(scenario=os.path.join(DATA_PATH, 'input_test_scenario.txt'),
                                 cmd_args={'output_dir': 'epm_test'})
        logging.basicConfig(level=logging.DEBUG)
        self.types = np.array([0, 3, 0, 0], dtype=np.uint64)
        self.logger = logging.getLogger('TestEPM')
        self.logger.debug('#'*120)
        self.logger.debug('TestEPM')
        self.logger.debug('#'*120)

    def test_training(self):
        self.logger.debug('Using feature_dict:')
        self.logger.debug(self.scenario.feature_dict)
        self.logger.debug('Using types:')
        model = EPM(types=self.types)
        self.logger.debug(self.types)
        X = np.array([[0.37454012, 0, 0.12, 99],               # c1, i1
                      [0.37454012, 0, 0.12, 99],               # c1, i1
                      [0.37454012, 0, 0.9, 0],                 # c1, i2
                      [0.37454012, 0, 0.9, 0],                 # c1, i2
                      [0.37454012, 0, 0.9, 0],                 # c1, i2 ##
                      [0.18343479, 0, 0.9, 0],                 # c2, i2
                      [0.18343479, 0, 0.9, 0],                 # c2, i2
                      [0.18343479, 0, 0.12, 99],               # c2, i1 ##
                      [0.59685016, 1, 0.12, 99],               # c3, i1
                      [0.59685016, 1, 0.12, 99],               # c3, i1
                      [0.59685016, 1, 0.9, 0],                  # c3, i2 ##
                      [0.15599452, 2, 0.12, 99],                # c4, i1
                      [0.15599452, 2, 0.9, 0],                  # c4, i2
                      [0.15599452, 2, 0.9, 0]])    # c4, i2 ##
        y = np.array([9., 8.7, 1., 1.01, 0.9, 0.99, 9., 8., 0.5, 0.8, 0.2, 9., 9., 5.])
        self.logger.debug('Training Model with X %s & y %s data.' % (str(X), str(y)))
        model.train(X, y)
        self.logger.debug('Model Prediction:')
        self.logger.debug(model.predict(X))

    def tearDown(self):
        os.chdir(CALL_PATH)
        self.logger.debug('At %s' % os.getcwd())

if __name__ == '__main__':
    unittest.main()
