import sys

import numpy as np

RS = np.random.RandomState(42)

def main():
    # '../test_func.py', '--instance', 'i1', '--cutoff', '99999999999999',
    # '--seed', '534895718', '--config', '-x', '0.3745401188473625', '-y', '0'
    inst = sys.argv[2]
    cutoff = float(sys.argv[4])
    seed = sys.argv[6]
    x = float(sys.argv[9])
    y = float(sys.argv[11])
    tmp = 9
    if inst == 'i1':
        if x == 0:
            if y == 1:
                tmp = 5
            else:
                tmp = RS.rand() * 2
        elif x < 0.5:
            tmp = RS.rand() * 2
        else:
            tmp = RS.rand() * 10
    else:
        if x == 0:
            if y == 1:
                tmp = 5
            else:
                tmp = RS.rand() * 20
        elif x < 0.5:
            tmp = RS.rand() * 20
        else:
            tmp = RS.rand() * 1.5

    if tmp == 0:
        tmp = 0.1
    if tmp < cutoff:
        print('Result of this algorithm run: {"status":"SUCCESS", "runtime":%f, "cost":0}' % tmp)
    else:
        print('Result of this algorithm run: {"status":"TIMEOUT", "runtime":%f, "cost":0}' % tmp)


def old():
    # instance, instance_specific, str(cutoff), "0", str(seed)
    inst = sys.argv[1]
    cutoff = float(sys.argv[3])
    seed = sys.argv[5]
    tmp = 9
    if 'x' in sys.argv[6]:
        x = float(sys.argv[7])
        y = float(sys.argv[9])
    else:
        y = float(sys.argv[7])
        x = float(sys.argv[9])
    if inst == 'i1':
        if x == 0:
            if y == 1:
                tmp = 5
            else:
                tmp = RS.rand() * 2
        elif x < 0.5:
            tmp = RS.rand() * 2
        else:
            tmp = RS.rand() * 10
    else:
        if x == 0:
            if y == 1:
                tmp = 5
            else:
                tmp = RS.rand() * 20
        elif x < 0.5:
            tmp = RS.rand() * 20
        else:
            tmp = RS.rand() * 1.5

    if tmp == 0:
        tmp = 0.1
    if tmp < cutoff:
        print('Result for ParamILS: SUCCESS, %f, 99999999, 9999999, %s' % (tmp, seed))
    else:
        print('Result for ParamILS: TIMEOUT, %f, 99999999, 9999999, %s' % (tmp, seed))

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        old()
