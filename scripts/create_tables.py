import argparse
import logging
import json
import glob
import os
import sys
import inspect

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from piac.util.utils import separate_data, MAX_PRINT_LEN, PRECISION, DIGIT_WIDTH, FLOAT_FMT_STR, INT_FMT_STR
from piac.util.utils import KEY_ORDER, HLINE, MAX_STR_LEN


def _output_lines(korder: list, piac_data: dict, smac_data: dict,
                  def_data: dict, out_str: str, fmt_dict: dict,
                  part: str='piac', mean: bool=False):
    """
    Simple method to print PIAC statistics to stdout.
    :param korder: list, key order in which to output Statistics
    :param piac_data: Dictionary of piac run data
    :param smac_data: Dictionary of smac run data
    :param part: str Name of partition to load data
    :param mean: bool Flag to determine if the mean data is used or not
    :return: 
    """
    count = 0
    if mean:
        piac_data = {'piac': piac_data}
        smac_data = {'smac_piac': smac_data}
        def_data = {'def_piac': def_data}
    tmp = []
    for key in korder:
        if key in ['timeouts', 'successes', 'crashes', 'n_points']:
            data_str = INT_FMT_STR.format(piac_data[part][key], width=DIGIT_WIDTH)
            smac_data_str = INT_FMT_STR.format(smac_data['smac_' + part][key], width=DIGIT_WIDTH)
            def_data_str = INT_FMT_STR.format(def_data['def_' + part][key], width=DIGIT_WIDTH)
        else:
            count += 1
            data_str = FLOAT_FMT_STR.format(piac_data[part][key], width=DIGIT_WIDTH, prec=PRECISION)
            smac_data_str = FLOAT_FMT_STR.format(smac_data['smac_' + part][key],
                                                 width=DIGIT_WIDTH, prec=PRECISION)
            def_data_str = FLOAT_FMT_STR.format(def_data['def_' + part][key],
                                                 width=DIGIT_WIDTH, prec=PRECISION)
        # if count == 1:
        #     print(HLINE.format(border='*', fill='-' * MAX_PRINT_LEN))
        key_str = '{:<{str_width}s}'.format(key, str_width=MAX_STR_LEN)

        left = '{:<{str_width}s}{separator}{:<{str_width}s}'.format(key_str, data_str, str_width=MAX_STR_LEN,
                                                                    separator=fmt_dict['sep'])
        middle = ' {:>{str_width}s}'.format(smac_data_str, str_width=MAX_STR_LEN)
        right = ' {:<{str_width}s}{separator}{:>{str_width}s}'.format(def_data_str, key_str, str_width=MAX_STR_LEN,
                                                                      separator=fmt_dict['sep'])
        tmp.append('{:>{half}s}{separator}{:<{third}s}{separator}{:<{half}s}'.format(
            left, middle, right, separator=fmt_dict['sep'], third=MAX_PRINT_LEN/6,
            half=int((MAX_PRINT_LEN - 3) / 3.)) + fmt_dict['new_line'])
    return ''.join(tmp)


def setup_output(margs):
    global MAX_PRINT_LEN
    MAX_PRINT_LEN = 91
    format_dict = {'CSV': ', ', 'LATEX': ' & ', 'terminal': ' | '}
    fmt_dict = {'hline': HLINE.format(border='', fill='-' * MAX_PRINT_LEN),
                'sep': format_dict['terminal'],
                'new_line': '\n'}
    if margs.format_ == 'CSV':
        fmt_dict['hline'] = ''
        fmt_dict['sep'] = format_dict['CSV']
    elif margs.format_ == 'LATEX':
        fmt_dict['hline'] = '\hline'
        fmt_dict['sep'] = format_dict['LATEX']
        fmt_dict['new_line'] = '\\\\\n'
    return fmt_dict


def main(margs):
    print('Searching data')
    json_files = glob.glob(os.path.join(margs.dir, '**-{}/**joint*.json'.format(margs.mode)), recursive=True)
    print('Found the following files:\n' + '\n'.join(
        ['\t{}' for _ in range(len(json_files))]).format(*json_files))

    print('Sorting data by date')
    date_strs = []
    for fn in json_files:
        split = fn.split(os.path.sep)
        if split[-2] not in date_strs:
            date_strs.append(split[-2])
        print(split)

    # First print the overall and partition statistics to stdout
    fmt_dict = setup_output(margs)
    out_str = []
    for date in date_strs:
        print('Loading Experiment data of {date}'.format(date=date))
        for fn in json_files:
            with open(fn, 'r') as fh:
                if 'mean' in fn:
                    mean_piac_dict, mean_piac_data, mean_smac_dict,\
                    mean_smac_data, mean_def_dict, mean_def_data = separate_data(json.load(fh))
                else:
                    piac_dict, piac_data, smac_dict, smac_data, def_dict, def_data = separate_data(json.load(fh),
                                                                                                   mean=False)

        # out_str += '{:^{width}s}'.format('MEAN RESULTS', width=MAX_PRINT_LEN) + fmt_dict['new_line']
        out_str.append('{:^{width}s}'.format('Experiment data of {date}'.format(date=date),
                                         width=MAX_PRINT_LEN) + fmt_dict['new_line'])
        out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])
        out_str.append('{:^{width}s}'.format('Overall Results', width=MAX_PRINT_LEN) + fmt_dict['new_line'])
        out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])
        out_str.append('{:^{width}s}'.format(
            '{:<{filla}s}{:>{half}s}{separator}{:^{hmiddle}s}{separator}{:<{half}s}{:>{fill}s}'.format('',
                'PIAC', 'SMAC', 'DEF', '',half=int(MAX_PRINT_LEN / 4.5), separator=fmt_dict['sep'], fill=25,
                                                                                                    filla=10,
                                                                                                       hmiddle=15),
            width=MAX_PRINT_LEN) + fmt_dict['new_line'])
        out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])
        out_str.append(_output_lines(KEY_ORDER, mean_piac_dict,
                                     mean_smac_dict, mean_def_dict, out_str, fmt_dict, mean=True))
        out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])
        for part in mean_piac_data:
            out_str.append('{:^{width}s}'.format('Results of partition', width=MAX_PRINT_LEN) + fmt_dict['new_line'])
            out_str.append('{:^{width}s}'.format('{}'.format(part), width=MAX_PRINT_LEN) + fmt_dict['new_line'])
            out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])
            out_str.append('{:^{width}s}'.format(
                '{:<{filla}s}{:>{half}s}{separator}{:^{hmiddle}s}{separator}{:<{half}s}{:>{fill}s}'.format('',
                    'PIAC', 'SMAC', 'DEF', '',half=int(MAX_PRINT_LEN / 4.5), separator=fmt_dict['sep'], fill=25,
                                                                                                        filla=10,
                                                                                                           hmiddle=15),
                width=MAX_PRINT_LEN) + fmt_dict['new_line'])
            out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])
            out_str.append(_output_lines(KEY_ORDER, mean_piac_data,
                                     mean_smac_data, mean_def_data, out_str, fmt_dict, part=part, mean=False))
            out_str.append(fmt_dict['hline'] + fmt_dict['new_line'])

    print(''.join(out_str))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', help='Directory to search .json files in.', required=True)
    parser.add_argument('-c', '--cutoff', help='cutoff_value', type=int, required=True)
    parser.add_argument('-p', '--par', help='PAR value', type=int, default=10, required=True)
    parser.add_argument('-m', '--mode', default='TEST', help='Valdiate on which set(s)',
                        choices=['TEST', 'TRAIN', 'TRAIN+TEST'], required=True)
    parser.add_argument('-s', '--save_path', help='Name to save the file', required=True)
    parser.add_argument('-f', '--format', dest='format_', help='File Format', default='out',
                        choices=['CSV', 'LATEX', 'terminal'])

    args, unknown = parser.parse_known_args()
    main(args)
