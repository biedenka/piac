import os
import sys
import csv
import pickle
import logging
import inspect
import argparse
from typing import Union
from multiprocessing import Process
import multiprocessing as mp

import numpy as np

from ConfigSpace import Configuration

from smac.scenario.scenario import Scenario
from smac.tae.execute_ta_run import StatusType, ExecuteTARun
from smac.tae.execute_ta_run_old import ExecuteTARunOld
from smac.configspace import Configuration
from smac.utils.io.input_reader import InputReader
from smac.runhistory.runhistory import RunHistory
from smac.optimizer.objective import average_cost

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from piac.util.n_ary_tree import TreeNode
from piac.util.utils import get_tae, read_traj_file
from piac.util.stats import PIACStats


def worker(in_q, out_q, func, args_=None):
    while not in_q.empty():
        item = in_q.get_nowait()
        item[0].logger = logging.getLogger("smac.tae")
        try:
            out_q.put(func(tae=TAE, config=item[0], inst=item[1], cutoff=item[2], seed=item[3]))
            # empirical_result = _run_conf_on_inst(tae=tae, config=config, inst=inst, scenario=scen,
            #                                      seed=inst_seed[num_run])
        except Exception as i:
            logging.error("\tProcessing failed with {}.".format(str(i)))


def traverse_tree(node: TreeNode, result: []):
    """
    Helper method to print some contents of the tree
    :param node: n_ary_tree node
    :return: None
    """
    if not node.split_criterion and node.instance_names is not None:
        result.append([node.configuration, node.name])
    for child in node.children:
        result = traverse_tree(child, result)
    return result


def _read_test_instances(fh: str) -> list:
    in_reader = InputReader()
    return in_reader.read_instance_file(fh)


def _read_inst_feats(fh: str) -> dict:
    in_reader = InputReader()
    return in_reader.read_instance_features_file(fh)[1]


def _run_conf_on_inst(tae: Union[ExecuteTARun, ExecuteTARunOld], config: Union[dict, Configuration],
                      inst: str, cutoff: int, seed: int = 12345) -> tuple:
    """
    Runs config on inst
    :param config: config (dict)
    :param inst: inst id (string)
    :return: (Status [1-5], cost, runtime, inst_specific) 1 = SUCCESS, 2 = TIMEOUT other Status IDS indicate crashed
    """
    empirical_result = tae.start(config, instance=inst,
                                 cutoff=cutoff,
                                 seed=seed,
                                 instance_specific="0")
    return empirical_result

TAE = None

def main():
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='Path to the file to load')
    parser.add_argument('scenario', help='Path to the scenario File')
    parser.add_argument('-m', '--mode', default='TEST', help='Validate on which set(s)',
                        choices=['TRAIN', 'TEST'])
    parser.add_argument('-n', '--n_runs', help='Num runs per instance', default=3, type=int)
    parser.add_argument('-M', '--method', help='Method to validate', default='ISMAC', choices=['ISMAC', 'ISAC',
                                                                                               'SMAC', 'HYDRA',
                                                                                               'DEFAULT'])
    parser.add_argument('-N', '--name', help='save_file_name', default='all_%s_configs_validated')
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-s', '--seed', default=12345, type=int)
    args_, unknown = parser.parse_known_args()
    rng = np.random.RandomState(args_.seed)

    logging.basicConfig(level=verbose_lvl_dict[args_.verbosity])
    scen = Scenario(args_.scenario,  # cmd_args needed to suppres SMAC scenario output folder creation
                    cmd_args={'output_dir': ""})  # type: Scenario

    test_inst_array = np.array(_read_test_instances(scen.test_inst_fn)).flatten()
    train_inst_array = np.array(_read_test_instances(scen.train_inst_fn)).flatten()
    if args_.mode == 'TEST':
        inst_array = test_inst_array
    elif args_.mode == 'TRAIN':
        inst_array = train_inst_array
    # else:
    #     inst_array = np.vstack((test_inst_array.reshape(-1, 1), train_inst_array.reshape(-1, 1))).flatten()
    inst_feat_dict = _read_inst_feats(scen.feature_fn)

    configs = []
    if args_.method == 'SMAC':
        incumbent, cost = read_traj_file(scen.cs, args_.file)
        configs.append([incumbent, 'smac_incumbent'])
    elif args_.method == 'DEFAULT':
        configs.append([scen.cs.get_default_configuration(), 'default'])
    elif args_.method in ['ISMAC', 'ISAC', 'HYDRA']:
        with open(args_.file, 'rb') as fh:
            configs = pickle.load(fh)
        if args_.method == 'ISMAC':
            configs = traverse_tree(configs, [])
        elif args_.method == 'HYDRA':
            configs = list(map(lambda x: [x[1], 'h_%02d' % x[0]], enumerate(configs)))
        else:
            configs = list(map(lambda x: [x.configuration, x.name], configs))
    sort_by_runtime = False
    tar_run_limit = len(inst_array) * args_.n_runs * len(configs)
    stats = PIACStats(ta_run_limit=tar_run_limit)
    stats.start_timing()
    runhist = RunHistory(aggregate_func=average_cost)
    global TAE
    TAE = get_tae(args_.tae, scen, stats=stats, runhist=runhist)

    base_name = os.path.basename(os.path.dirname(args_.file))

    configs = np.array(configs)
    feat_header = [' ']
    feat_header.extend(list(map(lambda x: 'f_%d' % x, range(len(scen.feature_array[0])))))
    perf_header = [' ']
    perf_header.extend(configs[:, 1])

    write_header = True
    mode = 'w'
    already_checked = []

    if os.path.exists('_'.join([args_.name, base_name, 'perf_%s.csv' % args_.mode])):
        logging.info('Found existing files!')
        mode = 'a'
        write_header = False
        with open('_'.join([args_.name, base_name, 'perf_%s.csv' % args_.mode]), 'r') as fh:
            reader = csv.reader(fh, delimiter=',')
            start = True
            for row in reader:
                if start:
                    start = False
                else:
                    already_checked.append(row[0])

    with open('_'.join([args_.name, base_name, 'perf_%s.csv' % args_.mode]), mode) as perf_file, \
            open('_'.join([args_.name, base_name, 'feats_%s.csv' % args_.mode]), mode) as feats_file:
        perf_writer = csv.writer(perf_file, delimiter=',')
        feat_writer = csv.writer(feats_file, delimiter=',')
        if write_header:
            perf_writer.writerow(perf_header)
            feat_writer.writerow(feat_header)
        count = 0
        for inst in inst_array:
            logging.info('%3d/%3d insts' % (count, len(inst_array)))
            f_out = [inst]
            p_out = [inst]
            inst_seed = rng.randint(0, 999999, args_.n_runs)
            logging.info('%s' % inst)
            if inst in already_checked:
                logging.info('%s => Previously checked!' % ('#' * 20))
                count += 1
                continue
            f_out.extend(scen.feature_dict[inst])
            manager = mp.Manager()
            for config, name in configs:
                logging.info('\tEvaluating %s' % name)
                empirical_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                                 np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
                processes = []
                inq = manager.Queue()
                outq = manager.Queue()
                for num_run in range(args_.n_runs):
                    logging.info('\t%s #round %d - seed %d' % ('-' * 5, num_run, inst_seed[num_run]))

                    inq.put([config, inst, scen.cutoff, inst_seed[num_run]])
                    processes.append(Process(target=worker, args=(inq, outq, _run_conf_on_inst)))
                    # empirical_result = _run_conf_on_inst(tae=tae, config=config, inst=inst, scenario=scen,
                    #                                      seed=inst_seed[num_run])
                for p in processes:
                    p.start()
                for p in processes:
                    p.join()
                for num_run in range(len(processes)):
                    empirical_result = outq.get_nowait()
                    # print(empirical_result)
                    empirical_res[0][num_run] = empirical_result[0]
                    empirical_res[1][num_run] = empirical_result[1]
                    empirical_res[2][num_run] = empirical_result[2]
                    empirical_res[3][num_run] = empirical_result[3]
                    logging.info('\t%s cost: %3.5f' % ('-' * 10, empirical_result[1]))

                sort_by = 2 if sort_by_runtime else 1

                sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_res[sort_by]), key=lambda x: x[1])))
                median_idx = (args_.n_runs - 1) / 2
                if median_idx == np.floor(median_idx):
                    median_idx = int(median_idx)
                    empirical_res[0] = empirical_res[0][sort_keys][median_idx]
                    empirical_res[1] = empirical_res[1][sort_keys][median_idx]
                    empirical_res[2] = empirical_res[2][sort_keys][median_idx]
                    empirical_res[3] = empirical_res[3][sort_keys][median_idx]
                else:
                    if sort_by_runtime:
                        empirical_res[0] = empirical_res[0][sort_keys][int(np.floor(median_idx))]
                    else:  # else we have a par factor which will cause the smaller value to have less weight
                        empirical_res[0] = empirical_res[0][sort_keys][int(np.ceil(median_idx))]
                    empirical_res[1] = np.mean([empirical_res[1][sort_keys][int(np.floor(median_idx))],
                                                empirical_res[1][sort_keys][int(np.ceil(median_idx))]])
                    empirical_res[2] = np.mean([empirical_res[2][sort_keys][int(np.floor(median_idx))],
                                                empirical_res[2][sort_keys][int(np.ceil(median_idx))]])
                    empirical_res[3] = empirical_res[3][sort_keys][int(np.floor(median_idx))]
                if empirical_res[1] >= scen.cutoff:
                    empirical_res[1] = scen.par_factor * scen.cutoff
                p_out.append(empirical_res[1])
                logging.info('\t%s median cost: %3.5f' % (' ' * 20, empirical_res[1]))
            feat_writer.writerow(f_out)
            perf_writer.writerow(p_out)
            perf_file.flush()
            feats_file.flush()
            count += 1
    runhist.save_json('_'.join([args_.name, base_name, 'all_configs_on_%s_runhist.json' % args_.mode]))

if __name__ == '__main__':
    main()