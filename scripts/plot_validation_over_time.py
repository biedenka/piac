import argparse
import logging
import json
import glob
import os
import sys
import inspect
import itertools
import pprint
from typing import Union

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

import matplotlib as mpl
import numpy as np

# mpl.use('Agg')
from matplotlib import pyplot as plt
import seaborn as sns

from scripts.evaluate import separate_data, HLINE, _output_lines, MAX_PRINT_LEN, KEY_ORDER


class MyArgparseNamespace:
    """
    Just a class that is used for type hinting such that pycharm knows what to expect in the main method
    """

    def __init__(self):
        self.par = 10  # type: int
        self.dir = '.'  # type: str
        self.cutoff = 5  # type: int
        self.plot = True  # type: bool
        self.cloud = True  # type: bool
        self.verbosity = 0  # type: int
        self.mode = 'TEST'  # type: str
        self.noPAR = False  # type: bool
        self.default = False  # type: bool
        self.isac = False  # type: bool
        self.hydra = False  # type: bool
        self.color = False  # type: bool
        self.save_logs_to = ''  # type: str
        self.save_plots_to = ''  # type: str
        self.brute = False  # type: bool


def main(margs: MyArgparseNamespace):
    """
    Main method that takes care of printing PIAC statistics and plotting results
    :param margs: argparse object
    :param logger: the logging object used
    :return: None
    """
    logger = logging.getLogger()

    logger.info('Searching data')
    if margs.brute:
        json_files = glob.glob(os.path.join(margs.dir, 'brute**-{}/***joint*.json'.format(margs.mode)),
                               recursive=True)
    else:
        json_files = glob.glob(os.path.join(margs.dir, 'piac**-{}/**joint*.json'.format(margs.mode)),
                               recursive=True)
    logger.info('Found the following files:\n' + '\n'.join(
        ['\t{}' for _ in range(len(json_files))]).format(*json_files))

    logger.info('Sorting data by date')
    date_strs = []
    for fn in json_files:
        split = fn.split(os.path.sep)
        if split[-2] not in date_strs:
            date_strs.append(split[-2])
        logger.debug(split)

    mean_def_data, mean_def_dict, mean_smac_data, mean_smac_dict, mean_isac_dict, mean_isac_data, \
    mean_hydra_dict, mean_hydra_data = {}, {}, {}, {}, {}, {}, {}, {}
    mean_piac_data, mean_piac_dict, smac_data, smac_dict, isac_dict, isac_data, hydra_dict, \
    hydra_data = {}, {}, {}, {}, {}, {}, {}, {}
    def_data, def_dict, piac_data, piac_dict = {}, {}, {}, {}

    # First print the overall and partition statistics to stdout
    prefixes, sets, belong_together = [], [], []
    date_strs = sorted(date_strs)
    for d in date_strs:
        if not sets:
            sets.append(d)
        else:
            tmp = sets + [d]
            if not prefixes:
                prefixes.append(os.path.commonprefix(tmp))
                sets = tmp
            elif len(sets) == 1:
                prefix = os.path.commonprefix(tmp)
                if prefix not in prefixes:
                    prefixes.append(prefix)
                sets = tmp
            else:
                prefix = os.path.commonprefix(tmp)
                if prefix != prefixes[-1]:
                    belong_together.append(sets)
                    sets = [d]
                else:
                    sets = tmp
    belong_together.append(sets)
    count = 0
    for b in belong_together:
        for a in b:
            print(a)
            count += 1
        print('#' * 60)
    print(count, len(date_strs))

    all_piac_vals = []
    all_smac_vals = []
    all_deff_vals = []
    all_hydr_vals = []
    all_isac_vals = []
    num_piac_parts = []
    overall = []
    for dates in belong_together:
        piac_values = []
        num_piac_parts.append([])
        for d_idx, date in enumerate(dates):
            logger = logging.getLogger(date[:20] + date[-20:])
            # logger.info('Loading Experiment data of {date}'.format(date=date))
            for fn in json_files:
                if date in fn:
                    with open(fn, 'r') as fh:
                        if 'mean' in fn:
                            mean_piac_dict, mean_piac_data, mean_smac_dict, \
                            mean_smac_data, mean_def_dict, mean_def_data, mean_isac_dict, mean_isac_data, \
                            mean_hydra_dict, mean_hydra_data = separate_data(
                                json.load(fh), cutoff=args.cutoff, ignore_unsolvable=False)
                        else:
                            piac_dict, piac_data, smac_dict, smac_data, def_dict, def_data, \
                            isac_dict, isac_data, hydra_dict, hydra_data = separate_data(
                                json.load(fh), mean=False, cutoff=args.cutoff, ignore_unsolvable=args.noUnsolvable)
                else:
                    continue

            # hydr_hand = {'Indu'}
            # for key in mean_hydra_dict:
            #     print(key)
            # for part in mean_hydra_data:
            #     for key in mean_hydra_data[part]:
            # if 'Conf_10' in mean_hydra_data:
            #     del mean_hydra_data['Conf_10']
            # print(bla)
            if args.noUnsolvable:  # TODO write a function instead of copy pasta code
                mean_hydra_dict['mean_costs'] = np.mean(hydra_dict['costs'])
                mean_hydra_dict['mean_par_runtime'] = np.mean(hydra_dict['par_runtimes'])
                mean_hydra_dict['mean_runtime'] = np.mean(hydra_dict['runtimes'])
                mean_hydra_dict['timeouts'] = hydra_dict['timeouts']
                mean_hydra_dict['n_points'] = hydra_dict['cropped_n_points']
                for partition in hydra_data:
                    mean_hydra_data[partition]['mean_costs'] = np.mean(hydra_data[partition]['costs'])
                    mean_hydra_data[partition]['mean_par_runtime'] = np.mean(hydra_data[partition]['par_runtimes'])
                    mean_hydra_data[partition]['mean_runtime'] = np.mean(hydra_data[partition]['runtimes'])
                    mean_hydra_data[partition]['timeouts'] = hydra_data[partition]['timeouts']
                    mean_hydra_data[partition]['n_points'] = hydra_data[partition]['n_points']
                mean_isac_dict['mean_costs'] = np.mean(isac_dict['costs'])
                mean_isac_dict['mean_par_runtime'] = np.mean(isac_dict['par_runtimes'])
                mean_isac_dict['mean_runtime'] = np.mean(isac_dict['runtimes'])
                mean_isac_dict['timeouts'] = isac_dict['timeouts']
                mean_isac_dict['n_points'] = isac_dict['cropped_n_points']
                for partition in isac_data:
                    mean_isac_data[partition]['mean_cost'] = np.mean(isac_data[partition]['costs'])
                    mean_isac_data[partition]['mean_par_runtime'] = np.mean(isac_data[partition]['par_runtimes'])
                    mean_isac_data[partition]['mean_runtime'] = np.mean(isac_data[partition]['runtimes'])
                    mean_isac_data[partition]['timeouts'] = isac_data[partition]['timeouts']
                    mean_isac_data[partition]['n_points'] = isac_data[partition]['n_points']
                mean_def_dict['mean_costs'] = np.mean(def_dict['costs'])
                mean_def_dict['mean_par_runtime'] = np.mean(def_dict['par_runtimes'])
                mean_def_dict['mean_runtime'] = np.mean(def_dict['runtimes'])
                mean_def_dict['timeouts'] = def_dict['timeouts']
                mean_def_dict['n_points'] = def_dict['cropped_n_points']
                for partition in def_data:
                    mean_def_data[partition]['mean_costs'] = np.mean(def_data[partition]['costs'])
                    mean_def_data[partition]['mean_par_runtime'] = np.mean(def_data[partition]['par_runtimes'])
                    mean_def_data[partition]['mean_runtime'] = np.mean(def_data[partition]['runtimes'])
                    mean_def_data[partition]['timeouts'] = def_data[partition]['timeouts']
                    mean_def_data[partition]['n_points'] = def_data[partition]['n_points']
                mean_smac_dict['mean_costs'] = np.mean(smac_dict['costs'])
                mean_smac_dict['mean_par_runtime'] = np.mean(smac_dict['par_runtimes'])
                mean_smac_dict['mean_runtime'] = np.mean(smac_dict['runtimes'])
                mean_smac_dict['timeouts'] = smac_dict['timeouts']
                mean_smac_dict['n_points'] = smac_dict['cropped_n_points']
                for partition in smac_data:
                    mean_smac_data[partition]['mean_costs'] = np.mean(smac_data[partition]['costs'])
                    mean_smac_data[partition]['mean_par_runtime'] = np.mean(smac_data[partition]['par_runtimes'])
                    mean_smac_data[partition]['mean_runtime'] = np.mean(smac_data[partition]['runtimes'])
                    mean_smac_data[partition]['timeouts'] = smac_data[partition]['timeouts']
                    mean_smac_data[partition]['n_points'] = smac_data[partition]['n_points']
                mean_piac_dict['mean_costs'] = np.mean(piac_dict['costs'])
                mean_piac_dict['mean_par_runtime'] = np.mean(piac_dict['par_runtimes'])
                mean_piac_dict['mean_runtime'] = np.mean(piac_dict['runtimes'])
                mean_piac_dict['timeouts'] = piac_dict['timeouts']
                mean_piac_dict['n_points'] = piac_dict['cropped_n_points']
                logger.info('%d unsolvable instances ignored!' % (
                    piac_dict['n_points'] - mean_piac_dict['n_points']))
                for partition in piac_data:
                    mean_piac_data[partition]['mean_costs'] = np.mean(piac_data[partition]['costs'])
                    mean_piac_data[partition]['mean_par_runtime'] = np.mean(piac_data[partition]['par_runtimes'])
                    mean_piac_data[partition]['mean_runtime'] = np.mean(piac_data[partition]['runtimes'])
                    mean_piac_data[partition]['timeouts'] = piac_data[partition]['timeouts']
                    mean_piac_data[partition]['n_points'] = piac_data[partition]['n_points']
            n_piac_cluster = len(list(piac_data.keys()))
            n_isac_cluster = len(list(isac_data.keys()))
            n_hydra_cluster = 10  # len(list(hydra_data.keys()))
            num_piac_parts[-1].append(n_piac_cluster)
            # logger.info('Validation used %d piac partitions' % n_piac_cluster)
            # logger.info('Validation used %d hydra partitions' % n_hydra_cluster)
            # logger.info('Validation used %d isac partitions' % n_isac_cluster)
            # logger.info('')
            # logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
            # logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
            # logger.info('*{:^{width}s}*'.format('Experiment data of {date}'.format(date=date), width=MAX_PRINT_LEN))
            # logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
            # logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
            # logger.info('*{:^{width}s}*'.format('MEAN RESULTS', width=MAX_PRINT_LEN))
            # HEAD = 'SMAC'
            # if margs.default:
            #     HEAD = 'DEF'
            # elif margs.isac:
            #     HEAD = 'ISAC'
            # elif margs.hydra:
            #     HEAD = 'HYDRA'
            # logger.info('*{:^{width}s}*'.format(
            #     '{:^{half}s}|{:^{half}s}'.format('PIAC', HEAD,
            #                                      half=MAX_PRINT_LEN / 6),
            #     width=MAX_PRINT_LEN))
            # logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
            # _output_lines(logger, KEY_ORDER, mean_piac_dict, mean_smac_dict, mean=True, def_=margs.default)
            # logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
            # logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
            piac_values.append(mean_piac_dict[margs.plot])
        overall.append([mean_def_dict, mean_smac_dict, mean_piac_dict, mean_hydra_dict, mean_isac_dict])
        all_piac_vals.append(piac_values)
        all_deff_vals.append(mean_def_dict[margs.plot])
        all_smac_vals.append(mean_smac_dict[margs.plot])
        all_hydr_vals.append(mean_hydra_dict[margs.plot])
        all_isac_vals.append(mean_isac_dict[margs.plot])
        logging.info('\n' * 20)

    median_smac = overall[0][1]
    median_def = overall[0][0]
    median_hydr = overall[0][3]
    median_isac = overall[0][4]

    for key in median_smac:
        if 'runtime' not in key:
            median_smac[key] = int(np.median(list(map(lambda x: x[1][key], overall))))
            median_def[key] = int(np.median(list(map(lambda x: x[0][key], overall))))
            median_hydr[key] = int(np.median(list(map(lambda x: x[3][key], overall))))
            median_isac[key] = int(np.median(list(map(lambda x: x[4][key], overall))))
        else:
            median_smac[key] = np.median(list(map(lambda x: x[1][key], overall)))
            median_def[key] = np.median(list(map(lambda x: x[0][key], overall)))
            median_hydr[key] = np.median(list(map(lambda x: x[3][key], overall)))
            median_isac[key] = np.median(list(map(lambda x: x[4][key], overall)))

    print('\\begin{sidewaystable}')
    print('\\begin{tabular}{lll %s}' % ('r' * (len(overall[:4]) + 4)))
    print('\\toprule')
    print(' &', ' &', ' &', 'Default', '&', 'SMAC', '&', 'ISAC', '&', 'Hydra', '&',
          '\\multicolumn{%d}{c}{ISMAC$^*$}\\\\' % len(
              overall[:4]))
    print('\\midrule')
    for key in KEY_ORDER:
        if 'crash' in key or 'points' in key or 'costs' in key:
            continue
        if 'runtime' not in key:
            fmt_str = '{:3d}'
        else:
            fmt_str = '{:3.2f}'
        tmp = [' ', ' ', key.replace('_', ' '), fmt_str.format(median_def[key]), fmt_str.format(median_smac[key]),
               fmt_str.format(median_isac[key]),
               fmt_str.format(median_hydr[key])]
        tmp.extend(list(map(lambda x: fmt_str.format(x[2][key]), overall[:4])))
        # print(tmp)
        prnt_str = ' & '.join(tmp) + '\\\\'
        print(prnt_str)
    exp_str = [' ', ' ', 'Exploration', '-', '-', '-', '-']
    inst_str = [' ', ' ', 'min \\#insts', '-', '-', '-', '-']
    budg_str = [' ', ' ', 'budget strat.', '-', '-', '-', '-']
    regu_str = [' ', ' ', 'regularized', '-', '-', '-', '-']
    for b in belong_together:
        b = b[0]
        if margs.brute:
            tamB = b.split('_')[1:]
        else:
            tamB = b.split('_')
        tamB[4] = tamB[4].replace('min', '')
        inst_str.append(tamB[4])
        if '_b_' in b:
            budg_str.append('I')
        else:
            budg_str.append('E')
        if '_r_' in b:
            regu_str.append('\\cmark')
        else:
            regu_str.append('\\xmark')
        if 'none' in b:
            exp_str.append('\\xmark')
        else:
            exp_str.append('\\cmark')
    print('\\midrule')
    print(' & '.join(exp_str[:7 + (len(overall[:4]))]) + '\\\\')
    print(' & '.join(budg_str[:7 + (len(overall[:4]))]) + '\\\\')
    print(' & '.join(regu_str[:7 + (len(overall[:4]))]) + '\\\\')
    print(' & '.join(inst_str[:7 + (len(overall[:4]))]) + '\\\\')
    print('\\bottomrule')
    print('\\end{tabular}')
    print('\\end{sidewaystable}')

    print()

    print('\\begin{sidewaystable}')
    print('\\begin{tabular}{lll %s}' % ('r' * (len(overall[4:14]))))
    print('\\toprule')
    print('&', '&', '&', '\\multicolumn{%d}{c}{ISMAC$^*$}\\\\' % len(overall[4:14]))
    print('\\midrule')
    for key in KEY_ORDER:
        if 'crash' in key or 'points' in key or 'costs' in key:
            continue
        if 'runtime' not in key:
            fmt_str = '{:3d}'
        else:
            fmt_str = '{:3.2f}'
        tmp = [' ', ' ', key.replace('_', ' ')]
        tmp.extend(list(map(lambda x: fmt_str.format(x[2][key]), overall[4:14])))
        # print(tmp)
        prnt_str = ' & '.join(tmp) + '\\\\'
        print(prnt_str)
    print('\\midrule')
    print('&', '&', exp_str[2], '&',
          ' & '.join(exp_str[7 + len(overall[:4]): 7 + len(overall[:4]) + len(overall[4:14])]) + '\\\\')
    print('&', '&', budg_str[2], '&',
          ' & '.join(budg_str[7 + len(overall[:4]): 7 + len(overall[:4]) + len(overall[4:14])]) +
          '\\\\')
    print('&', '&', regu_str[2], '&',
          ' & '.join(regu_str[7 + len(overall[:4]): 7 + len(overall[:4]) + len(overall[4:14])]) +
          '\\\\')
    print('&', '&', inst_str[2], '&',
          ' & '.join(inst_str[7 + len(overall[:4]): 7 + len(overall[:4]) + len(overall[4:14])]) +
          '\\\\')
    print('\\bottomrule')
    print('\\end{tabular}')
    print('\\end{sidewaystable}')

    print()

    print('\\begin{sidewaystable}\\centering')
    print('\\begin{tabular}{lll %s}' % ('r' * (len(overall[14:]))))
    print('\\toprule')
    print('&', '&', '&', '\\multicolumn{%d}{c}{ISMAC$^*$}\\\\' % len(overall[14:]))
    print('\\midrule')
    for key in KEY_ORDER:
        if 'crash' in key or 'points' in key or 'costs' in key:
            continue
        if 'runtime' not in key:
            fmt_str = '{:3d}'
        else:
            fmt_str = '{:3.2f}'
        tmp = [' ', ' ', key.replace('_', ' ')]
        tmp.extend(list(map(lambda x: fmt_str.format(x[2][key]), overall[14:])))
        # print(tmp)
        prnt_str = ' & '.join(tmp) + '\\\\'
        print(prnt_str)
    print('\\midrule')
    print('&', '&', exp_str[2], '&', ' & '.join(exp_str[7 + len(overall[:4]) + len(overall[4:14]):]) + '\\\\')
    print('&', '&', budg_str[2], '&', ' & '.join(budg_str[7 + len(overall[:4]) + len(overall[4:14]):]) + '\\\\')
    print('&', '&', regu_str[2], '&', ' & '.join(regu_str[7 + len(overall[:4]) + len(overall[4:14]):]) + '\\\\')
    print('&', '&', inst_str[2], '&', ' & '.join(inst_str[7 + len(overall[:4]) + len(overall[4:14]):]) + '\\\\')
    print('\\bottomrule')
    print('\\end{tabular}')
    print('\\end{sidewaystable}')

    for idx, dates in enumerate(belong_together):
        title = prefixes[idx]
        title = title.split('_')
        if title[0] == 'piac':
            title = title[1:]
            if len(title[-1]) == 1:
                title = title[:-1]
        title = title[0:1] + title[2:]
        fig = plt.figure()
        plt.title(' '.join(title))
        plt.plot(range(len(dates)), [int(np.median(all_deff_vals)) for _ in range(len(dates))], label='Default',
                 ls=':')
        plt.plot(range(len(dates)), [int(np.median(all_smac_vals)) for _ in range(len(dates))], label='SMAC',
                 ls='--')
        plt.plot(range(len(dates)), [int(np.median(all_hydr_vals)) for _ in range(len(dates))], label='Hydra')
        plt.plot(range(len(dates)), [int(np.median(all_isac_vals)) for _ in range(len(dates))], label='ISAC',
                 ls='-.')
        plt.plot(range(len(dates)), all_piac_vals[idx], label='PIAC')
        # print(num_piac_parts[idx])
        for i in range(len(dates)):
            plt.annotate(str(num_piac_parts[idx][i]), xy=(i, all_piac_vals[idx][i]))
        plt.annotate(str(n_isac_cluster), xy=(i, int(np.median(all_isac_vals))))
        plt.annotate(str(n_hydra_cluster), xy=(i, int(np.median(all_hydr_vals))))
        if margs.plot in ['successes', 'timeouts']:
            plt.ylabel('#' + margs.plot[0].upper() + margs.plot[1:])
        else:
            tmp_label = margs.plot.split('_')
            for j, tml in enumerate(tmp_label):
                tmp_label[j] = tml[0].upper() + tml[1:]
            plt.ylabel(' '.join(tmp_label + ['[sec]']))
        plt.xlabel('#Iterations')
        plt.xlim([-.25, len(dates) - 0.75])

        ax = plt.gca()

        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.1,
                         box.width, box.height * 0.9])

        # Put a legend below current axis
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
                  fancybox=True, shadow=True, ncol=5)
        name = '_'.join(title)
        if margs.brute:
            name = 'brute_' + name
        name = margs.mode + '_' + name
        fig.savefig(name)
        plt.show()


if __name__ == '__main__':
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', help='Directory to search .json files in.')
    parser.add_argument('cutoff', help='cutoff_value', type=int)
    parser.add_argument('--par', help='PAR value', type=int, default=10)
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-m', '--mode', default='TEST', help='Validate on which set(s)',
                        choices=['TEST', 'TRAIN', 'TRAIN+TEST'])
    parser.add_argument('-n', '--noPAR', action='store_true', help="Don't use PAR values but raw runtimes.")
    parser.add_argument('-d', '--default', action='store_true', help='Use default results')
    parser.add_argument('-b', '--brute', action='store_true', help='Use brute results')
    parser.add_argument('-i', '--isac', action='store_true', help='Use isac results')
    parser.add_argument('-H', '--hydra', action='store_true', help='Use hydra results')
    parser.add_argument('-s', '--save_plots_to', help='Name to save the plots')
    parser.add_argument('-p', '--plot', choices=['mean_costs', 'mean_par_runtime', 'mean_runtime', 'timeouts',
                                                 'successes'], default='timeouts')
    parser.add_argument('-u', '--noUnsolvable', help="""Ignore instances that were not solved
                        by any approach/configuration""", action='store_true')

    args, unknown = parser.parse_known_args()
    logFormatter = logging.Formatter("%(message)s")
    logging.basicConfig(level=verbose_lvl_dict[args.verbosity])
    main(args)
