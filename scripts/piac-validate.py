import os
import sys
import glob
import inspect
import argparse
import pickle
import logging
from typing import Union, List
import pprint
import json
import time
import datetime

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

import matplotlib as mpl

mpl.use('Agg')
import numpy as np
import matplotlib as mpl
from sklearn.metrics import euclidean_distances
from sklearn.ensemble import RandomForestClassifier

mpl.use('Agg')

from smac.utils.io.input_reader import InputReader
from smac.runhistory.runhistory import RunHistory
from smac.optimizer.objective import average_cost
from smac.scenario.scenario import Scenario

from piac.tae import ExecuteTARun, ExecuteTARunOld, StatusType
from piac.configspace.split_criteria import AxisAlignedSplit, NonAxisAlignedSplit
from piac.util import n_ary_tree
from piac.util.utils import get_tae
from piac.util.stats import PIACStats
from piac.configspace import Configuration
from piac.util.utils import read_traj_file, load_runhistory_from_disc


def traverse_tree(node: n_ary_tree):
    """
    Helper method to print some contents of the tree
    :param node: n_ary_tree node
    :return: None
    """
    print('#' * 120)
    for child in node.children:
        traverse_tree(child)
    print('~' * 120)
    print(node.split_criterion)
    print(node.instance_names)
    print(node.configuration)


def _read_test_instances(fh: str) -> list:
    in_reader = InputReader()
    return in_reader.read_instance_file(fh)


def _read_inst_feats(fh: str) -> dict:
    in_reader = InputReader()
    return in_reader.read_instance_features_file(fh)[1]


def _run_conf_on_inst(tae: Union[ExecuteTARun, ExecuteTARunOld], config: Union[dict, Configuration],
                      inst: str, scenario: Scenario, seed: int = 12345) -> tuple:
    """
    Runs config on inst
    :param config: config (dict)
    :param inst: inst id (string)
    :return: (Status [1-5], cost, runtime, inst_specific) 1 = SUCCESS, 2 = TIMEOUT other Status IDS indicate crashed
    """
    empirical_result = tae.start(config, instance=inst,
                                 cutoff=scenario.cutoff,
                                 seed=seed,
                                 instance_specific="0")
    return empirical_result


def trickle_down_tree(node: n_ary_tree, inst_feat_dict: dict,
                      instance: str, depth: int = 0) -> (Configuration, str):
    """
    Recursive method that climbs up the tree to determine which configuration to apply to a given instance
    :param node: n_ary_tree
    :param inst_feat_dict: Dictionary with instance_name -> instance_feature mapping
    :param instance: instance_name to lookup features in inst_feat_dict
    :param depth: Used for debug log output
    :return: (Configuration to apply to instance, Name of cluster this configuration belongs to)
    """
    if node.split_criterion:
        go_left = node.split_criterion(inst_feat_dict, instance)  # type: Union[AxisAlignedSplit, NonAxisAlignedSplit]
        if go_left and node.children[0].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 0))
            return trickle_down_tree(node.children[0], inst_feat_dict, instance, depth + 1)
        elif go_left and node.children[1].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 1))
            return trickle_down_tree(node.children[1], inst_feat_dict, instance, depth + 1)
        if not go_left and not node.children[0].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 0))
            return trickle_down_tree(node.children[0], inst_feat_dict, instance, depth + 1)
        elif not go_left and not node.children[1].left:
            logging.debug('At depth {:>3d} {:1s} (is child {:>3d})'.format(depth, 'l' if go_left else 'r', 1))
            return trickle_down_tree(node.children[1], inst_feat_dict, instance, depth + 1)
    else:
        logging.debug('Found Configuration at depth {:>3d}'.format(depth))
        return node.configuration, node.name


def _setup_evaluation(args_) -> (n_ary_tree, np.ndarray, dict, Union[ExecuteTARunOld, ExecuteTARun], Scenario):
    """
    Method to read in all the necessary files for PIAC
    :param args_: Argparse Object
    """
    np.random.seed(args.seed)
    with open(args_.tree, 'rb') as tree_fh:
        tree = pickle.load(tree_fh)  # type: n_ary_tree
    # traverse_tree(tree)
    scen = Scenario(args_.scenario,  # cmd_args needed to suppres SMAC scenario output folder creation
                    cmd_args={'output_dir': ""})  # type: Scenario
    test_inst_array = np.array(_read_test_instances(scen.test_inst_fn)).flatten()
    train_inst_array = np.array(_read_test_instances(scen.train_inst_fn)).flatten()
    if args.mode == 'TEST':
        inst_array = test_inst_array
    elif args.mode == 'TRAIN':
        inst_array = train_inst_array
    else:
        inst_array = np.vstack((test_inst_array.reshape(-1, 1), train_inst_array.reshape(-1, 1))).flatten()
    inst_feat_dict = _read_inst_feats(scen.feature_fn)

    if tree.preprocessor:
        feature_array = []
        for inst_ in inst_array:
            feature_array.append(inst_feat_dict[inst_])
        feature_array = np.array(feature_array)
        feature_array = tree.preprocessor(feature_array)

        for feat, inst_ in zip(feature_array, inst_array):
            inst_feat_dict[inst_] = feat

    tar_run_limit = len(inst_array) * args_.n_runs
    times = 1
    if args_.smac:
        times += 1
    if args_.default:
        times += 1
    if args_.isac:
        times += 1
    if args_.hydra:
        times += 1
    tar_run_limit *= times
    stats = PIACStats(ta_run_limit=tar_run_limit)
    stats.start_timing()
    runhist = RunHistory(aggregate_func=average_cost)
    tae = get_tae(args_.tae, scen, stats=stats, runhist=runhist)
    return tree, inst_array, inst_feat_dict, tae, scen


def _update_stats_of_cluster(cluster_name: str, cluster_dict: dict, empirical_result: Union[tuple, List],
                             scen: Scenario, inst: str) -> dict:
    """
    Method to update the overall dictionary that keeps track of the results
    :param cluster_name: In which cluster to put the results
    :param cluster_dict: Needed for recursive initialization
    :param empirical_result: The empirical results to store
    :param scen: The scenario used for validation
    :param inst: The name of the instance used for validation
    :return: dict
    """
    if cluster_name not in cluster_dict.keys():
        # Status [1-5], runtime, cost, inst_specific
        cluster_dict[cluster_name] = {'n_points': 0,
                                      'timeouts': 0,
                                      'crashes': 0,
                                      'successes': 0,
                                      'runtimes': [],
                                      'par_runtimes': [],
                                      'costs': [],
                                      'insts': []}
        return _update_stats_of_cluster(cluster_name, cluster_dict, empirical_result, scen, inst)
    else:
        res, cost, par, run = empirical_result[0], empirical_result[1], empirical_result[1], empirical_result[2]
        if res.value == 1 and scen.cutoff <= run:
            logging.warning('FOUND SUCCESS HIGHER THAN CUTOFF!!!!')
            logging.warning('Setting to par*cutoff')
            res = StatusType.TIMEOUT
            par = scen.cutoff * scen.par_factor
            cost = par
            logging.warning('\-/|' * 30)
        cluster_dict[cluster_name]['n_points'] += 1
        if res.value == 1:
            cluster_dict[cluster_name]['successes'] += 1
        elif res.value == 2:
            cluster_dict[cluster_name]['timeouts'] += 1
        else:
            cluster_dict[cluster_name]['crashes'] += 1
        cluster_dict[cluster_name]['par_runtimes'].append(par)
        cluster_dict[cluster_name]['costs'].append(cost)
        cluster_dict[cluster_name]['runtimes'].append(run)
        cluster_dict[cluster_name]['insts'].append(inst)
        return cluster_dict


def get_mean_cluster_dict(cluster_dict: dict) -> dict:
    """
    Helper to compute mean statistics given the overall dict
    """
    mean_cluster_dict = {}
    for name in cluster_dict.keys():
        if name not in mean_cluster_dict:
            mean_cluster_dict[name] = {'n_points': 0, 'timeouts': 0, 'crashes': 0, 'successes': 0,
                                       'mean_runtime': 0., 'mean_par_runtime': 0., 'mean_costs': 0.}
        mean_cluster_dict[name]['n_points'] = cluster_dict[name]['n_points']
        mean_cluster_dict[name]['timeouts'] = cluster_dict[name]['timeouts']
        mean_cluster_dict[name]['crashes'] = cluster_dict[name]['crashes']
        mean_cluster_dict[name]['successes'] = cluster_dict[name]['successes']
        mean_cluster_dict[name]['mean_runtime'] = np.mean(cluster_dict[name]['runtimes'])
        mean_cluster_dict[name]['mean_par_runtime'] = np.mean(cluster_dict[name]['par_runtimes'])
        mean_cluster_dict[name]['mean_costs'] = np.mean(cluster_dict[name]['costs'])
    return mean_cluster_dict


def get_median_over_n_runs(args_, tae, piac_config, instance: str, scen: Scenario, incumbent=None,
                           default_config=None, isac=None, hydra=None,
                           sort_by_runtime: bool = False) -> ((StatusType, float, float, str),
                                                              (StatusType, float, float, str),
                                                              (StatusType, float, float, str),
                                                              (StatusType, float, float, str),
                                                              (StatusType, float, float, str)):
    """
    As the name suggests, this method gets the median over n runs. 
    TODO refactor to use less copy paste code *HORRIBLE HORRIBLE STYLE*
    :param hydra: Configuration used with hydra
    :param isac: Configuration used with isac
    :param args_: argparse object that needs to contain n_runs and two flags (smac & default) to determine if smac and
                  the default are to be validated
    :param tae: the TAE to use
    :param piac_config: config that was determined to be the best by the piac system
    :param instance: instance to validate the config on
    :param scen: scenario object
    :param incumbent: the config to use for smac
    :param default_config: the default configuration
    :param sort_by_runtime: sort key, to sort either by cost or by runtime
    :return: three lists that contain the empirical piac, smac and default results
    """
    empirical_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                     np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
    empirical_smac_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                          np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
    empirical_def_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                         np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
    empirical_isa_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                         np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
    empirical_hyd_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                         np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
    for num_run in range(args_.n_runs):
        inst_seed = np.random.randint(0, 999999)
        logging.info('%s #round %d - seed %d' % ('-' * 20, num_run, inst_seed))
        empirical_result = _run_conf_on_inst(tae=tae, config=piac_config, inst=instance, scenario=scen,
                                             seed=inst_seed)
        empirical_res[0][num_run] = empirical_result[0]
        empirical_res[1][num_run] = empirical_result[1]
        empirical_res[2][num_run] = empirical_result[2]
        empirical_res[3][num_run] = empirical_result[3]
        if args_.smac:
            empirical_smac_result = _run_conf_on_inst(tae=tae, config=incumbent, inst=instance, scenario=scen,
                                                      seed=inst_seed)
            empirical_smac_res[0][num_run] = empirical_smac_result[0]
            empirical_smac_res[1][num_run] = empirical_smac_result[1]
            empirical_smac_res[2][num_run] = empirical_smac_result[2]
            empirical_smac_res[3][num_run] = empirical_smac_result[3]
        if args_.default:
            empirical_def_result = _run_conf_on_inst(tae=tae, config=default_config, inst=instance, scenario=scen,
                                                     seed=inst_seed)
            empirical_def_res[0][num_run] = empirical_def_result[0]
            empirical_def_res[1][num_run] = empirical_def_result[1]
            empirical_def_res[2][num_run] = empirical_def_result[2]
            empirical_def_res[3][num_run] = empirical_def_result[3]
        if args_.isac:
            empirical_isa_result = _run_conf_on_inst(tae=tae, config=isac, inst=instance, scenario=scen,
                                                     seed=inst_seed)
            empirical_isa_res[0][num_run] = empirical_isa_result[0]
            empirical_isa_res[1][num_run] = empirical_isa_result[1]
            empirical_isa_res[2][num_run] = empirical_isa_result[2]
            empirical_isa_res[3][num_run] = empirical_isa_result[3]
        if args_.hydra:
            empirical_hyd_result = _run_conf_on_inst(tae=tae, config=hydra, inst=instance, scenario=scen,
                                                     seed=inst_seed)
            empirical_hyd_res[0][num_run] = empirical_hyd_result[0]
            empirical_hyd_res[1][num_run] = empirical_hyd_result[1]
            empirical_hyd_res[2][num_run] = empirical_hyd_result[2]
            empirical_hyd_res[3][num_run] = empirical_hyd_result[3]

    sort_by = 2 if sort_by_runtime else 1

    sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_res[sort_by]), key=lambda x: x[1])))
    median_idx = (args_.n_runs - 1) / 2
    if median_idx == np.floor(median_idx):
        median_idx = int(median_idx)
        empirical_res[0] = empirical_res[0][sort_keys][median_idx]
        empirical_res[1] = empirical_res[1][sort_keys][median_idx]
        empirical_res[2] = empirical_res[2][sort_keys][median_idx]
        empirical_res[3] = empirical_res[3][sort_keys][median_idx]
    else:
        if sort_by_runtime:  # if we sort by runtime, the mean of a timeout and a success will also be a success,
            # thus we have to take the smaller value / the success state
            empirical_res[0] = empirical_res[0][sort_keys][int(np.floor(median_idx))]
        else:  # else we have a par factor which will cause the smaller value to have less weight
            empirical_res[0] = empirical_res[0][sort_keys][int(np.ceil(median_idx))]
        empirical_res[1] = np.mean([empirical_res[1][sort_keys][int(np.floor(median_idx))],
                                    empirical_res[1][sort_keys][int(np.ceil(median_idx))]])
        empirical_res[2] = np.mean([empirical_res[2][sort_keys][int(np.floor(median_idx))],
                                    empirical_res[2][sort_keys][int(np.ceil(median_idx))]])
        empirical_res[3] = empirical_res[3][sort_keys][int(np.floor(median_idx))]

    if args_.smac:
        sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_smac_res[sort_by]), key=lambda x: x[1])))
        median_idx = (args_.n_runs - 1) / 2
        if median_idx == np.floor(median_idx):
            median_idx = int(median_idx)
            empirical_smac_res[0] = empirical_smac_res[0][sort_keys][median_idx]
            empirical_smac_res[1] = empirical_smac_res[1][sort_keys][median_idx]
            empirical_smac_res[2] = empirical_smac_res[2][sort_keys][median_idx]
            empirical_smac_res[3] = empirical_smac_res[3][sort_keys][median_idx]
        else:
            if sort_by_runtime:  # if we sort by runtime, the mean of a timeout and a success will also be a success,
                # thus we have to take the smaller value / the success state
                empirical_smac_res[0] = empirical_smac_res[0][sort_keys][int(np.floor(median_idx))]
            else:  # else we have a par factor which will cause the smaller value to have less weight
                empirical_smac_res[0] = empirical_smac_res[0][sort_keys][int(np.ceil(median_idx))]
            empirical_smac_res[1] = np.mean([empirical_smac_res[1][sort_keys][int(np.floor(median_idx))],
                                             empirical_smac_res[1][sort_keys][int(np.ceil(median_idx))]])
            empirical_smac_res[2] = np.mean([empirical_smac_res[2][sort_keys][int(np.floor(median_idx))],
                                             empirical_smac_res[2][sort_keys][int(np.ceil(median_idx))]])
            empirical_smac_res[3] = empirical_smac_res[3][sort_keys][int(np.floor(median_idx))]

    if args_.default:
        sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_def_res[sort_by]), key=lambda x: x[1])))
        median_idx = (args_.n_runs - 1) / 2
        if median_idx == np.floor(median_idx):
            median_idx = int(median_idx)
            empirical_def_res[0] = empirical_def_res[0][sort_keys][median_idx]
            empirical_def_res[1] = empirical_def_res[1][sort_keys][median_idx]
            empirical_def_res[2] = empirical_def_res[2][sort_keys][median_idx]
            empirical_def_res[3] = empirical_def_res[3][sort_keys][median_idx]
        else:
            if sort_by_runtime:  # if we sort by runtime, the mean of a timeout and a success will also be a success,
                # thus we have to take the smaller value / the success state
                empirical_def_res[0] = empirical_def_res[0][sort_keys][int(np.floor(median_idx))]
            else:  # else we have a par factor which will cause the smaller value to have less weight
                empirical_def_res[0] = empirical_def_res[0][sort_keys][int(np.ceil(median_idx))]
            empirical_def_res[1] = np.mean([empirical_def_res[1][sort_keys][int(np.floor(median_idx))],
                                            empirical_def_res[1][sort_keys][int(np.ceil(median_idx))]])
            empirical_def_res[2] = np.mean([empirical_def_res[2][sort_keys][int(np.floor(median_idx))],
                                            empirical_def_res[2][sort_keys][int(np.ceil(median_idx))]])
            empirical_def_res[3] = empirical_def_res[3][sort_keys][int(np.floor(median_idx))]

    if args_.isac:
        sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_isa_res[sort_by]), key=lambda x: x[1])))
        median_idx = (args_.n_runs - 1) / 2
        if median_idx == np.floor(median_idx):
            median_idx = int(median_idx)
            empirical_isa_res[0] = empirical_isa_res[0][sort_keys][median_idx]
            empirical_isa_res[1] = empirical_isa_res[1][sort_keys][median_idx]
            empirical_isa_res[2] = empirical_isa_res[2][sort_keys][median_idx]
            empirical_isa_res[3] = empirical_isa_res[3][sort_keys][median_idx]
        else:
            if sort_by_runtime:  # if we sort by runtime, the mean of a timeout and a success will also be a success,
                # thus we have to take the smaller value / the success state
                empirical_isa_res[0] = empirical_isa_res[0][sort_keys][int(np.floor(median_idx))]
            else:  # else we have a par factor which will cause the smaller value to have less weight
                empirical_isa_res[0] = empirical_isa_res[0][sort_keys][int(np.ceil(median_idx))]
            empirical_isa_res[1] = np.mean([empirical_isa_res[1][sort_keys][int(np.floor(median_idx))],
                                            empirical_isa_res[1][sort_keys][int(np.ceil(median_idx))]])
            empirical_isa_res[2] = np.mean([empirical_isa_res[2][sort_keys][int(np.floor(median_idx))],
                                            empirical_isa_res[2][sort_keys][int(np.ceil(median_idx))]])
            empirical_isa_res[3] = empirical_isa_res[3][sort_keys][int(np.floor(median_idx))]

    if args_.hydra:
        sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_hyd_res[sort_by]), key=lambda x: x[1])))
        median_idx = (args_.n_runs - 1) / 2
        if median_idx == np.floor(median_idx):
            median_idx = int(median_idx)
            empirical_hyd_res[0] = empirical_hyd_res[0][sort_keys][median_idx]
            empirical_hyd_res[1] = empirical_hyd_res[1][sort_keys][median_idx]
            empirical_hyd_res[2] = empirical_hyd_res[2][sort_keys][median_idx]
            empirical_hyd_res[3] = empirical_hyd_res[3][sort_keys][median_idx]
        else:
            if sort_by_runtime:  # if we sort by runtime, the mean of a timeout and a success will also be a success,
                # thus we have to take the smaller value / the success state
                empirical_hyd_res[0] = empirical_hyd_res[0][sort_keys][int(np.floor(median_idx))]
            else:  # else we have a par factor which will cause the smaller value to have less weight
                empirical_hyd_res[0] = empirical_hyd_res[0][sort_keys][int(np.ceil(median_idx))]
            empirical_hyd_res[1] = np.mean([empirical_hyd_res[1][sort_keys][int(np.floor(median_idx))],
                                            empirical_hyd_res[1][sort_keys][int(np.ceil(median_idx))]])
            empirical_hyd_res[2] = np.mean([empirical_hyd_res[2][sort_keys][int(np.floor(median_idx))],
                                            empirical_hyd_res[2][sort_keys][int(np.ceil(median_idx))]])
            empirical_hyd_res[3] = empirical_hyd_res[3][sort_keys][int(np.floor(median_idx))]
    return empirical_res, empirical_smac_res, empirical_def_res, empirical_isa_res, empirical_hyd_res


ISAC_STATS = []


def get_isac_conf(partition_list: List[n_ary_tree.TreeNode], scenario: Scenario, inst: str):
    """
    Out of all partitions / clusters found with isac, determine which to apply for a new instance
    :param partition_list: list of TreeNode objects that contain all the ISAC training results
    :param scenario: SMACScenario that contains all infos about the instances
    :param inst: instance_id (as str)
    :return: 
    """
    global ISAC_STATS
    if not ISAC_STATS:
        ISAC_STATS = [0 for _ in partition_list]
    dist_list = [np.float('inf') for _ in partition_list]
    preprocessor = partition_list[0].preprocessor
    inst_feat = preprocessor(scenario.feature_dict[inst])
    for idx, partition in enumerate(partition_list):
        if partition.name != '-1':
            logging.debug('Checking %s' % partition.name)
            tmp_dist = euclidean_distances(inst_feat.reshape(1, -1), partition.centroid.reshape(1, -1)).flatten()[0]
            dist_list[idx] = (tmp_dist, tmp_dist < partition.split_criterion, idx, partition.instances.shape[0])
            logging.debug(dist_list[idx])
            logging.debug(partition.split_criterion)
        else:
            dist_list[idx] = (np.float('inf'), True, idx, len(partition_list[idx].instances))
            partition.split_criterion = np.float('inf')
    dist_list = sorted(dist_list, key=lambda x: x[0])  # type: List[tuple]
    logging.debug('dist_list: %s' % str(dist_list))
    for d in dist_list:
        if d[1]:
            ISAC_STATS[d[2]] += 1
            logging.debug(ISAC_STATS)
            return partition_list[d[2]].configuration, partition_list[d[2]].name


def _setup_hydra(scenario):
    hydra_files = glob.glob('hydra**/**/*.pkl', recursive=True)
    overall_runhist = glob.glob('hydra**/**/overall_rh.json', recursive=True)
    # print(hydra_files, overall_runhist)
    with open(hydra_files[-1], 'rb') as hy:
        portfolio = pickle.load(hy)
    rh = RunHistory(average_cost)
    rh = load_runhistory_from_disc(overall_runhist[-1], scenario.cs, rh)

    sort_by = scenario.train_insts
    Y = []
    for config in portfolio:
        runs = np.array(rh.get_runs_for_config(config), dtype=object)
        y = []
        for inst in sort_by:
            y.append(average_cost(config, rh, runs[runs[:, 0] == inst]))
        y = np.array(y)
        Y.append(y)
    X = scenario.feature_array

    classifiers = []
    for i in range(len(portfolio)):
        for j in range(i + 1, len(portfolio)):
            y_i = Y[i]
            y_j = Y[j]
            y = y_i < y_j
            weights = np.abs(y_i - y_j)
            clf = RandomForestClassifier()
            clf.fit(X, y, weights)
            classifiers.append(clf)
    return portfolio, classifiers


def get_hydra_conf(portfolio: List[Configuration], classifiers: List[RandomForestClassifier], instance: str,
                   scenario: Scenario):
    x = scenario.feature_dict[instance].reshape(1, -1)
    scores = np.zeros((1, len(portfolio)))
    clf_indx = 0
    for i in range(len(portfolio)):
        for j in range(i + 1, len(portfolio)):
            clf = classifiers[clf_indx]
            Y = clf.predict(x)
            scores[Y == 1, i] += 1
            scores[Y == 0, j] += 1
            clf_indx += 1
    logging.debug(scores)
    min_ = np.argmax(scores, axis=1)[0]  # NOT MIN!!!! We want the winner not the looser !!!!!

    logging.debug('np.argmin: %d' % min_)
    return portfolio[min_], 'Conf_' + str(min_)


def main(args_):
    """
    Main method. Runs the whole validation process. Saves the result to json files
    :param args_: Argparse object
    :return: None
    """
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M:%S')
    path = os.path.join(os.getcwd(), '%s-piac-validation-%s-%s-%s' % (args_.name, ts, args_.seed, args_.mode))
    os.mkdir(path)
    with open(os.path.join(path, 'piac-validate-args.json'), 'w') as fh:
        json.dump(args_.__dict__, fh, sort_keys=True, indent=4, separators=(',', ': '))
    logging.basicConfig(level=verbose_lvl_dict[args_.verbosity])
    tree, inst_array, inst_feat_dict, tae, scen = _setup_evaluation(args)
    unique_configs = list()
    cluster_dict = {}
    incumbent = None
    hydra = None
    isac = None
    isac_list = None
    hy_m, hy_p = None, None
    hydra_cluster_name = None
    default_config = None
    if args_.default:
        default_config = scen.cs.get_default_configuration()
        unique_configs.append(default_config)
    if args_.smac:
        json_files = glob.glob('smac3**/**/traj_aclib2.json', recursive=True)
        tmp = []
        for jf in json_files:
            logging.info('READING %s' % jf)
            tmp.append(read_traj_file(scen.cs, jf))
        tmp_idcs = list(map(lambda y: y[0], sorted(enumerate(tmp), key=lambda x: x[1][1])))
        incumbent = tmp[tmp_idcs[0]][0]
        # incumbent = read_traj_file(scen.cs, json_files[2])[0]
        logging.info('Best incumbent found in %s' % json_files[tmp_idcs[0]])
        unique_configs.append(incumbent)
    if args_.isac:
        isac_files = glob.glob('isac**/**/*.pkl', recursive=True)
        with open(isac_files[-1], 'rb') as isac_in:
            isac_list = pickle.load(isac_in)
    if args_.hydra:
        hy_p, hy_m = _setup_hydra(scen)
    for idx, instance in enumerate(inst_array):
        if idx % 5 == 0:
            tae.stats.print_stats()
        logging.info('#' * 120)
        logging.info('#Inst {:>3d} of {:>3d}'.format(idx + 1, len(inst_array)))
        config, cluster_name = trickle_down_tree(tree, inst_feat_dict, instance)
        unique_configs.append(config)
        short_inst_name = '...' + instance[-37:]
        logging.info('~' * 120)
        logging.info('{:>40s} falls into cluster {:>15s}'.format(short_inst_name, cluster_name))

        if args_.isac:
            isac, isac_cluster_name = get_isac_conf(isac_list, scen, instance)
        if args_.hydra:
            hydra, hydra_cluster_name = get_hydra_conf(hy_p, hy_m, instance, scen)

        empirical_result, empirical_smac_result, empirical_def_result, empirical_isac_result, empirical_hydra_result \
            = get_median_over_n_runs(
                                     args_, tae, config, instance, scen, incumbent, default_config, isac, hydra,
                                     sort_by_runtime=False)

        logging.debug("PIAC: Status: %d, cost: %f, time. %f, additional: %s" % (
            empirical_result[0].value, empirical_result[1], empirical_result[2], str(empirical_result[3])))
        cluster_dict = _update_stats_of_cluster(cluster_name, cluster_dict, empirical_result, scen, instance)
        cluster_dict = _update_stats_of_cluster('piac', cluster_dict, empirical_result, scen, instance)
        if args_.smac:
            smac_cluster_name = 'smac_' + cluster_name
            logging.debug("SMAC: Status: %d, cost: %f, time. %f, additional: %s" % (
                empirical_smac_result[0].value, empirical_smac_result[1], empirical_smac_result[2],
                str(empirical_smac_result[3])))
            cluster_dict = _update_stats_of_cluster(smac_cluster_name, cluster_dict,
                                                    empirical_smac_result, scen, instance)
            cluster_dict = _update_stats_of_cluster('smac', cluster_dict, empirical_smac_result, scen, instance)
        if args_.default:
            def_cluster_name = 'def_' + cluster_name
            logging.debug(" DEF: Status: %d, cost: %f, time. %f, additional: %s" % (
                empirical_def_result[0].value, empirical_def_result[1], empirical_def_result[2],
                str(empirical_def_result[3])))
            cluster_dict = _update_stats_of_cluster(def_cluster_name, cluster_dict,
                                                    empirical_def_result, scen, instance)
            cluster_dict = _update_stats_of_cluster('def', cluster_dict, empirical_def_result, scen, instance)
        if args_.isac:
            logging.debug("ISAC: Status: %d, cost: %f, time. %f, additional: %s" % (
                empirical_isac_result[0].value, empirical_isac_result[1], empirical_isac_result[2],
                str(empirical_isac_result[3])))
            cluster_dict = _update_stats_of_cluster(isac_cluster_name, cluster_dict,
                                                    empirical_isac_result, scen, instance)
            cluster_dict = _update_stats_of_cluster('isac', cluster_dict, empirical_isac_result, scen, instance)
        if args_.hydra:
            logging.debug("HYDR: Status: %d, cost: %f, time. %f, additional: %s" % (
                empirical_hydra_result[0].value, empirical_hydra_result[1], empirical_hydra_result[2],
                str(empirical_hydra_result[3])))
            cluster_dict = _update_stats_of_cluster(hydra_cluster_name, cluster_dict,
                                                    empirical_hydra_result, scen, instance)
            cluster_dict = _update_stats_of_cluster('hydra', cluster_dict, empirical_hydra_result, scen, instance)

    tae.stats.print_stats()

    logging.info('Done')
    logging.debug('Unique Configurations used: {:>3d}'.format(len(set(unique_configs))))
    if not args_.smac:
        logging.info('\n' + pprint.pformat(get_mean_cluster_dict(cluster_dict)))
        with open(os.path.join(path, 'joint_evaluation_mean.json'), 'w') as fh:
            json.dump(get_mean_cluster_dict(cluster_dict), fh, sort_keys=True, indent=4, separators=(',', ': '))
        logging.debug('*' * 120)
        logging.debug('\n' + pprint.pformat(cluster_dict))
        with open(os.path.join(path, 'joint_evaluation.json'), 'w') as fh:
            json.dump(cluster_dict, fh, sort_keys=True, indent=4, separators=(',', ': '))
    else:
        logging.info('\n' + pprint.pformat(get_mean_cluster_dict(cluster_dict)))
        with open(os.path.join(path, 'joint_evaluation_mean.json'), 'w') as fh:
            json.dump(get_mean_cluster_dict(cluster_dict), fh, sort_keys=True, indent=4, separators=(',', ': '))
        logging.debug('*' * 120)
        logging.debug('\n' + pprint.pformat(cluster_dict))
        with open(os.path.join(path, 'joint_evaluation.json'), 'w') as fh:
            json.dump(cluster_dict, fh, sort_keys=True, indent=4, separators=(',', ': '))


if __name__ == '__main__':
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('tree', help='Path to the result PIAC cluster tree')
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-s', '--seed', default=12345, help='SEED for experiments', type=int)
    parser.add_argument('-m', '--mode', default='TEST', help='Validate on which set(s)',
                        choices=['TEST', 'TRAIN', 'TRAIN+TEST'])
    parser.add_argument('--smac', action='store_true', help='Load SMAC incumbent')
    parser.add_argument('--default', help='Validate default', action='store_true')
    parser.add_argument('--isac', help='Validate isac', action='store_true')
    parser.add_argument('--hydra', help='Validate hydra', action='store_true')
    parser.add_argument('-n', '--n_runs', help='Num runs per instance', default=1, type=int)
    parser.add_argument('scenario', help='Path to the scenario used')
    parser.add_argument('-N', '--name', help='Prefix of the outfile folder', default='unnamed')

    args, unknown = parser.parse_known_args()
    main(args)
