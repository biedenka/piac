import os
import sys
import csv
import glob
import pickle
import logging
import inspect
import argparse
from typing import Union, List

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from smac.scenario.scenario import Scenario
from smac.tae.execute_ta_run import StatusType, ExecuteTARun
from smac.tae.execute_ta_run_old import ExecuteTARunOld
from smac.configspace import Configuration
from smac.utils.io.input_reader import InputReader
from smac.runhistory.runhistory import RunHistory
from smac.optimizer.objective import average_cost

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from piac.util.n_ary_tree import TreeNode
from piac.util.utils import get_tae
from piac.util.stats import PIACStats


def traverse_tree(node: TreeNode, result: []):
    """
    Helper method to print some contents of the tree
    :param node: n_ary_tree node
    :return: None
    """
    if not node.split_criterion and node.instance_names is not None:
        result.append([node.configuration, node.name])
    for child in node.children:
        result = traverse_tree(child, result)
    return result


def _read_test_instances(fh: str) -> list:
    in_reader = InputReader()
    return in_reader.read_instance_file(fh)


def _read_inst_feats(fh: str) -> dict:
    in_reader = InputReader()
    return in_reader.read_instance_features_file(fh)[1]


def _run_conf_on_inst(tae: Union[ExecuteTARun, ExecuteTARunOld], config: Union[dict, Configuration],
                      inst: str, scenario: Scenario, seed: int = 12345) -> tuple:
    """
    Runs config on inst
    :param config: config (dict)
    :param inst: inst id (string)
    :return: (Status [1-5], cost, runtime, inst_specific) 1 = SUCCESS, 2 = TIMEOUT other Status IDS indicate crashed
    """
    empirical_result = tae.start(config, instance=inst,
                                 cutoff=scenario.cutoff,
                                 seed=seed,
                                 instance_specific="0")
    return empirical_result


def main():
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('tree', help='Path to the result PIAC cluster tree')
    parser.add_argument('scenario', help='Path to the scenario File')
    parser.add_argument('-m', '--mode', default='TRAIN', help='Validate on which set(s)',
                        choices=['TRAIN', 'TEST'])
    parser.add_argument('-n', '--n_runs', help='Num runs per instance', default=3, type=int)
    parser.add_argument('-N', '--name', help='save_file_name', default='autofolio')
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-s', '--seed', default=12345, type=int)
    args_, unknown = parser.parse_known_args()

    rng = np.random.RandomState(args_.seed)

    logging.basicConfig(level=verbose_lvl_dict[args_.verbosity])
    scen = Scenario(args_.scenario,  # cmd_args needed to suppres SMAC scenario output folder creation
                    cmd_args={'output_dir': ""})  # type: Scenario

    test_inst_array = np.array(_read_test_instances(scen.test_inst_fn)).flatten()
    train_inst_array = np.array(_read_test_instances(scen.train_inst_fn)).flatten()
    if args_.mode == 'TEST':
        inst_array = test_inst_array
    elif args_.mode == 'TRAIN':
        inst_array = train_inst_array
    else:
        inst_array = np.vstack((test_inst_array.reshape(-1, 1), train_inst_array.reshape(-1, 1))).flatten()
    inst_feat_dict = _read_inst_feats(scen.feature_fn)

    hydra_files = sorted(glob.glob('hydra**/**/*.pkl', recursive=True))
    with open(hydra_files[-1], 'rb') as hy:
        hydra_list = pickle.load(hy)

    isac_files = sorted(glob.glob('isac**/**/*.pkl', recursive=True))
    with open(isac_files[-1], 'rb') as isac_in:
        isac_list = pickle.load(isac_in)

    with open(args_.tree, 'rb') as tree_fh:
        tree = pickle.load(tree_fh)  # type: n_ary_tree
    piac_list = traverse_tree(tree, [])

    sort_by_runtime = False
    configs = list(map(lambda x: [x[1], 'h_%02d' % x[0]], enumerate(hydra_list)))
    configs.extend(piac_list)
    configs.extend(list(map(lambda x: [x.configuration, x.name], isac_list)))

    tar_run_limit = len(inst_array) * args_.n_runs * len(configs)
    times = 3
    tar_run_limit *= times
    stats = PIACStats(ta_run_limit=tar_run_limit)
    stats.start_timing()
    runhist = RunHistory(aggregate_func=average_cost)
    tae = get_tae(args_.tae, scen, stats=stats, runhist=runhist)

    configs = np.array(configs)
    feat_header = [' ']
    feat_header.extend(list(map(lambda x: 'f_%d' % x, range(len(scen.feature_array[0])))))
    perf_header = [' ']
    perf_header.extend(configs[:, 1])
    pperf_header = [' ']
    iperf_header = [' ']
    hperf_header = [' ']
    hperf_header.extend(configs[:len(hydra_list), 1])
    pperf_header.extend(configs[len(hydra_list):len(hydra_list) + len(piac_list), 1])
    iperf_header.extend(configs[len(hydra_list) + len(piac_list):, 1])

    write_header = True
    mode = 'w'
    already_checked = []

    if os.path.exists(args_.name + '_autofolio_perf_%s.csv' % args_.mode):
        logging.info('Found existing files!')
        mode = 'a'
        write_header = False
        with open(args_.name + '_autofolio_perf_%s.csv' % args_.mode, 'r') as fh:
            reader = csv.reader(fh, delimiter=',')
            start = True
            for row in reader:
                if start:
                    start = False
                else:
                    already_checked.append(row[0])

    with open(args_.name + '_autofolio_perf_%s.csv' % args_.mode, mode) as perf_file, \
            open(args_.name + '_autofolio_feats_%s.csv' % args_.mode, mode) as feats_file, \
            open(args_.name + '_piac_autofolio_perf_%s.csv' % args_.mode, mode) as pperf, \
            open(args_.name + '_isac_autofolio_perf_%s.csv' % args_.mode, mode) as iperf, \
            open(args_.name + '_hydra_autofolio_perf_%s.csv' % args_.mode, mode) as hperf:
        perf_writer = csv.writer(perf_file, delimiter=',')
        pperf_writer = csv.writer(pperf, delimiter=',')
        iperf_writer = csv.writer(iperf, delimiter=',')
        hperf_writer = csv.writer(hperf, delimiter=',')
        feat_writer = csv.writer(feats_file, delimiter=',')
        if write_header:
            perf_writer.writerow(perf_header)
            feat_writer.writerow(feat_header)

            pperf_writer.writerow(pperf_header)
            iperf_writer.writerow(iperf_header)
            hperf_writer.writerow(hperf_header)

        count = 0
        for inst in train_inst_array:
            logging.info('%3d/%3d insts' % (count, len(train_inst_array)))
            f_out = [inst]
            p_out = [inst]
            inst_seed = rng.randint(0, 999999, args_.n_runs)
            logging.info('%s' % inst)
            if inst in already_checked:
                logging.info('%s => Previously checked!' % ('#' * 20))
                count += 1
                continue
            f_out.extend(scen.feature_dict[inst])
            for config, name in configs:
                logging.info('\tEvaluating %s' % name)
                empirical_res = [np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object),
                                 np.empty(args_.n_runs, dtype=object), np.empty(args_.n_runs, dtype=object)]
                for num_run in range(args_.n_runs):
                    logging.info('\t%s #round %d - seed %d' % ('-' * 5, num_run, inst_seed[num_run]))
                    empirical_result = _run_conf_on_inst(tae=tae, config=config, inst=inst, scenario=scen,
                                                         seed=inst_seed[num_run])
                    empirical_res[0][num_run] = empirical_result[0]
                    empirical_res[1][num_run] = empirical_result[1]
                    empirical_res[2][num_run] = empirical_result[2]
                    empirical_res[3][num_run] = empirical_result[3]
                    logging.info('\t%s cost: %3.5f' % ('-' * 10, empirical_result[1]))

                sort_by = 2 if sort_by_runtime else 1

                sort_keys = list(map(lambda y: y[0], sorted(enumerate(empirical_res[sort_by]), key=lambda x: x[1])))
                median_idx = (args_.n_runs - 1) / 2
                if median_idx == np.floor(median_idx):
                    median_idx = int(median_idx)
                    empirical_res[0] = empirical_res[0][sort_keys][median_idx]
                    empirical_res[1] = empirical_res[1][sort_keys][median_idx]
                    empirical_res[2] = empirical_res[2][sort_keys][median_idx]
                    empirical_res[3] = empirical_res[3][sort_keys][median_idx]
                else:
                    if sort_by_runtime:
                        empirical_res[0] = empirical_res[0][sort_keys][int(np.floor(median_idx))]
                    else:  # else we have a par factor which will cause the smaller value to have less weight
                        empirical_res[0] = empirical_res[0][sort_keys][int(np.ceil(median_idx))]
                    empirical_res[1] = np.mean([empirical_res[1][sort_keys][int(np.floor(median_idx))],
                                                empirical_res[1][sort_keys][int(np.ceil(median_idx))]])
                    empirical_res[2] = np.mean([empirical_res[2][sort_keys][int(np.floor(median_idx))],
                                                empirical_res[2][sort_keys][int(np.ceil(median_idx))]])
                    empirical_res[3] = empirical_res[3][sort_keys][int(np.floor(median_idx))]
                if empirical_res[1] >= scen.cutoff:
                    empirical_res[1] = scen.par_factor * scen.cutoff
                p_out.append(empirical_res[1])
                logging.info('\t%s median cost: %3.5f' % (' ' * 20, empirical_res[1]))
            feat_writer.writerow(f_out)
            perf_writer.writerow(p_out)
            pperf_writer.writerow([p_out[0]] + p_out[len(hydra_list)+1:len(hydra_list) + 1 + len(piac_list)])
            iperf_writer.writerow([p_out[0]] + p_out[len(hydra_list) + 1 + len(piac_list):])
            hperf_writer.writerow(p_out[:len(hydra_list) + 1])
            perf_file.flush()
            feats_file.flush()
            pperf.flush()
            iperf.flush()
            hperf.flush()
            count += 1
    runhist.save_json(args_.name + 'all_configs_on_%s_runhist.json' % args_.mode)

if __name__ == '__main__':
    main()