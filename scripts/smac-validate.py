import os
import sys
import inspect
import argparse
import pickle
import logging
from typing import Union
import pprint
import json
import time
import datetime

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

import numpy as np
from smac.utils.io.input_reader import InputReader
from smac.runhistory.runhistory import RunHistory
from smac.smbo.objective import average_cost
from smac.scenario.scenario import Scenario as Scenario

from piac.tae import ExecuteTARun, ExecuteTARunOld
from piac.util import n_ary_tree
from piac.util.utils import get_tae
from piac.util.stats import PIACStats
from piac.configspace import Configuration, CategoricalHyperparameter, FloatHyperparameter, IntegerHyperparameter


def _read_traj_file(scenario: Scenario, fn: str) -> (Configuration, float):
    """
    Simple method to read in a trajectory file in the json format / aclib2 format
    :param fn:
        file name
    :return:
        tuple of (incumbent [Configuration], incumbent_cost [float])
    """
    if not (os.path.exists(fn) and os.path.isfile(fn)):  # File existence check
        raise FileNotFoundError('File %s not found!' % fn)
    with open(fn, 'r') as fh:
        for line in fh.readlines():
            pass
    line = line.strip()
    incumbent_dict = json.loads(line)
    inc_dict = {}
    for key_val in incumbent_dict['incumbent']:  # convert string to Configuration
        key, val = key_val.replace("'", '').split('=')
        if isinstance(scenario.cs.get_hyperparameter(key), (CategoricalHyperparameter)):
            inc_dict[key] = val
        elif isinstance(scenario.cs.get_hyperparameter(key), (FloatHyperparameter)):
            inc_dict[key] = float(val)
        elif isinstance(scenario.cs.get_hyperparameter(key), (IntegerHyperparameter)):
            inc_dict[key] = int(val)
    incumbent = Configuration(scenario.cs, inc_dict)
    incumbent_cost = incumbent_dict['cost']
    return incumbent, incumbent_cost


def _read_test_instances(fh: str) -> list:
    in_reader = InputReader()
    return in_reader.read_instance_file(fh)


def _read_inst_feats(fh: str) -> dict:
    in_reader = InputReader()
    return in_reader.read_instance_features_file(fh)[1]


def _run_conf_on_inst(tae: Union[ExecuteTARun, ExecuteTARunOld], config: dict,
                      inst: str, scenario: Scenario) -> tuple:
    """
    Runs config on inst
    :param config: config (dict)
    :param inst: inst id (string)
    :return: (Status [1-5], cost, runtime, inst_specific) 1 = SUCCESS, 2 = TIMEOUT other Status IDS indicate crashed
    """
    empirical_result = tae.start(config, instance=inst,
                                 cutoff=scenario.cutoff,
                                 seed=np.random.randint(0, 999999999),
                                 instance_specific="0")
    return empirical_result


def _setup_evaluation(args_) -> (n_ary_tree, np.ndarray, dict, Union[ExecuteTARunOld, ExecuteTARun], Scenario):
    np.random.seed(args.seed)

    scen = Scenario(args.scenario)
    test_inst_array = np.array(_read_test_instances(scen.test_inst_fn)).flatten()
    train_inst_array = np.array(_read_test_instances(scen.train_inst_fn)).flatten()
    if args.mode == 'TEST':
        inst_array = test_inst_array
    elif args.mode == 'TRAIN':
        inst_array = train_inst_array
    else:
        inst_array = np.vstack((test_inst_array.reshape(-1, 1), train_inst_array.reshape(-1, 1))).flatten()
    inst_feat_dict = _read_inst_feats(scen.feature_fn)
    stats = PIACStats(ta_run_limit=len(inst_array))
    stats.start_timing()
    runhist = RunHistory(aggregate_func=average_cost)
    tae = get_tae(args_.tae, scen, stats=stats, runhist=runhist)
    return inst_array, inst_feat_dict, tae, scen



def _update_stats_of_cluster(cluster_name: str, cluster_dict: dict, empirical_result: tuple) -> dict:
    if cluster_name not in cluster_dict.keys():
        # Status [1-5], runtime, cost, inst_specific
        cluster_dict[cluster_name] = {'n_points': 0,
                                      'timeouts': 0,
                                      'crashes': 0,
                                      'successes': 0,
                                      'runtimes': [],
                                      'par_runtimes': [],
                                      'costs': []}
        return _update_stats_of_cluster(cluster_name, cluster_dict, empirical_result)
    else:
        cluster_dict[cluster_name]['n_points'] += 1
        if empirical_result[0].value == 1:
            cluster_dict[cluster_name]['successes'] += 1
        elif empirical_result[0].value == 2:
            cluster_dict[cluster_name]['timeouts'] += 1
        else:
            cluster_dict[cluster_name]['crashes'] += 1
        cluster_dict[cluster_name]['par_runtimes'].append(empirical_result[1])
        cluster_dict[cluster_name]['costs'].append(empirical_result[1])
        cluster_dict[cluster_name]['runtimes'].append(empirical_result[2])
        return cluster_dict


def _get_mean_cluster_dict(cluster_dict: dict={}) -> dict:
    mean_cluster_dict = {}
    for name in cluster_dict.keys():
        if name not in mean_cluster_dict:
            mean_cluster_dict[name] = {'n_points': 0, 'timeouts': 0, 'crashes': 0, 'successes': 0,
                                       'mean_runtime': 0., 'mean_par_runtime': 0., 'mean_costs': 0.}
        mean_cluster_dict[name]['n_points'] = cluster_dict[name]['n_points']
        mean_cluster_dict[name]['timeouts'] = cluster_dict[name]['timeouts']
        mean_cluster_dict[name]['crashes'] = cluster_dict[name]['crashes']
        mean_cluster_dict[name]['successes'] = cluster_dict[name]['successes']
        mean_cluster_dict[name]['mean_runtime'] = np.mean(cluster_dict[name]['runtimes'])
        mean_cluster_dict[name]['mean_par_runtime'] = np.mean(cluster_dict[name]['par_runtimes'])
        mean_cluster_dict[name]['mean_costs'] = np.mean(cluster_dict[name]['costs'])
    return mean_cluster_dict


def main(args_):
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M:%S')
    path = os.path.join(os.getcwd(), 'smac-validation-%s-%s-%s' % (ts, args_.seed, args_.mode))
    os.mkdir(path)
    print(args_.__dict__)
    with open(os.path.join(path, 'smac-validate-args.json'), 'w') as fh:
        json.dump(args_.__dict__, fh, sort_keys=True, indent=4, separators=(',', ': '))
    logging.basicConfig(level=verbose_lvl_dict[args_.verbosity])
    inst_array, inst_feat_dict, tae, scen = _setup_evaluation(args_)
    incumbent = _read_traj_file(scen, args_.traj)[0]
    print(incumbent)
    unique_configs = list()
    cluster_dict = {}
    for idx, instance in enumerate(inst_array):
        logging.info('#'*120)
        logging.info('#Inst {:>3d} of {:>3d}'.format(idx + 1, len(inst_array)))
        config, cluster_name = incumbent, 'smac'
        unique_configs.append(config)
        short_inst_name = '...' + instance[-37:]
        logging.info('~'*120)
        logging.info('{:>40s} falls into cluster {:>15s}'.format(short_inst_name, cluster_name))
        empirical_result = _run_conf_on_inst(tae=tae, config=unique_configs[-1], inst=instance, scenario=scen)
        logging.debug("Return: Status: %d, cost: %f, time. %f, additional: %s" % (
            empirical_result[0].value, empirical_result[1], empirical_result[2], str(empirical_result[3])))
        cluster_dict = _update_stats_of_cluster(cluster_name, cluster_dict, empirical_result)

    logging.info('Done')
    logging.info('\n' + pprint.pformat(_get_mean_cluster_dict(cluster_dict)))
    with open(os.path.join(path, 'smac_evaluation_mean.json'), 'w') as fh:
        json.dump(_get_mean_cluster_dict(cluster_dict), fh, sort_keys=True, indent=4, separators=(',', ': '))
    logging.debug('*'*120)
    logging.debug('\n' + pprint.pformat(cluster_dict))
    with open(os.path.join(path, 'smac_evaluation.json'), 'w') as fh:
        json.dump(cluster_dict, fh, sort_keys=True, indent=4, separators=(',', ': '))



if __name__ == '__main__':
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('scenario', help='Path to the smac scenario')
    parser.add_argument('traj', help='Path to the smac trajectory')
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-t', '--tae', default='aclib', help='Which TAE to use', choices=['old', 'aclib'])
    parser.add_argument('-s', '--seed', default=12345, help='SEED for experiments', type=int)
    parser.add_argument('-m', '--mode', default='TEST', help='Valdiate on which set(s)',
                        choices=['TEST', 'TRAIN', 'TRAIN+TEST'])

    args, unknown = parser.parse_known_args()
    main(args)
