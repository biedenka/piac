import os
import sys
import glob
import logging
import inspect
import argparse

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from smac.scenario.scenario import Scenario

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)


def main():
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('scenario', help='Path to the scenario File')
    parser.add_argument('working_dir', help='Path to the scenario File')
    parser.add_argument('-m', '--mode', default='TRAIN', help='Validate on which set(s)',
                        choices=['TRAIN', 'TEST'])
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    args_, unknown = parser.parse_known_args()

    # rng = np.random.RandomState(args_.seed)

    logging.basicConfig(level=verbose_lvl_dict[args_.verbosity])
    scen = Scenario(args_.scenario,  # cmd_args needed to suppres SMAC scenario output folder creation
                    cmd_args={'output_dir': ""})  # type: Scenario

    # if args_.mode == 'TRAIN':
    #     args_.mode = ''
    mode = args_.mode if args_.mode == 'TEST' else ''

    list_ = np.array(sorted(glob.glob(os.path.join(args_.working_dir, '**autofolio*.csv'), recursive=True))).reshape(
        (-1, 5))
    all = [[], [], []]
    for l in list_:
        l = np.insert(l, 0, os.path.split(os.path.commonprefix(list(l)))[0])
        logging.debug(l)
        logging.info('Loading files from %s' % l[0])
        for file_ in l[3:]:
            if mode in file_:
                at = -1
                print(file_)
                if '_hydra_' in file_:
                    at = 0
                    logging.info('%s Reading in Hydra data' % ('#' * 20))
                elif '_isac_' in file_:
                    at = 1
                    logging.info('%s Reading in ISAC data' % ('#' * 20))
                elif '_piac_' in file_:
                    logging.info('%s Reading in PIAC data' % ('#' * 20))
                with open(file_, 'r') as fh:
                    count = 0
                    mean = []
                    for line in fh:
                        line = line.strip().split(',')
                        if count > 0:
                            inst_ = [line[0]]
                            vals = list(map(lambda x: float(x), line[1:]))
                            vals = list(map(
                                lambda x: x if x <= scen.cutoff * scen.par_factor else scen.cutoff*scen.par_factor,
                                vals))
                            # print(vals)
                            inst_.extend(vals)
                            line = inst_
                            mean.append(np.min(vals))
                            logging.debug(np.mean(mean))
                        logging.debug(line)
                        count += 1
                if '_piac_' in file_:
                    logging.info('%sPortfolio performance %f' % ('#'*120, np.mean(mean)))
                all[at].append(np.mean(mean))
    logging.info('%s' % ('#'*60))
    logging.info('%s' % ('#'*60))
    logging.info('Results obtained on %s' % args_.mode)
    logging.info('%s' % ('#'*60))
    logging.info('%s' % ('#'*60))
    logging.info('Median Performances:')
    hyd = np.median(all[0])
    isa = np.median(all[1])
    pia = np.median(all[2])
    logging.info('Hydra: %f' % hyd)
    logging.info(' ISAC: %f' % isa)
    logging.info(' PIAC: %f' % pia)
    logging.info('%s' % ('#'*60))
    logging.info('Mean/std Performances:')
    hyd = np.mean(all[0])
    hyds = np.std(all[0])
    isa = np.mean(all[1])
    isas = np.std(all[1])
    pia = np.mean(all[2])
    pias = np.std(all[2])
    logging.info('Hydra: %f/%f' % (hyd, hyds))
    logging.info(' ISAC: %f/%f' % (isa, isas))
    logging.info(' PIAC: %f/%f' % (pia, pias))
    logging.info('%s' % ('#'*60))
    logging.info('Min Performances:')
    hyd = np.min(all[0])
    isa = np.min(all[1])
    pia = np.min(all[2])
    logging.info('Hydra: %f' % hyd)
    logging.info(' ISAC: %f' % isa)
    logging.info(' PIAC: %f' % pia)
    logging.info('%s' % ('#'*60))
    logging.info('Max Performances:')
    hyd = np.max(all[0])
    isa = np.max(all[1])
    pia = np.max(all[2])
    logging.info('Hydra: %f' % hyd)
    logging.info(' ISAC: %f' % isa)
    logging.info(' PIAC: %f' % pia)
    logging.info('%s' % ('#'*60))
    logging.info('%s' % ('#'*60))

if __name__ == '__main__':
    main()