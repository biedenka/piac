import argparse
import logging
import json
import glob
import os
import sys
import inspect
import itertools
import pprint
from typing import Union

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

import matplotlib as mpl
import numpy as np

mpl.use('Agg')
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd


########################################################################################################################
# EVALUATION UTILS EVALUATION UTILS EVALUATION UTILS EVALUATION UTILS EVALUATION UTILS EVALUATION UTILS EVALUATION UTILS
########################################################################################################################

MAX_PRINT_LEN = 81
PRECISION = 3
INT_WIDTH = 4
DIGIT_WIDTH = 8
FLOAT_FMT_STR = '{:= {width}.{prec}f}'
INT_FMT_STR = '{:> {width}d}'
KEY_ORDER = ['crashes', 'timeouts', 'successes', 'n_points', 'mean_costs', 'mean_runtime', 'mean_par_runtime']
HLINE = '{border}{fill}{border}'
MAX_STR_LEN = 10


def _update_digit_fmt(width: int, prec: int = PRECISION):
    global PRECISION
    global INT_WIDTH
    global DIGIT_WIDTH
    PRECISION = prec
    INT_WIDTH = width
    DIGIT_WIDTH = width + 1 + prec
    logging.debug('UPDATED DIGIT FORMATTING SPECS')


def _update_str_fmt(width: int):
    global MAX_STR_LEN
    MAX_STR_LEN = width
    logging.debug('UPDATED MAX_STR_LEN -> {:{width}d}'.format(width, width=3))


def separate_data(piacData: dict, mean: bool = True, ignore_unsolvable: bool = True, cutoff: int = 5) -> (dict, dict,
                                                                                                          dict, dict):
    """
    Separates the data generated during validation
    :param piacData: dict. Contains infos of runtimes and costs per partition and overall
    :param mean: bool if mean data is used or not
    :param ignore_unsolvable: bool if unsolvable instances are ignored or not
    :param cutoff: int cutoff
    :return: 
    """
    global MAX_STR_LEN, INT_WIDTH
    logging.info('Seperating PIAC from SMAC data {}'.format('(mean values)' if mean else ''))
    if mean:
        logging.debug('Mean file contents:\n' + pprint.pformat(piacData))

    piacDict = piacData['piac']
    smacDict, smacData, defDict, defData, isacDict, isacData, hydraDict, hydraData = {}, {}, {}, {}, {}, {}, {}, {}
    del piacData['piac']
    try:
        smacDict = piacData['smac']
        del piacData['smac']
    except KeyError:
        smacDict = piacDict
    try:
        defDict = piacData['def']
        del piacData['def']
    except KeyError:
        defDict = piacDict
    try:
        isacDict = piacData['isac']
        del piacData['isac']
    except KeyError:
        isacDict = piacDict
    try:
        hydraDict = piacData['hydra']
        del piacData['hydra']
    except KeyError:
        hydraDict = piacDict

    tmp_max = max(map(lambda x: len(x), piacDict.keys()))
    if tmp_max >= MAX_STR_LEN:
        _update_str_fmt(tmp_max)
    del_keys = []
    for idx, partition_name in enumerate(piacData):
        if 'smac' in partition_name:
            smacData[partition_name] = piacData[partition_name]
            del_keys.append(partition_name)
        if 'def' in partition_name:
            defData[partition_name] = piacData[partition_name]
            del_keys.append(partition_name)
        if 'Conf' in partition_name:
            hydraData[partition_name] = piacData[partition_name]
            del_keys.append(partition_name)
        try:
            tmp = int(partition_name)
            isacData[partition_name] = piacData[partition_name]
            del_keys.append(partition_name)
        except ValueError:
            pass
    if len(str(piacDict['n_points'])) > INT_WIDTH:
        _update_digit_fmt(len(str(piacDict['n_points'])))
    try:
        if len(str(int(piacDict['mean_runtime']))) > INT_WIDTH:
            _update_digit_fmt(len(str(int(piacDict['mean_runtime']))))
        if len(str(int(piacDict['mean_par_runtime']))) > INT_WIDTH:
            _update_digit_fmt(len(str(int(piacDict['mean_par_runtime']))))
    except KeyError:
        if len(str(int(max(piacDict['runtimes'])))) > INT_WIDTH:
            _update_digit_fmt(len(str(int(max(piacDict['runtimes'])))))
        if len(str(int(max(piacDict['par_runtimes'])))) > INT_WIDTH:
            _update_digit_fmt(len(str(int(max(piacDict['par_runtimes'])))))

    for key in del_keys:
        del piacData[key]
    del del_keys

    if ignore_unsolvable:
        unsolvable = 0
        usable_methods = [(piacDict, piacData)]
        if smacDict:
            usable_methods.append((smacDict, smacData))
        if defDict:
            usable_methods.append((defDict, defData))
        if hydraDict:
            usable_methods.append((hydraDict, hydraData))
        if isacDict:
            usable_methods.append((isacDict, isacData))
        to_delete = []
        count = 0
        for idx, inst in enumerate(piacDict['insts']):
            solvable = False
            for method in usable_methods:
                solvable = solvable or method[0]['par_runtimes'][idx] < cutoff
                # print(method[0]['par_runtimes'][idx], '>=', cutoff, method[0]['par_runtimes'][idx] >= cutoff, solvable)
            if not solvable:
                count += 1
                logging.debug('%d=>%s not solvable by all methods in the validation file' % (count, inst))
                unsolvable += 1
                to_delete.append([idx, inst])

        logging.info('removing %d unsolvable insts' % count)
        to_delete = sorted(to_delete, reverse=True, key=lambda x: x[0])
        for idx, inst in to_delete:
            piacDict['cropped_n_points'] = piacDict['n_points'] - len(to_delete)
            piacDict['timeouts'] -= 1
            del piacDict['runtimes'][idx]
            del piacDict['par_runtimes'][idx]
            del piacDict['costs'][idx]
            del piacDict['insts'][idx]
            for partition in piacData:
                if inst in piacData[partition]['insts']:
                    piacData[partition]['n_points'] -= 1
                    piacData[partition]['timeouts'] -= 1
                    tmp_idx = piacData[partition]['insts'].index(inst)
                    del piacData[partition]['runtimes'][tmp_idx]
                    del piacData[partition]['par_runtimes'][tmp_idx]
                    del piacData[partition]['costs'][tmp_idx]
                    del piacData[partition]['insts'][tmp_idx]

            if smacData:
                smacDict['cropped_n_points'] = smacDict['n_points'] - len(to_delete)
                smacDict['timeouts'] -= 1
                del smacDict['runtimes'][idx]
                del smacDict['par_runtimes'][idx]
                del smacDict['costs'][idx]
                del smacDict['insts'][idx]
                for partition in smacData:
                    if inst in smacData[partition]['insts']:
                        smacData[partition]['n_points'] -= 1
                        smacData[partition]['timeouts'] -= 1
                        tmp_idx = smacData[partition]['insts'].index(inst)
                        del smacData[partition]['runtimes'][tmp_idx]
                        del smacData[partition]['par_runtimes'][tmp_idx]
                        del smacData[partition]['costs'][tmp_idx]
                        del smacData[partition]['insts'][tmp_idx]

            if defData:
                defDict['cropped_n_points'] = defDict['n_points'] - len(to_delete)
                defDict['timeouts'] -= 1
                del defDict['runtimes'][idx]
                del defDict['par_runtimes'][idx]
                del defDict['costs'][idx]
                del defDict['insts'][idx]
                for partition in defData:
                    if inst in defData[partition]['insts']:
                        defData[partition]['n_points'] -= 1
                        defData[partition]['timeouts'] -= 1
                        tmp_idx = defData[partition]['insts'].index(inst)
                        del defData[partition]['runtimes'][tmp_idx]
                        del defData[partition]['par_runtimes'][tmp_idx]
                        del defData[partition]['costs'][tmp_idx]
                        del defData[partition]['insts'][tmp_idx]

            if hydraData:
                hydraDict['cropped_n_points'] = hydraDict['n_points'] - len(to_delete)
                hydraDict['timeouts'] -= 1
                del hydraDict['runtimes'][idx]
                del hydraDict['par_runtimes'][idx]
                del hydraDict['costs'][idx]
                del hydraDict['insts'][idx]
                for partition in hydraData:
                    if inst in hydraData[partition]['insts']:
                        hydraData[partition]['n_points'] -= 1
                        hydraData[partition]['timeouts'] -= 1
                        tmp_idx = hydraData[partition]['insts'].index(inst)
                        del hydraData[partition]['runtimes'][tmp_idx]
                        del hydraData[partition]['par_runtimes'][tmp_idx]
                        del hydraData[partition]['costs'][tmp_idx]
                        del hydraData[partition]['insts'][tmp_idx]

            if isacData:
                isacDict['cropped_n_points'] = isacDict['n_points'] - len(to_delete)
                isacDict['timeouts'] -= 1
                del isacDict['runtimes'][idx]
                del isacDict['par_runtimes'][idx]
                del isacDict['costs'][idx]
                del isacDict['insts'][idx]
                for partition in isacData:
                    if inst in isacData[partition]['insts']:
                        isacData[partition]['n_points'] -= 1
                        isacData[partition]['timeouts'] -= 1
                        tmp_idx = isacData[partition]['insts'].index(inst)
                        del isacData[partition]['runtimes'][tmp_idx]
                        del isacData[partition]['par_runtimes'][tmp_idx]
                        del isacData[partition]['costs'][tmp_idx]
                        del isacData[partition]['insts'][tmp_idx]

    return piacDict, piacData, smacDict, smacData, defDict, defData, isacDict, isacData, hydraDict, hydraData


def _check_data(x_data: np.ndarray, y_data: np.ndarray, cutoff: int, value: Union[int, float]):
    x_data[x_data >= cutoff] = value
    y_data[y_data >= cutoff] = value
    return x_data, y_data


def plot_scatter_plot(x_data: list, y_data: list, labels: list, ax: plt.axes, title: str = "", cutoff: int = 5,
                      par: int = 10, colors: int = None,
                      linefactors: Union[list, None] = None, lims: Union[list, None] = None) -> [plt.axes,
                                                                                                 [float, float]]:
    """
    Plots x_data vs y_data as scatter plot.
    """
    if colors is not None:
        palet = sns.color_palette('dark', 30)
        colors = list(map(lambda x: palet[x], colors))
    sns.set_style('darkgrid')
    x_data = np.array(x_data)
    y_data = np.array(y_data)
    x_data, y_data = _check_data(x_data, y_data, cutoff, 10 * ((10 % cutoff) + 1))
    ax.set(xscale="log", yscale="log")
    df = {labels[0]: x_data, labels[1]: y_data, 'x_timeouts': x_data >= cutoff * par,
          'y_timeouts': y_data >= cutoff * par,
          'timeouts': list(map(lambda x, y: x >= cutoff * par and y >= cutoff * par, x_data, y_data))}
    grid = sns.regplot(labels[0], labels[1],
                       data=pd.DataFrame(df),
                       fit_reg=False, ax=ax, scatter_kws={"s": 5, 'color': 'k' if colors is None else colors})
    grid.set(xscale="log", yscale="log")

    # Colors
    ref_colors = itertools.cycle([
        "#377eb8",  # Blue
        "#4daf4a",  # Green
        "#984ea3",  # Purple
        "#ff7f00",  # Orange
        "#ffff33",  # Yellow
        "#a65628",  # Brown
        "#f781bf",  # Pink
    ])

    # set initial limits
    if not lims:
        auto_min_val = min([np.min(x_data) - abs(np.min(x_data)) * .5, np.min(y_data) - abs(np.min(y_data)) * .5])
        auto_max_val = np.max(x_data) + np.max(x_data)
    else:
        auto_min_val, auto_max_val = lims

    # Plot angle bisector and reference_lines
    out_up = cutoff
    out_lo = max(10 ** -6, auto_min_val)
    if linefactors:
        for f in linefactors:
            c = next(ref_colors)
            # Lower reference lines
            ax.plot([f * out_lo, out_up], [out_lo, (1.0 / f) * out_up], c=c,
                    linestyle='--', linewidth=.5 * 1.5, zorder=0, label='{:>4s}x'.format(str(f)))
            # Upper reference lines
            ax.plot([out_lo, (1.0 / f) * out_up], [f * out_lo, out_up], c=c,
                    linestyle='--', linewidth=.5 * 1.5, zorder=0)
    ax.plot(np.linspace(auto_min_val, auto_max_val, 3),
            np.linspace(auto_min_val, auto_max_val, 3), 'r', zorder=0)
    ax.plot(np.linspace(auto_min_val, cutoff, 3), [cutoff] * 3, 'r', zorder=0,
            linestyle='--', linewidth=.5 * 1.5, alpha=0.75)
    ax.plot([cutoff] * 3, np.linspace(auto_min_val, cutoff, 3), 'r', zorder=0,
            linestyle='--', linewidth=.5 * 1.5, alpha=0.75)

    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1], labelpad=-15)
    ax.set_xlim((auto_min_val, auto_max_val))
    ax.set_ylim((auto_min_val, auto_max_val))

    start = int(np.floor(np.log10(np.abs(auto_min_val)))) - 1
    tmp_max = int(np.floor(np.log10(np.max(x_data))))
    x_labels = [r'$10^{%d}$' % (start + i) if start + i < tmp_max else 'TIMEOUTS' for i in range(
        len(ax.get_xticklabels()))]
    tmp_max = int(np.floor(np.log10(np.max(y_data))))
    y_labels = [r'$10^{%d}$' % (start + i) if start + i < tmp_max else 'TIMEOUTS' for i in range(
        len(ax.get_yticklabels()))]
    ax.set_xticklabels(x_labels)
    ax.set_yticklabels(y_labels)
    ax.set_title(title)
    ax.legend(handletextpad=0, loc='center left', bbox_to_anchor=(-.1, 1.015), fancybox=True, frameon=True)
    return ax, (auto_min_val, auto_max_val)


def plot_dists_A(ax: plt.axes, x: list, y: list, cutoff: int, legend: list,
                 par: Union[None, int] = None, title: str = '', labels: Union[str, list] = '') -> plt.axes:
    """
    Fit and plot curves to x and y data for visual comparison
    """
    x = np.array(x)
    y = np.array(y)
    try:
        x, y = _check_data(x, y, cutoff, par * cutoff)
    except TypeError:
        x, y = _check_data(x, y, cutoff, 10 * cutoff)
    da = sns.distplot(x[x <= cutoff if not par else cutoff * par], ax=ax)
    db = sns.distplot(y[y <= cutoff], ax=ax)
    ax.set_xlim([0, cutoff if not par else cutoff * par])
    ax.set_ylim([0, 2.5])
    ax.legend(legend)
    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    ax.set_title(title)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    return ax


def plot_dists_B(ax: plt.axes, a: list, b: list, cutoff: int,
                 legend: list, title: str, labels: list, colors: int = None) -> plt.axes:
    """
    Plot a and b data as point-clouds for visual comparison
    """
    try:
        tada = len(set(colors))
    except TypeError:
        tada = -9999
    if colors is not None:
        palet = sns.color_palette('dark', 30)
        _bla = False
        colors = np.array(list(map(lambda x: palet[x], colors)))
    else:
        colors = np.array([sns.color_palette('hls', 20)[13] for _ in range(len(a))])
        _bla = True
    if tada == 1:
        _bla = True

    a = np.array(a)
    b = np.array(b)
    a, b = _check_data(a, b, cutoff, 10 * ((10 % cutoff) + 1))
    plot_rng = np.random.RandomState(349834)
    spread = 0.125
    ax.plot(np.linspace(-1, 2, 3), [cutoff] * 3, 'r', zorder=0,
            linestyle='--', linewidth=.5 * 1.5, alpha=0.75)
    ax.scatter(plot_rng.randn(len(a[a < cutoff])) * spread, a[a < cutoff], alpha=0.9, s=5, marker='o',
               label='SUCCESS', c=colors[a < cutoff])
    ax.scatter(1 + plot_rng.randn(len(b[b < cutoff])) * spread, b[b < cutoff], alpha=0.9, s=5, marker='o',
               c=colors[b < cutoff])

    ax.scatter(plot_rng.randn(len(a[a >= cutoff])) * spread, a[a >= cutoff], alpha=0.9, s=5, marker='8',
               label='TIMEOUT', c=sns.color_palette('hls', 20)[10] if _bla else colors[a >= cutoff])
    ax.scatter(1 + plot_rng.randn(len(b[b >= cutoff])) * spread, b[b >= cutoff], alpha=0.9, s=5, marker='8',
               c=sns.color_palette('hls', 20)[10] if _bla else colors[b >= cutoff])
    ax.set_xlim(-1, 2)

    ax.set_xticks([0, 1], ['a', 'b'])
    if _bla:
        ax.legend(handletextpad=-.5, loc='center right', bbox_to_anchor=(0.15, 1.015), fancybox=True, frameon=True)
    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    ax.set_title(title)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1], labelpad=-15)
    x_labels = ['' for i in range(len(ax.get_xticklabels()))]
    x_labels[2] = legend[0]
    x_labels[4] = legend[1]
    ax.set_xticklabels(x_labels)
    return ax


class MyArgparseNamespace:
    """
    Just a class that is used for type hinting such that pycharm knows what to expect in the main method
    """

    def __init__(self):
        self.par = 10  # type: int
        self.dir = '.'  # type: str
        self.cutoff = 5  # type: int
        self.plot = True  # type: bool
        self.cloud = True  # type: bool
        self.verbosity = 0  # type: int
        self.mode = 'TEST'  # type: str
        self.noPAR = False  # type: bool
        self.default = False  # type: bool
        self.isac = False  # type: bool
        self.hydra = False  # type: bool
        self.color = False  # type: bool
        self.save_logs_to = ''  # type: str
        self.save_plots_to = ''  # type: str


def _output_lines(logger: logging._loggerClass, korder: list, piac_data: dict,
                  smac_data: dict, part: str = 'piac', mean: bool = False, def_: bool = False):
    """
    Simple method to print PIAC statistics to stdout.
    :param korder: list, key order in which to output Statistics
    :param piac_data: Dictionary of piac run data
    :param smac_data: Dictionary of smac run data
    :param part: str Name of partition to load data
    :param mean: bool Flag to determine if the mean data is used or not
    :return: 
    """
    count = 0
    smac_key = 'smac_' if not def_ else 'def_'
    if mean:
        piac_data = {'piac': piac_data}
        smac_data = {'smac_piac': smac_data} if not def_ else {'def_piac': smac_data}
    for key in korder:
        if key in ['timeouts', 'successes', 'crashes', 'n_points']:
            data_str = INT_FMT_STR.format(piac_data[part][key], width=DIGIT_WIDTH)
            smac_data_str = INT_FMT_STR.format(smac_data[smac_key + part][key], width=DIGIT_WIDTH)
        else:
            count += 1
            data_str = FLOAT_FMT_STR.format(piac_data[part][key], width=DIGIT_WIDTH, prec=PRECISION)
            smac_data_str = FLOAT_FMT_STR.format(smac_data[smac_key + part][key],
                                                 width=DIGIT_WIDTH, prec=PRECISION)
        if count == 1:
            logger.info(HLINE.format(border='*', fill='-' * MAX_PRINT_LEN))
        key_str = '{:<{str_width}s}'.format(key, str_width=MAX_STR_LEN)

        left_half = '{:<{str_width}s}: {:<{str_width}s}'.format(key_str, data_str, str_width=MAX_STR_LEN)
        right_half = ' {:<{str_width}s} :{:>{str_width}s}'.format(smac_data_str, key_str, str_width=MAX_STR_LEN)
        logger.info('*{:>{half}s}{separator}{:<{half}s}*'.format(left_half, right_half, separator=' | ',
                                                                 half=int((MAX_PRINT_LEN - 2) / 2.)))


def main(margs: MyArgparseNamespace):
    """
    Main method that takes care of printing PIAC statistics and plotting results
    :param margs: argparse object
    :param logger: the logging object used
    :return: None
    """
    logger = logging.getLogger()

    fileHandler = logging.FileHandler("{0}.log".format(args.save_logs_to))
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)
    logger.info('Searching data')
    json_files = glob.glob(os.path.join(margs.dir, '**-{}/**joint*.json'.format(margs.mode)), recursive=True)
    logger.info('Found the following files:\n' + '\n'.join(
        ['\t{}' for _ in range(len(json_files))]).format(*json_files))

    logger.info('Sorting data by date')
    date_strs = []
    for fn in json_files:
        split = fn.split(os.path.sep)
        if split[-2] not in date_strs:
            date_strs.append(split[-2])
        logger.debug(split)

    mean_def_data, mean_def_dict, mean_smac_data, mean_smac_dict, mean_isac_dict, mean_isac_data, \
    mean_hydra_dict, mean_hydra_data = {}, {}, {}, {}, {}, {}, {}, {}
    mean_piac_data, mean_piac_dict, smac_data, smac_dict, isac_dict, isac_data, hydra_dict, \
    hydra_data = {}, {}, {}, {}, {}, {}, {}, {}
    def_data, def_dict, piac_data, piac_dict = {}, {}, {}, {}

    # First print the overall and partition statistics to stdout
    date_strs = sorted(date_strs)
    for date in date_strs:
        logger = logging.getLogger(date[:20] + date[-20:])

        fileHandler = logging.FileHandler("{0}.log".format(args.save_logs_to + '_' + date))
        fileHandler.setFormatter(logFormatter)
        logger.addHandler(fileHandler)
        logger.info('Loading Experiment data of {date}'.format(date=date))
        for fn in json_files:
            if date in fn:
                with open(fn, 'r') as fh:
                    if 'mean' in fn:
                        mean_piac_dict, mean_piac_data, mean_smac_dict, \
                            mean_smac_data, mean_def_dict, mean_def_data, mean_isac_dict, mean_isac_data, \
                            mean_hydra_dict, mean_hydra_data = separate_data(
                                json.load(fh), cutoff=args.cutoff, ignore_unsolvable=False)
                    else:
                        piac_dict, piac_data, smac_dict, smac_data, def_dict, def_data, \
                            isac_dict, isac_data, hydra_dict, hydra_data = separate_data(
                                json.load(fh), mean=False, cutoff=args.cutoff, ignore_unsolvable=args.noUnsolvable)
            else:
                continue
        if args.noUnsolvable:  # TODO write a function instead of copy pasta code
            mean_hydra_dict['mean_costs'] = np.mean(hydra_dict['costs'])
            mean_hydra_dict['mean_par_runtime'] = np.mean(hydra_dict['par_runtimes'])
            mean_hydra_dict['mean_runtime'] = np.mean(hydra_dict['runtimes'])
            mean_hydra_dict['timeouts'] = hydra_dict['timeouts']
            mean_hydra_dict['n_points'] = hydra_dict['cropped_n_points']
            for partition in hydra_data:
                mean_hydra_data[partition]['mean_costs'] = np.mean(hydra_data[partition]['costs'])
                mean_hydra_data[partition]['mean_par_runtime'] = np.mean(hydra_data[partition]['par_runtimes'])
                mean_hydra_data[partition]['mean_runtime'] = np.mean(hydra_data[partition]['runtimes'])
                mean_hydra_data[partition]['timeouts'] = hydra_data[partition]['timeouts']
                mean_hydra_data[partition]['n_points'] = hydra_data[partition]['n_points']
            mean_isac_dict['mean_costs'] = np.mean(isac_dict['costs'])
            mean_isac_dict['mean_par_runtime'] = np.mean(isac_dict['par_runtimes'])
            mean_isac_dict['mean_runtime'] = np.mean(isac_dict['runtimes'])
            mean_isac_dict['timeouts'] = isac_dict['timeouts']
            mean_isac_dict['n_points'] = isac_dict['cropped_n_points']
            for partition in isac_data:
                mean_isac_data[partition]['mean_cost'] = np.mean(isac_data[partition]['costs'])
                mean_isac_data[partition]['mean_par_runtime'] = np.mean(isac_data[partition]['par_runtimes'])
                mean_isac_data[partition]['mean_runtime'] = np.mean(isac_data[partition]['runtimes'])
                mean_isac_data[partition]['timeouts'] = isac_data[partition]['timeouts']
                mean_isac_data[partition]['n_points'] = isac_data[partition]['n_points']
            mean_def_dict['mean_costs'] = np.mean(def_dict['costs'])
            mean_def_dict['mean_par_runtime'] = np.mean(def_dict['par_runtimes'])
            mean_def_dict['mean_runtime'] = np.mean(def_dict['runtimes'])
            mean_def_dict['timeouts'] = def_dict['timeouts']
            mean_def_dict['n_points'] = def_dict['cropped_n_points']
            for partition in def_data:
                mean_def_data[partition]['mean_costs'] = np.mean(def_data[partition]['costs'])
                mean_def_data[partition]['mean_par_runtime'] = np.mean(def_data[partition]['par_runtimes'])
                mean_def_data[partition]['mean_runtime'] = np.mean(def_data[partition]['runtimes'])
                mean_def_data[partition]['timeouts'] = def_data[partition]['timeouts']
                mean_def_data[partition]['n_points'] = def_data[partition]['n_points']
            mean_smac_dict['mean_costs'] = np.mean(smac_dict['costs'])
            mean_smac_dict['mean_par_runtime'] = np.mean(smac_dict['par_runtimes'])
            mean_smac_dict['mean_runtime'] = np.mean(smac_dict['runtimes'])
            mean_smac_dict['timeouts'] = smac_dict['timeouts']
            mean_smac_dict['n_points'] = smac_dict['cropped_n_points']
            for partition in smac_data:
                mean_smac_data[partition]['mean_costs'] = np.mean(smac_data[partition]['costs'])
                mean_smac_data[partition]['mean_par_runtime'] = np.mean(smac_data[partition]['par_runtimes'])
                mean_smac_data[partition]['mean_runtime'] = np.mean(smac_data[partition]['runtimes'])
                mean_smac_data[partition]['timeouts'] = smac_data[partition]['timeouts']
                mean_smac_data[partition]['n_points'] = smac_data[partition]['n_points']
            mean_piac_dict['mean_costs'] = np.mean(piac_dict['costs'])
            mean_piac_dict['mean_par_runtime'] = np.mean(piac_dict['par_runtimes'])
            mean_piac_dict['mean_runtime'] = np.mean(piac_dict['runtimes'])
            mean_piac_dict['timeouts'] = piac_dict['timeouts']
            mean_piac_dict['n_points'] = piac_dict['cropped_n_points']
            logger.info('%d unsolvable instances ignored!' % (
                piac_dict['n_points'] - mean_piac_dict['n_points']))
            for partition in piac_data:
                mean_piac_data[partition]['mean_costs'] = np.mean(piac_data[partition]['costs'])
                mean_piac_data[partition]['mean_par_runtime'] = np.mean(piac_data[partition]['par_runtimes'])
                mean_piac_data[partition]['mean_runtime'] = np.mean(piac_data[partition]['runtimes'])
                mean_piac_data[partition]['timeouts'] = piac_data[partition]['timeouts']
                mean_piac_data[partition]['n_points'] = piac_data[partition]['n_points']
        n_piac_cluster = len(list(piac_data.keys()))
        n_isac_cluster = len(list(isac_data.keys()))
        n_hydra_cluster = len(list(hydra_data.keys()))
        logger.info('Validation used %d piac partitions' % n_piac_cluster)
        if margs.hydra:
            logger.info('Validation used %d hydra partitions' % n_hydra_cluster)
        if margs.isac:
            logger.info('Validation used %d isac partitions' % n_isac_cluster)

        colors = None
        if margs.color:
            try:
                colors = []
                partition_keys = list(piac_data.keys())
                for inst in piac_dict['insts']:
                    for idx, key in enumerate(partition_keys):
                        if inst in piac_data[key]['insts']:
                            colors.append(idx)
                            continue
                colors = np.array(colors)
            except KeyError:
                colors = None
        if margs.default:
            mean_smac_dict = mean_def_dict
            mean_smac_data = mean_def_data
            smac_dict = def_dict
            smac_data = def_data
        if margs.isac:
            mean_smac_dict = mean_isac_dict
            mean_smac_data = mean_isac_data
            smac_dict = isac_dict
            smac_data = isac_data
        if margs.hydra:
            mean_smac_dict = mean_hydra_dict
            mean_smac_data = mean_hydra_data
            smac_dict = hydra_dict
            smac_data = hydra_data
        logger.info('')
        logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
        logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
        logger.info('*{:^{width}s}*'.format('Experiment data of {date}'.format(date=date), width=MAX_PRINT_LEN))
        logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
        logger.info(HLINE.format(border='*', fill='*' * MAX_PRINT_LEN))
        logger.info('*{:^{width}s}*'.format('MEAN RESULTS', width=MAX_PRINT_LEN))
        HEAD = 'SMAC'
        if margs.default:
            HEAD = 'DEF'
        elif margs.isac:
            HEAD = 'ISAC'
        elif margs.hydra:
            HEAD = 'HYDRA'
        logger.info('*{:^{width}s}*'.format(
            '{:^{half}s}|{:^{half}s}'.format('PIAC', HEAD,
                                             half=MAX_PRINT_LEN / 6),
            width=MAX_PRINT_LEN))
        logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
        _output_lines(logger, KEY_ORDER, mean_piac_dict, mean_smac_dict, mean=True, def_=margs.default)
        logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
        logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
        if not margs.isac and not margs.hydra and not margs.noPartitions:
            for part in mean_piac_data:
                logger.info('*{:^{width}s}*'.format('Results of partition', width=MAX_PRINT_LEN))
                logger.info('*{:^{width}s}*'.format('{}'.format(part), width=MAX_PRINT_LEN))
                logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))
                logger.info(
                    '*{:^{width}s}*'.format(
                        '{:^{half}s}|{:^{half}s}'.format('PIAC', 'DEF' if margs.default else 'SMAC',
                                                         half=MAX_PRINT_LEN / 6),
                        width=MAX_PRINT_LEN))
                logger.info(HLINE.format(border='*', fill='-' * MAX_PRINT_LEN))
                _output_lines(logger, KEY_ORDER, mean_piac_data, mean_smac_data, part=part, def_=margs.default)

                logger.info(HLINE.format(border='*', fill='~' * MAX_PRINT_LEN))

        if not margs.isac and not margs.hydra:
            smac_key = 'smac_' if not margs.default else 'def_'
            # Then plot the results
            if margs.plot:
                share = False
                if margs.cloud:
                    share = True
                fig, ax = plt.subplots(1, 2, figsize=(12, 6), sharey=share)
                ax[0], lims = plot_scatter_plot(smac_dict['par_runtimes'],
                                                piac_dict['par_runtimes'],
                                                labels=['%s runtime [sec]' % ('DEF' if margs.default else 'SMAC'),
                                                        'PIAC runtime [sec]'],
                                                ax=ax[0], linefactors=[10, 100], cutoff=margs.cutoff,
                                                par=margs.par, colors=colors,
                                                title='PIAC/%s scatter' % ('DEF' if margs.default else 'SMAC'))
                if not margs.cloud:
                    ax[1] = plot_dists_A(ax[1], piac_dict['par_runtimes'],
                                         smac_dict['par_runtimes'], margs.cutoff,
                                         ['PIAC', 'DEF' if margs.default else 'SMAC'],
                                         title='PIAC/%s distribution' % ('DEF' if margs.default else 'SMAC'),
                                         labels=['runtime [sec]', '#insts solved'])
                else:
                    ax[1] = plot_dists_B(ax[1], piac_dict['par_runtimes'],
                                         smac_dict['par_runtimes'], margs.cutoff, colors=colors,
                                         legend=['PIAC', 'DEF' if margs.default else 'SMAC'],
                                         title='PIAC/%s distribution' % ('DEF' if margs.default else 'SMAC'),
                                         labels=['', 'runtime [sec]'])
                fig.suptitle('Overall')
                if margs.save_plots_to:
                    name_ = '_'.join((margs.save_plots_to, 'DEF' if margs.default else 'SMAC', 'overall',
                                      date, '.png'))
                    logger.info('Saving %s' % name_)
                    plt.savefig(name_, dpi=150)
                else:
                    plt.show()
                if not margs.noPartitions:
                    for t_idx, partition in enumerate(piac_data):
                        if margs.color:
                            try:
                                t_color = colors[colors == t_idx]
                            except TypeError:
                                t_color = None  # not supported due to an old format
                        else:
                            t_color = None
                        plt.close('all')
                        fig, ax = plt.subplots(1, 2, figsize=(12, 6), sharey=share)
                        ax[0], _ = plot_scatter_plot(smac_data[smac_key + partition]['par_runtimes'],
                                                     piac_data[partition]['par_runtimes'], colors=t_color,
                                                     labels=['%s runtime [sec]' % ('DEF' if margs.default else 'SMAC'),
                                                             'PIAC runtime [sec]'],
                                                     ax=ax[0], linefactors=[10, 100], cutoff=margs.cutoff,
                                                     par=margs.par,
                                                     title='PIAC/%s scatter' % ('DEF' if margs.default else 'SMAC'),
                                                     lims=lims)
                        if not margs.cloud:
                            ax[1] = plot_dists_A(ax[1], piac_data[partition]['par_runtimes'],
                                                 smac_data[smac_key + partition]['par_runtimes'], margs.cutoff,
                                                 ['PIAC', 'DEF' if margs.default else 'SMAC'],
                                                 title='PIAC/%s distribution' % ('DEF' if margs.default else 'SMAC'),
                                                 labels=['runtime [sec]', '#insts solved'])
                        else:
                            ax[1] = plot_dists_B(ax[1], piac_data[partition]['par_runtimes'],
                                                 smac_data[smac_key + partition]['par_runtimes'], margs.cutoff,
                                                 legend=['PIAC', 'DEF' if margs.default else 'SMAC'], colors=t_color,
                                                 title='PIAC/%s distribution' % ('DEF' if margs.default else 'SMAC'),
                                                 labels=['', 'runtime [sec]'])
                        fig.suptitle(partition)
                        if margs.save_plots_to:
                            name_ = '_'.join((margs.save_plots_to, 'DEF' if margs.default else 'SMAC', partition,
                                              date, '.png'))
                            logger.info('Saving %s' % name_)
                            plt.savefig(name_, dpi=150)
                        else:
                            plt.show()
        else:
            if margs.plot:
                # Then plot the results
                if margs.plot:
                    share = False
                    if margs.cloud:
                        share = True
                    fig, ax = plt.subplots(1, 2, figsize=(12, 6), sharey=share)
                    ax[0], lims = plot_scatter_plot(smac_dict['par_runtimes'],
                                                    piac_dict['par_runtimes'],
                                                    labels=['%s runtime [sec]' % ('ISAC' if margs.isac else 'HYDRA'),
                                                            'PIAC runtime [sec]'], colors=colors,
                                                    ax=ax[0], linefactors=[10, 100], cutoff=margs.cutoff,
                                                    par=margs.par,
                                                    title='PIAC/%s scatter' % ('ISAC' if margs.isac else 'HYDRA'))
                    if not margs.cloud:
                        ax[1] = plot_dists_A(ax[1], piac_dict['par_runtimes'],
                                             smac_dict['par_runtimes'], margs.cutoff,
                                             ['PIAC', 'ISAC' if margs.isac else 'HYDRA'],
                                             title='PIAC/%s distribution' % ('ISAC' if margs.isac else 'HYDRA'),
                                             labels=['runtime [sec]', '#insts solved'])
                    else:
                        ax[1] = plot_dists_B(ax[1], piac_dict['par_runtimes'],
                                             smac_dict['par_runtimes'], margs.cutoff,
                                             legend=['PIAC', 'ISAC' if margs.isac else 'HYDRA'], colors=colors,
                                             title='PIAC/%s distribution' % ('ISAC' if margs.isac else 'HYDRA'),
                                             labels=['', 'runtime [sec]'])
                    fig.suptitle('PIAC/%s' % ('ISAC' if margs.isac else 'HYDRA'))
                    if margs.save_plots_to:
                        name_ = '_'.join((margs.save_plots_to, 'ISAC' if margs.isac else 'HYDRA', 'overall',
                                          date, '.png'))
                        logger.info('Saving %s' % name_)
                        plt.savefig(name_, dpi=150)
                    else:
                        plt.show()


if __name__ == '__main__':
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', help='Directory to search .json files in.')
    parser.add_argument('cutoff', help='cutoff_value', type=int)
    parser.add_argument('--par', help='PAR value', type=int, default=10)
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-m', '--mode', default='TEST', help='Validate on which set(s)',
                        choices=['TEST', 'TRAIN', 'TRAIN+TEST'])
    parser.add_argument('-n', '--noPAR', action='store_true', help="Don't use PAR values but raw runtimes.")
    parser.add_argument('-p', '--plot', action='store_true', help='Plot results')
    parser.add_argument('-c', '--cloud', action='store_true', help='Plot cloud results')
    parser.add_argument('-k', '--color', action='store_true', help='Plot color coded results')
    parser.add_argument('-d', '--default', action='store_true', help='Use default results')
    parser.add_argument('-i', '--isac', action='store_true', help='Use isac results')
    parser.add_argument('-H', '--hydra', action='store_true', help='Use hydra results')
    parser.add_argument('-s', '--save_plots_to', help='Name to save the plots')
    parser.add_argument('-l', '--save_logs_to', help='Name to save the logs file', default='log')
    # TODO
    parser.add_argument('-u', '--noUnsolvable', help="""Ignore instances that were not solved
                        by any approach/configuration""", action='store_true')
    parser.add_argument('-o', '--noPartitions', help="""Don't show partitions""", action='store_true')

    args, unknown = parser.parse_known_args()
    logFormatter = logging.Formatter("%(message)s")
    logging.basicConfig(level=verbose_lvl_dict[args.verbosity])
    main(args)
