import os
import glob
import argparse
import logging
import numpy as np
import sys
import inspect

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from kraken.util.utils import scenario_option_names
from smac.facade.smac_facade import SMAC
from smac.scenario.scenario import Scenario
from smac.runhistory.runhistory import RunHistory
from smac.optimizer.objective import average_cost
from kraken.util.utils import read_traj_file


def cmd_line_call():
    verbose_lvl_dict = {'INFO': logging.INFO, 'DEBUG': logging.DEBUG}
    parser = argparse.ArgumentParser()

    parser.add_argument('working_dir', help='Directory to run in')
    parser.add_argument('-v', '--verbosity', default='INFO', help='verbosity', choices=list(verbose_lvl_dict.keys()))
    parser.add_argument('-s', '--seed', default=None, type=int)
    parser.add_argument('-w', '--wallclock_limit', default=None, type=float)
    args, unknown = parser.parse_known_args()
    logging.basicConfig(level=verbose_lvl_dict[args.verbosity])

    if args.seed is None:
        args.seed = int.from_bytes(os.urandom(4), byteorder="big")
        logging.info('Using seed: %d' % args.seed)
    restart_smac(args.working_dir, args.seed, args.wallclock_limit)


def restart_smac(working_dir, seed, wallclock_limit):
    working_dir = os.path.abspath(working_dir)
    out_f = open(os.path.join(working_dir, 'restart_scen.txt'), 'w')
    scen_fn = glob.glob(os.path.join(working_dir, 'scenario*'))[0]
    orig_dir = os.getcwd()
    with open(scen_fn) as in_fh:
        for line in in_fh:
            line = line.replace(working_dir.split(os.path.sep)[-1], '.')
            out_f.write(line)
            header, line = line.strip().split('=')
            header = header.strip()
            line = line.strip()
            logging.info(line)
            if scenario_option_names[header] in ['paramfile', 'feature_file',
                                                 'instance_file', 'test_instance_file']:
                if os.path.isdir(line) and not os.path.islink(line):
                    fn_ = line.split(os.path.sep)[0].strip()
                elif scenario_option_names[header] not in ['feature_file', 'paramfile']:
                    open_name = os.path.join(working_dir, line) if line.startswith('.') else line
                    with open(open_name, 'r') as tmp_fh:
                        l = tmp_fh.readline()
                    fn_ = l.split(',')[0].strip().split(os.path.sep)[0].strip()
                else:
                    continue
                try:
                    os.symlink(os.path.join(orig_dir, fn_), os.path.join(working_dir, fn_))
                    logging.info('Creating Symlink for %s' % fn_)
                except FileExistsError:
                    logging.debug('Not creating symlink for %s' % fn_)
            elif scenario_option_names[header] in ['algo']:
                if len(line.split(' ')) > 1:
                    fn_ = line.split(' ')[-1]
                if os.path.sep in fn_:
                    tmp_fn = fn_.split(os.path.sep)
                    if tmp_fn[0] == '.':
                        del tmp_fn[0]
                    fn_ = tmp_fn[0]
                try:
                    os.symlink(os.path.join(orig_dir, fn_), os.path.join(working_dir, fn_))
                except FileExistsError:
                    logging.debug('Not creating symlink for %s' % fn_)
    out_f.close()
    scen_fn = os.path.join(working_dir, 'restart_scen.txt')
    os.chdir(working_dir)

    scen = Scenario(scen_fn,  # cmd_args needed to suppres SMAC scenario output folder creation
                    cmd_args={'output_dir': working_dir})  # type: Scenario
    # scen.output_dir = os.path.join(working_dir, 'continuation')
    if wallclock_limit is not None:
        scen.wallclock_limit = wallclock_limit
    configuration, c = read_traj_file(scen.cs, 'traj_aclib2.json')
    runhist = RunHistory(aggregate_func=average_cost)
    runhist.load_json('runhistory.json', scen.cs)
    smac = SMAC(scen, runhistory=runhist, initial_configurations=[configuration], rng=np.random.RandomState(seed))
    smac.optimize()


if __name__ == '__main__':
    cmd_line_call()
